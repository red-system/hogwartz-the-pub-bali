<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class About_us extends Model
{
     protected $primaryKey = 'id';
    protected $guarded = [];
    protected $table = 'about_us';

    public function getPageHeaderAttribute()
    {
        if ($this->image !== '') {
            return asset('assets/front/images/' . $this->image);
        } else {
            return 'http://placehold.it/1129x306';
        }
    }

    public function getHomeSliderAttribute()
    {
        if ($this->image !== '') {
            return asset('assets/front/images/' . $this->image);
        } else {
            return 'http://placehold.it/1920x1080';
        }
    }

    public function getPartnerImageAttribute()
    {
        if ($this->image !== '') {
            return asset('assets/front/images/' . $this->image);
        } else {
            return 'http://placehold.it/176x108';
        }
    }
}
