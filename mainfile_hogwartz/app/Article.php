<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $table = 'article';
    protected $fillable = [
        'title', 'thumb_image', 'short_description', 'conten', 'additional_conten', 'meta_title', 'meta_keyword', 'meta_description', 'position', 'published', 'slug', 'parent_id', 'longitude', 'latitude', 'link', 'more_config', 'admin_config', 'lang', 'equal_id', 'featured'
    ];

    public function childs()
    {
        return $this->hasMany('App\Article', 'parent_id');
    }

    public function parent()
    {
        return $this->belongsTo('App\Article', 'parent_id');
    }

    public function images() {
        return $this->hasMany('App\Images', 'article_id');
    }

    public function packages() {
        return $this->hasMany('App\Packages', 'article_id');
    }

    public function categories() {
        return $this->belongsToMany('App\Category', 'article_category', 'article_id', 'category_id')->withTimestamps();
    }

    public function getArticleThumbImageAttribute()
    {
        if ($this->thumb_image != '') {
            return asset('assets/front/images/' . $this->thumb_image);
        } else {
            return 'http://placehold.it/600x477';
        }
    }

      /*
     * Makes a MongoID like Key value.
     * Based off time, machine Id, Pid and a sequence no.
     * From Stackoverflow: http://stackoverflow.com/questions/24355927/which-algorithm-does-mongodb-use-for-id
     */
    static public function MakeDBId($yourTimestamp = null)
    {
        static $inc = 0;
        $id = '';

        if ($yourTimestamp === null) {
            $yourTimestamp = time();
        }
        $ts = pack('N', $yourTimestamp);
        $m = substr(md5(gethostname()), 0, 3);
        $pid = pack('n', getmypid());
        $trail = substr(pack('N', $inc++), 1, 3);
        $bin = sprintf("%s%s%s%s", $ts, $m, $pid, $trail);

        for ($i = 0; $i < 12; $i++) {
            $id .= sprintf("%02X", ord($bin[$i]));
        }

        return $id;
    }
}
