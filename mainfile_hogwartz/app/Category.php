<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category';
    protected $fillable = [
        'category', 'type', 'slug', 'lang', 'equal_id', 'image'
    ];

    public function article() {
        return $this->belongsToMany('App\Article', 'article_category', 'category_id', 'article_id')->withTimestamps();
    }

    public function menus() {
        return $this->belongsToMany('App\Menus', 'menu_category', 'category_id', 'menu_id')->withTimestamps();
    }
}
