<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\About_us;
use App\Services\MyImage;

class BAboutUsController extends Controller
{
	public function index(){
     $about_us = About_us::where('lang', config('app.default_locale'))->where('type','!=','core-business')->get();
     $business = About_us::where('lang', config('app.default_locale'))->where('type','core-business')->get();
     return view('admin.webconfig.about_us.index', compact('about_us','business'));
	}

	public function edit($id){
		 $about = About_us::where('equal_id',$id)->get();
		 return view('admin.webconfig.about_us.edit', compact('about'));
	}

	  public function update(Request $request, $equalid)
    {
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');

        $about = About_us::where('equal_id', $equalid)->get();

        $rowRules = [
            'image' => 'mimes:jpeg,png|max:1024',
           
        ];

        foreach($alllangs as $lang) {
            $langRules = [
                'title_'.$lang => 'required',
                'description_'.$lang => 'required',

            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            ${'data_'.$lang} = $about->where('lang', $lang)->first();

            $data = [];
            $data['title'] = $request->{'title_'.$lang};
            $data['description'] = $request->{'description_'.$lang};

            if(${'data_'.$lang}->type == 'company-profile'){

            	$data['video'] = $request->video;	
            	
            }
           
            if ($lang == $deflang) {
                if ($request->hasFile('image')) {
                    $myimage = new MyImage();
                    if (${'data_'.$deflang}->image != '') {
                        $myimage->deleteImage(${'data_'.$deflang}->image);
                    }
                    $data['image'] = $myimage->saveImage($request->file('image'), $request->{'title_'.$lang});
                }
            } else {
                $data['image'] = ${'data_'.$deflang}->image;
            }

            

            ${'data_'.$lang}->update($data);
        }

         \Session::flash('notification', ['level' => 'success', 'message' =>  ${'data_'.$deflang}->title.' '.${'data_'.$deflang}->name.' updated.']);
        return redirect()->route('admin.config.about.index');
    }

    //create hanya untuk core business
    public function create(){
		 return view('admin.webconfig.about_us.create');
    }

     public function store(Request $request)
    {
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');
        $eqid = 'IMG'.Article::MakeDBId();

        $rowRules = [
            'image' => 'mimes:jpeg,png|max:1024',
        ];

        foreach($alllangs as $lang) {
            $langRules = [
                'title_'.$lang => 'required',
                'description_'.$lang => 'required',

            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            $data = [];
            $data['title'] = $request->{'title_'.$lang};
            $data['description'] = $request->{'description_'.$lang};
            $data['type'] = 'core-business';
            $data['equal_id'] = $eqid;
            $data['lang'] =$lang;
          

            if ($lang == $deflang) {
                if ($request->hasFile('image')) {
                    $myimage = new MyImage();
                    $data['image'] = $myimage->saveImage($request->file('image'), $request->{'title_'.$lang});
                }
            } else {
                $data['image'] = ${'data_'.$deflang}->image;
            }

            ${'data_'.$lang} = About_us::create($data);
        }

        \Session::flash('notification', ['level' => 'success', 'message' => 'Slider '.${'data_'.$deflang}->title.' saved.']);
        return redirect()->route('admin.config.about.index');
    }

    public function destroy($equal_id){
    	$images = About_us::where('equal_id', $equal_id)->groupBy('image')->pluck('image');
        foreach ($images as $image) {
            if ($image !== '') {
                $myimage = new MyImage();
                $myimage->deleteImage($image);
            }
        }
    	 About_us::where('equal_id', $equal_id)->delete();
        \Session::flash('notification', ['level' => 'success', 'message' => 'Core business deleted.']);
       	return redirect()->route('admin.config.about.index');
    }




		
    
}
