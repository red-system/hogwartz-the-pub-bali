<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Category;
use App\Menu_category;

class BCategoryController extends Controller
{
    public function create(){
        return view('admin.webconfig.category.create');
    }

    public function store(Request $request){
    	$alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');
        $eqid = 'CT'.Article::MakeDBId();

        // dd($request->icon);
        $rowRules = [];
        foreach($alllangs as $lang) {
            $langRules = [
                'category_'.$lang => 'required',
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            $data = [];
            $data['category'] = $request->{'category_'.$lang};
            $data['type'] = 'menu';
            $data['lang'] = $lang;
            $data['equal_id'] = $eqid;
            $data['image']   = $request->icon;
         
            ${'data_'.$lang} = Category::create($data);
        }

        \Session::flash('notification', ['level' => 'success', 'message' => 'Category '.${'data_'.$deflang}->category.' saved.']);
       	return redirect()->route('admin.config.menu-konten.index');
    }

    public function edit($id){
        $category= Category::where('equal_id',$id)->get();
        return view('admin.webconfig.category.edit',compact('category'));
    }

    public function update(Request $request,$equal_id){
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');
    
        $rowRules = [];
        foreach($alllangs as $lang) {
            $langRules = [
                'category_'.$lang => 'required',
               
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {

            $data = [];
            $data['category'] = $request->{'category_'.$lang};
            $data['lang'] = $lang;
            $data['image']   = $request->icon;

            ${'data_'.$lang} = Category::where('lang',$lang)->where('equal_id',$equal_id)->update($data);
        }

        \Session::flash('notification', ['level' => 'success', 'message' => 'Category  updated.']);
        return redirect()->route('admin.config.menu-konten.index');
    }

    public function destroy($id){
        $category_menu = Menu_category::where('category_id',$id)->first();
        if(!$category_menu){
            \Session::flash('notification', ['level' => 'warning', 'message' => 'Cant Deleted, Category already used in menu.']);
            return redirect()->route('admin.config.menu-konten.index');
            
        }else{
            Category::where('equal_id', $id)->delete();
             \Session::flash('notification', ['level' => 'success', 'message' => 'Category success deleted.']);
            return redirect()->route('admin.config.menu-konten.index');
        }
    }

    




}
