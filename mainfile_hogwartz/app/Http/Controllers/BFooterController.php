<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BFooterController extends Controller
{
    public function edit($id) {
        $article = Article::whereIn('position', ['footer-about', 'footer-hubungikami'])->where('equal_id', $id)->get();
        return view('admin.webconfig.edit_footer', compact('article'));
    }

    public function update(Request $request, $id) {
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');

        $article = Article::whereIn('position', ['footer-about', 'footer-hubungikami'])->where('equal_id', $id)->get();
        $rowRules = [];

        foreach($alllangs as $lang) {
            $langRules = [
                'title_'.$lang => 'required',
                'conten_'.$lang => 'required',
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            ${'footer_'.$lang} = $article->where('lang', $lang)->first();

            $data = [];
            $data['title'] = $request->{'title_'.$lang};
            $data['conten'] = $request->{'conten_'.$lang};

            ${'footer_'.$lang}->update($data);
        }

        \Session::flash('notification', ['level' => 'success', 'message' => 'Footer '.${'footer_'.$deflang}->title . ' updated.']);
        return redirect()->route('menu-utama.index');
    }
}
