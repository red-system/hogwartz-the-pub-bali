<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Article;
use App\Images;
use App\Services\MyImage;
use App\Specialities;
use App\VideoHome;
use App\Packages;
use App\Menus;
use Illuminate\Http\Request;
use App\Service;
use App\Testimonial;

class BHomeKontenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
          $specials = Specialities::where('lang', config('app.default_locale'))->get();
          $packages = Article::where('featured', '1')->where('position', 'sub-menu-utama')->where('lang', config('app.default_locale'))->where('link', \Lang::get('route.package',[], config('app.default_locale')))->get();
          $menus = Menus::where('lang', config('app.default_locale'))->where('recommend','1')->get();
          $video=VideoHome::where('id',1)->firstOrFail();
          $services = Service::where('lang', config('app.default_locale'))->get();
          $testimonials = Testimonial::get();
          $article = Article::whereIn('position', ['discover'])->get();

          $dsimage = Images::where('type', 'dsimage')->where('lang', config('app.default_locale'))->first();
          // dd($article);

          return view('admin.webconfig.homekonten.index', compact('specials','video','packages','menus', 'services', 'testimonials', 'article', 'dsimage'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function updateDiscover(Request $request, $id){

        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');

        $article = Article::whereIn('position', ['discover'])->where('equal_id', $id)->get();
        $rowRules = [];

        foreach($alllangs as $lang) {
            $langRules = [
                'title_'.$lang => 'required',
                'conten_'.$lang => 'required',
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            $discover = $article->where('lang', $lang)->first();

            $data = [];
            $data['title'] = $request->{'title_'.$lang};
            $data['conten'] = $request->{'conten_'.$lang};

            $discover->update($data);
        }

        \Session::flash('notification', ['level' => 'success', 'message' => 'Discover updated.']);
        return redirect()->route('admin.config.home-konten.index');
    }

    public function updateDsImage(Request $request) {
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');

        $dsimages = Images::where('type', 'dsimage')->get();

        $rowRules = [
            'dsimage' => 'mimes:jpeg,png|max:1024'
        ];
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            ${'image_'.$lang} = $dsimages->where('lang', $lang)->first();

            $data = [];
            if ($lang == $deflang) {
                if ($request->hasFile('dsimage')) {
                    $myimage = new MyImage();
                    if (${'image_'.$deflang}->image != '') {
                        $myimage->deleteImage(${'image_'.$deflang}->image);
                    }
                    $data['image'] = $myimage->saveImage($request->file('dsimage'), 'Daily Special');
                }
            } else {
                $data['image'] = ${'image_'.$deflang}->image;
            }

            ${'image_'.$lang}->update($data);
        }

        \Session::flash('notification', ['level' => 'success', 'message' => 'Image daily special updated.']);
        return redirect()->route('admin.config.home-konten.index');
    }
}
