<?php

namespace App\Http\Controllers;

use App\Images;
use App\Article;
use App\Services\MyImage;
use Illuminate\Http\Request;
use App\Http\Requests;

class BHomeSliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $article = Article::where('link', '/')->where('position', 'menu-utama')->where('lang', config('app.default_locale'))->firstOrFail();
        $homeslider = Images::where('type', 'home-slider')->where('slider_type', '!=', '')->where('lang', config('app.default_locale'))->get();
        return view('admin.webconfig.homeslider.index', compact('homeslider','article'));
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $article = Article::where('link', '/')->where('position', 'menu-utama')->where('lang', config('app.default_locale'))->firstOrFail();
        return view('admin.webconfig.homeslider.create', compact('article'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');
        $eqid = 'IMG'.Article::MakeDBId();

        $rowRules = [
            'image' => 'required|mimes:jpeg,png|max:1024',
            'slider_type' => 'required|in:'.implode(",", Images::allowedTipe()),
        ];
        foreach($alllangs as $lang) {
            $langRules = [
                'name_'.$lang => 'required',
                'slider_text_'.$lang => 'required',
                'link_title_'.$lang => 'required',
                'link_text_'.$lang => 'required'
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            $data = [];
            $data['name'] = $request->{'name_'.$lang};
            $data['slider_type'] = $request->slider_type;
            $data['type'] = 'home-slider';
            $data['lang'] = $lang;
            $data['link_text'] = $request->{'link_text_'.$lang};
            $data['link_title'] = $request->{'link_title_'.$lang};
            $data['equal_id'] = $eqid;
            $data['slider_text'] = $request->{'slider_text_'.$lang};

            if ($lang == $deflang) {
                if ($request->hasFile('image')) {
                    $myimage = new MyImage();
                    $data['image'] = $myimage->saveImage($request->file('image'), $request->{'name_'.$lang});
                }
            } else {
                $data['image'] = ${'image_'.$deflang}->image;
            }

            ${'image_'.$lang} = Images::create($data);
        }

        \Session::flash('notification', ['level' => 'success', 'message' => 'Slider '.${'image_'.$deflang}->name.' saved.']);
        return redirect()->route('admin.config.homeslider.index');
    }

 
    public function edit($equalid)
    {
        $article = Article::where('link', '/')->where('position', 'menu-utama')->where('lang', config('app.default_locale'))->firstOrFail();
        $slider = Images::where('type', 'home-slider')->where('equal_id', $equalid)->get();
        return view('admin.webconfig.homeslider.edit', compact('slider','article'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $equalid)
    {
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');

        $images = Images::where('type', 'home-slider')->where('equal_id', $equalid)->get();

        $rowRules = [
            'image' => 'mimes:jpeg,png|max:1024',
            'slider_type' => 'required|in:'.implode(",", Images::allowedTipe()),
        ];

        foreach($alllangs as $lang) {
            $langRules = [
                'name_'.$lang => 'required',
                'slider_text_'.$lang => 'required',
                'link_title_'.$lang => 'required',
                'link_text_'.$lang => 'required'
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            ${'image_'.$lang} = $images->where('lang', $lang)->first();

            $data = [];
            $data['name'] = $request->{'name_'.$lang};
            $data['slider_type'] = $request->slider_type;
            $data['link_text'] = $request->{'link_text_'.$lang};
            $data['link_title'] = $request->{'link_title_'.$lang};
            $data['slider_text'] = $request->{'slider_text_'.$lang};

            if ($lang == $deflang) {
                if ($request->hasFile('image')) {
                    $myimage = new MyImage();
                    if (${'image_'.$deflang}->image != '') {
                        $myimage->deleteImage(${'image_'.$deflang}->image);
                    }
                    $data['image'] = $myimage->saveImage($request->file('image'), $request->{'name_'.$lang});
                }
            } else {
                $data['image'] = ${'image_'.$deflang}->image;
            }

            ${'image_'.$lang}->update($data);
        }

         \Session::flash('notification', ['level' => 'success', 'message' => 'Slider '.${'image_'.$deflang}->name.' updated.']);
        return redirect()->route('admin.config.homeslider.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
       $images = Images::where('equal_id', $request->equal_id)->groupBy('image')->pluck('image');
        foreach ($images as $image) {
            if ($image !== '') {
                $myimage = new MyImage();
                $myimage->deleteImage($image);
            }
        }
        Images::where('equal_id', $request->equal_id)->delete();
        \Session::flash('notification', ['level' => 'success', 'message' => 'Home slider deleted.']);
        return redirect()->route('admin.config.homeslider.index');
    }
}
