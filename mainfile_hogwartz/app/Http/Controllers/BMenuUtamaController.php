<?php

namespace App\Http\Controllers;

use App\Article;
use Illuminate\Http\Request;
use App\Services\MyImage;

class BMenuUtamaController extends Controller
{
    public function index()
    {
        $bmenus = Article::where('admin_config', '1')->where('lang', config('app.default_locale'))->select('id', 'title', 'link', 'slug', 'parent_id', 'more_config', 'equal_id')->orderBy('id')->get();
        $no = 1;
        $menuconfig = $this->menuConfig();

        // dd($menuconfig);
        $footers = Article::whereIn('position', ['footer-about'])->where('lang', config('app.default_locale'))->get();
        return view('admin.webconfig.index', compact('bmenus', 'no', 'onpages', 'menuconfig', 'footers'));
    }

    public function edit($id)
    {

        $article = Article::where('admin_config', '1')->where('equal_id', $id)->get();
        return view('admin.webconfig.edit', compact('article'));
    }

    public function update(Request $request, $id)
    {
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');

        $article = Article::where('admin_config', '1')->where('equal_id', $id)->get();
        $rowRules = [
            'published' => 'required|in:1,0'
        ];

        foreach ($alllangs as $lang) {
            $langRules = [
                'title_' . $lang => 'required',
                'conten_' . $lang => 'required',
                'meta_title_' . $lang => 'required',
                'meta_keyword_' . $lang => 'required',
                'meta_description_' . $lang => 'required',
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);


        $thumb_image = '';
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $file->move('assets/front/images', $file->getClientOriginalName());
            $thumb_image = $file->getClientOriginalName();
        }

        foreach ($alllangs as $lang) {
            ${'menu_' . $lang} = $article->where('lang', $lang)->first();

            $data = [];
            $data['title'] = $request->{'title_' . $lang};
            $data['conten'] = $request->{'conten_' . $lang};
            $data['meta_title'] = $request->{'meta_title_' . $lang};
            $data['meta_keyword'] = $request->{'meta_keyword_' . $lang};
            $data['meta_description'] = $request->{'meta_description_' . $lang};
            $data['published'] = $request->published;

            if ($thumb_image) {
                $data['thumb_image'] = $thumb_image;
            }

            //return $data;

            ${'menu_' . $lang}->update($data);
        }

        \Session::flash('notification', ['level' => 'success', 'message' => 'Article  ' . ${'menu_' . $deflang}->title . ' updated.']);
        return redirect()->route('menu-utama.index');
    }


    public function menuConfig()
    {
        $lang = config('app.default_locale');
        return [
            '/' => [['type' => 'image', 'tooltip' => 'Home Sliders', 'link' => 'homeslider'], ['type' => 'data', 'tooltip' => 'Home  Content', 'link' => 'home-konten']],
            \Lang::get('route.about', [], $lang) => [['type' => 'data', 'tooltip' => 'About', 'link' => 'about']],
            \Lang::get('route.schedule', [], $lang) => [['type' => 'data', 'tooltip' => 'Schedule', 'link' => 'schedule']],
            \Lang::get('route.gallery', [], $lang) => [['type' => 'data', 'tooltip' => 'Gallery', 'link' => 'gallery']],
            // \Lang::get('route.package',[], $lang) => [['type' => 'data', 'tooltip' => 'Package', 'link' => 'packages']],
            \Lang::get('route.menu', [], $lang) => [['type' => 'data', 'tooltip' => 'Menu', 'link' => 'menu-konten'], ['type' => 'data', 'tooltip' => 'Additional Conten', 'link' => 'menu-konten/additional']],
            \Lang::get('route.news', [], $lang) => [['type' => 'data', 'tooltip' => 'Event', 'link' => 'news']],
            \Lang::get('route.sistercompany', [], $lang) => [['type' => 'data', 'tooltip' => 'Sister company', 'link' => 'sistercompany']],
        ];
    }
}
