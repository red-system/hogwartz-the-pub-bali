<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Category;
use App\Menus;
use App\Services\MyImage;

class BMenusController extends Controller
{

 public function index()
    {
      $categorys = Category::where('lang', config('app.default_locale'))->where('type','menu')->get();
      $menus = Menus::where('lang', config('app.default_locale'))->get();
      return view('admin.webconfig.menus.index', compact('categorys','video','menus'));
    }

    public function create(){
          return view('admin.webconfig.menus.create');
    }

    public function store(Request $request){
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');
        $eqid = uniqid('MN', true);

        $rowRules = [
            'image' => 'required_if:type,food|mimes:jpeg,png|max:512',
            'categories' => 'required',
            'type' => 'required|in:'.implode(',', Menus::allowedMenu())
        ];
        foreach($alllangs as $lang) {
            $langRules = [
                'name_'.$lang => 'required',
                'short_description_'.$lang => 'required_if:type,food',
                'description_'.$lang => 'required_if:type,food',
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {

            $data = [];
            $data['name'] = $request->{'name_'.$lang};
            $data['type'] = $request->type;
            $data['price'] = $request->price;
            $data['lang'] = $lang;
            $data['equal_id'] = $eqid;
            if ($lang == $deflang) {
                if ($request->hasFile('image')) {
                    $myimage = new MyImage();
                    $data['image'] = $myimage->saveImage($request->file('image'), $request->{'name_'.$lang});
                }
            } else {
                $data['image'] = ${'menu_'.$deflang}->image;
            }

            if ($request->type == 'food') {
                $data['short_description'] = $request->{'short_description_'.$lang};
                $data['description'] = $request->{'description_'.$lang};
                
            } else {
                $data['short_description'] = '';
                $data['description'] = '';
                // $data['image'] = '';
            }

            ${'menu_'.$lang} = Menus::create($data);

            ${'categories_'.$lang} = Category::whereIn('equal_id', $request->categories)->where('lang', $lang)->pluck('id');
            ${'menu_'.$lang}->categories()->sync(${'categories_'.$lang});
        }

        \Session::flash('notification', ['level' => 'success', 'message' => 'Menus '.${'menu_'.$deflang}->name.' saved.']);
        return redirect()->route('admin.config.menu-konten.index');
    }

    public function edit($id){
        $menus = Menus::where('equal_id',$id)->get();
        return view('admin.webconfig.menus.edit', compact('menus'));
    }

    public function update(Request $request, $equal_id) {
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');
        $menus = Menus::where('equal_id',$equal_id)->get();

        $rowRules = [
            'image' => 'mimes:jpeg,png|max:512',
            'categories' => 'required',
            'type' => 'required|in:'.implode(',', Menus::allowedMenu())
        ];
        foreach($alllangs as $lang) {
            $langRules = [
                'name_'.$lang => 'required',
                'description_'.$lang => 'required_if:type,food',
                'short_description_'.$lang => 'required_if:type,food',
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            ${'menu_'.$lang} = $menus->where('lang', $lang)->first();

            $data = [];
            $data['name'] = $request->{'name_'.$lang};
            $data['type'] = $request->type;
            $data['price'] = $request->price;

            if ($lang == $deflang) {
                if ($request->hasFile('image')) {
                    $myimage = new MyImage();
                    if (${'menu_'.$deflang}->image != '') {
                        $myimage->deleteImage(${'menu_'.$deflang}->image);
                    }
                    $data['image'] = $myimage->saveImage($request->file('image'), $request->{'name_'.$lang});
                }
            } else {
                $data['image'] = ${'menu_'.$deflang}->image;
            }

            if ($request->type == 'food') {
                $data['short_description'] = $request->{'short_description_'.$lang};
                $data['description'] = $request->{'description_'.$lang};

                
            } else {
                $data['short_description'] = '';
                $data['description'] = '';
                // $data['image'] = '';
            }

            ${'menu_'.$lang}->update($data);

            ${'categories_'.$lang} = Category::whereIn('equal_id', $request->categories)->where('lang', $lang)->pluck('id');
            ${'menu_'.$lang}->categories()->sync(${'categories_'.$lang});
        }

        \Session::flash('notification', ['level' => 'success', 'message' =>  'Menu '.${'menu_'.$deflang}->name.' updated.']);
        return redirect()->route('admin.config.menu-konten.index');
    }

    public function destroy($equal_id) {
        $images = Menus::where('equal_id',$equal_id)->groupBy('image')->pluck('image');
        foreach ($images as $image) {
            if ($image !== '') {
                $myimage = new MyImage();
                $myimage->deleteImage($image);
            }
        }
        Menus::where('equal_id',$equal_id)->delete();
        \Session::flash('notification', ['level' => 'success', 'message' => 'Menu deleted.']);
        return redirect()->route('admin.config.menu-konten.index');
    }

    // recommend controller

   public function createRecommend(){
       $list_menus = Menus::where('recommend', '0')->where('lang', config('app.default_locale'))->pluck('name','equal_id');
       return view('admin.webconfig.menus.create_recommend', compact('list_menus'));
   }

   public function updateRecommend(Request $request){
        $equal_id = $request->menu;
        $recommend = '1';
        
        $update = Menus::where('equal_id',$equal_id)->update([
            'recommend' => $recommend
        ]);

        \Session::flash('notification', ['level' => 'success', 'message' => 'Recommend menu updated.']);
        return redirect()->route('admin.config.home-konten.index');
    }

    public function destroyRecommend($id){
        $equal_id = $id;
        $recommend = '0';
        
        $update = Menus::where('equal_id',$equal_id)->update([
            'recommend' => $recommend
        ]);

        \Session::flash('notification', ['level' => 'success', 'message' => 'Recommend menu deleted.']);
        return redirect()->route('admin.config.home-konten.index');
    }

    public function editadditional() {
        $articlemenu = Article::where('link', \Lang::get('route.menu',[], config('app.default_locale')))->where('position', 'menu-utama')->where('lang', config('app.default_locale'))->firstOrFail();
        $artmenus = Article::where('equal_id', $articlemenu->equal_id)->get();
        return view('admin.webconfig.menus.edit_additional', compact('artmenus'));
    }

    public function updateadditional(Request $request) {
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');

        $articlemenu = Article::where('link', \Lang::get('route.menu',[], config('app.default_locale')))->where('position', 'menu-utama')->where('lang', config('app.default_locale'))->firstOrFail();
        $artmenus = Article::where('equal_id', $articlemenu->equal_id)->get();

        foreach($alllangs as $lang) {
            ${'artmenu_'.$lang} = $artmenus->where('lang', $lang)->first();

            $data = [];
            $data['additional_conten'] = $request->{'additional_conten_'.$lang};

            ${'artmenu_'.$lang}->update($data);
        }

        \Session::flash('notification', ['level' => 'success', 'message' => 'Additional menu conten updated.']);
        return back();
    }
}
