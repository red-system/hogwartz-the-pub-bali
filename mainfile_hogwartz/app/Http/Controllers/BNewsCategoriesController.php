<?php

namespace App\Http\Controllers;

use App\NewsCategories;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BNewsCategoriesController extends Controller
{
    public function create() {
        return view('admin.webconfig.news.category.create');
    }

    public function store(Request $request) {
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');

        $eqid = uniqid('NC', true);
        $rowRules = [];
        foreach($alllangs as $lang) {
            $langRules = [
                'category_'.$lang => 'required',
                'meta_title_'.$lang => 'required',
                'meta_keyword_'.$lang => 'required',
                'meta_description_'.$lang => 'required',
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            $data = [];
            $data['category'] = $request->{'category_'.$lang};
            $data['equal_id'] = $eqid;
            $data['lang'] =$lang;
            $data['meta_title'] = $request->{'meta_title_'.$lang};
            $data['meta_keyword'] = $request->{'meta_keyword_'.$lang};
            $data['meta_description'] = $request->{'meta_description_'.$lang};
            $data['slug'] = str_slug($request->{'category_'.$lang});

            ${'data_'.$lang} = NewsCategories::create($data);
        }

        \Session::flash('notification', ['level' => 'success', 'message' => 'News category '.${'data_'.$deflang}->category.' saved.']);
        return redirect()->route('admin.config.news.index');
    }

    public function edit($equal_id) {
        $categories = NewsCategories::where('equal_id', $equal_id)->get();
        return view('admin.webconfig.news.category.edit', compact('categories'));
    }

    public function update(Request $request, $equal_id) {
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');
        $categories = NewsCategories::where('equal_id', $equal_id)->get();

        $rowRules = [];
        foreach($alllangs as $lang) {
            $langRules = [
                'category_'.$lang => 'required',
                'meta_title_'.$lang => 'required',
                'meta_keyword_'.$lang => 'required',
                'meta_description_'.$lang => 'required',
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            ${'category_'.$lang} = $categories->where('lang', $lang)->first();

            $data = [];
            $data['category'] = $request->{'category_'.$lang};
            $data['meta_title'] = $request->{'meta_title_'.$lang};
            $data['meta_keyword'] = $request->{'meta_keyword_'.$lang};
            $data['meta_description'] = $request->{'meta_description_'.$lang};
            $data['slug'] = str_slug($request->{'category_'.$lang});

            ${'category_'.$lang}->update($data);
        }

        \Session::flash('notification', ['level' => 'success', 'message' =>  'News category "'.${'category_'.$deflang}->category.'" updated.']);
        return redirect()->route('admin.config.news.index');
    }

    public function destroy($equal_id){
        NewsCategories::where('equal_id', $equal_id)->delete();
        \Session::flash('notification', ['level' => 'success', 'message' => 'News category deleted.']);
        return redirect()->route('admin.config.news.index');
    }
}
