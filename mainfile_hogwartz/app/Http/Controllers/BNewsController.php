<?php

namespace App\Http\Controllers;

use App\NewsCategories;
use App\NewsTags;
use Illuminate\Http\Request;
use App\Article;
use App\News;
use App\Services\MyImage;


class BNewsController extends Controller
{
    public function index(){
     	$news = News::where('lang', config('app.default_locale'))->get();
        $categories = NewsCategories::where('lang', config('app.default_locale'))->get();
        $tags = NewsTags::where('lang', config('app.default_locale'))->get();
      return view('admin.webconfig.news.index', compact('news', 'categories', 'tags'));
    }


    public function create(){
		 return view('admin.webconfig.news.create');
    }

  	public function store(Request $request)
    {
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');
        $eqid = 'NWS'.Article::MakeDBId();

        $rowRules = [
            'image' => 'required|mimes:jpeg,png|max:1024',
            'categories' => 'required',
            'tags' => 'required'
        ];

        foreach($alllangs as $lang) {
            $langRules = [
                'title_'.$lang => 'required',
                'description_'.$lang => 'required',
                'meta_title_'.$lang => 'required',
                'meta_keyword_'.$lang => 'required',
                'meta_description_'.$lang => 'required',
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            $data = [];
            $data['title'] = $request->{'title_'.$lang};
            $data['description'] = $request->{'description_'.$lang};
            $data['equal_id'] = $eqid;
            $data['lang'] =$lang;
            $data['meta_title'] = $request->{'meta_title_'.$lang};
            $data['meta_keyword'] = $request->{'meta_keyword_'.$lang};
            $data['meta_description'] = $request->{'meta_description_'.$lang};
            $data['slug'] = str_slug($request->{'title_'.$lang});
          

            if ($lang == $deflang) {
                if ($request->hasFile('image')) {
                    $myimage = new MyImage();
                    $data['image'] = $myimage->saveImage($request->file('image'), $request->{'title_'.$lang});
                }
            } else {
                $data['image'] = ${'data_'.$deflang}->image;
            }

            ${'data_'.$lang} = News::create($data);

            ${'categories_'.$lang} = NewsCategories::whereIn('equal_id', $request->categories)->where('lang', $lang)->pluck('id');
            ${'data_'.$lang}->categories()->sync(${'categories_'.$lang});

            ${'tags_'.$lang} = NewsTags::whereIn('equal_id', $request->tags)->where('lang', $lang)->pluck('id');
            ${'data_'.$lang}->tags()->sync(${'tags_'.$lang});
        }

        \Session::flash('notification', ['level' => 'success', 'message' => 'News '.${'data_'.$deflang}->title.' saved.']);
        return redirect()->route('admin.config.news.index');
    }

    public function edit($id){
		 $news = News::where('equal_id',$id)->get();
		 return view('admin.webconfig.news.edit', compact('news'));
	}

	 public function update(Request $request, $equalid)
    {
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');

        $news = News::where('equal_id', $equalid)->get();

        $rowRules = [
            'image' => 'mimes:jpeg,png|max:1024',
            'categories' => 'required',
            'tags' => 'required'
        ];

        foreach($alllangs as $lang) {
            $langRules = [
                'title_'.$lang => 'required',
                'description_'.$lang => 'required',
                'meta_title_'.$lang => 'required',
                'meta_keyword_'.$lang => 'required',
                'meta_description_'.$lang => 'required',
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            ${'data_'.$lang} = $news->where('lang', $lang)->first();

            $data = [];
            $data['title'] = $request->{'title_'.$lang};
            $data['description'] = $request->{'description_'.$lang};
            $data['meta_title'] = $request->{'meta_title_'.$lang};
            $data['meta_keyword'] = $request->{'meta_keyword_'.$lang};
            $data['meta_description'] = $request->{'meta_description_'.$lang};
            $data['slug'] = str_slug($request->{'title_'.$lang});
           
           
            if ($lang == $deflang) {
                if ($request->hasFile('image')) {
                    $myimage = new MyImage();
                    if (${'data_'.$deflang}->image != '') {
                        $myimage->deleteImage(${'data_'.$deflang}->image);
                    }
                    $data['image'] = $myimage->saveImage($request->file('image'), $request->{'title_'.$lang});
                }
            } else {
                $data['image'] = ${'data_'.$deflang}->image;
            }

            ${'data_'.$lang}->update($data);

            ${'categories_'.$lang} = NewsCategories::whereIn('equal_id', $request->categories)->where('lang', $lang)->pluck('id');
            ${'data_'.$lang}->categories()->sync(${'categories_'.$lang});

            ${'tags_'.$lang} = NewsTags::whereIn('equal_id', $request->tags)->where('lang', $lang)->pluck('id');
            ${'data_'.$lang}->tags()->sync(${'tags_'.$lang});
        }

         \Session::flash('notification', ['level' => 'success', 'message' =>  ${'data_'.$deflang}->title.' '.${'data_'.$deflang}->name.' updated.']);
        return redirect()->route('admin.config.news.index');
    }

    public function destroy($equal_id){
    	$images = News::where('equal_id', $equal_id)->groupBy('image')->pluck('image');
        foreach ($images as $image) {
            if ($image !== '') {
                $myimage = new MyImage();
                $myimage->deleteImage($image);
            }
        }
    	 News::where('equal_id', $equal_id)->delete();
        \Session::flash('notification', ['level' => 'success', 'message' => 'News deleted.']);
       	return redirect()->route('admin.config.news.index');
    }

}
