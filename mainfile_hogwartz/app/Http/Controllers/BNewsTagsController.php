<?php

namespace App\Http\Controllers;

use App\NewsTags;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BNewsTagsController extends Controller
{
    public function create() {
        return view('admin.webconfig.news.tag.create');
    }

    public function store(Request $request) {
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');

        $eqid = uniqid('NT', true);
        $rowRules = [];
        foreach($alllangs as $lang) {
            $langRules = [
                'tag_'.$lang => 'required',
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            $data = [];
            $data['tag'] = $request->{'tag_'.$lang};
            $data['equal_id'] = $eqid;
            $data['lang'] =$lang;
            $data['slug'] = str_slug($request->{'tag_'.$lang});

            ${'data_'.$lang} = NewsTags::create($data);
        }

        \Session::flash('notification', ['level' => 'success', 'message' => 'News tag "'.${'data_'.$deflang}->tag.'" saved.']);
        return redirect()->route('admin.config.news.index');
    }

    public function edit($equal_id) {
        $tags = NewsTags::where('equal_id', $equal_id)->get();
        return view('admin.webconfig.news.tag.edit', compact('tags'));
    }

    public function update(Request $request, $equal_id) {
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');
        $tags = NewsTags::where('equal_id', $equal_id)->get();

        $rowRules = [];
        foreach($alllangs as $lang) {
            $langRules = [
                'tag_'.$lang => 'required',
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            ${'tag_'.$lang} = $tags->where('lang', $lang)->first();

            $data = [];
            $data['tag'] = $request->{'tag_'.$lang};
            $data['slug'] = str_slug($request->{'tag_'.$lang});

            ${'tag_'.$lang}->update($data);
        }

        \Session::flash('notification', ['level' => 'success', 'message' =>  'News tag "'.${'tag_'.$deflang}->tag.'" updated.']);
        return redirect()->route('admin.config.news.index');
    }

    public function destroy($equal_id){
        NewsTags::where('equal_id', $equal_id)->delete();
        \Session::flash('notification', ['level' => 'success', 'message' => 'News tag deleted.']);
        return redirect()->route('admin.config.news.index');
    }
}
