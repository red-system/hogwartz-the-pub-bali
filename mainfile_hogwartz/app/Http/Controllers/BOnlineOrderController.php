<?php

namespace App\Http\Controllers;

use App\OnlineOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Email;
use App\Mail\MailReservationConfirmForm;
use App\Mail\MailReservationCompleteForm;

class BOnlineOrderController extends Controller
{
    public function index(Request $request) {
        $status = $request->input('status');
        $status = $status ? $status : 'all';
        if($status !==  'all') {
            $onlineorders = OnlineOrder
                ::where([
                    'status'=>$status
                ])
                ->orderBy('id', 'DESC')
                ->get();
        } else {
            $onlineorders = OnlineOrder
                ::orderBy('id', 'DESC')
                ->get();
        }

        return view('admin.onlineorder.index', compact('onlineorders', 'status'));
    }

    public function edit($id) {
        $order = OnlineOrder::findOrFail($id);
        return view('admin.onlineorder.edit', compact('order'));
    }

    public function update(Request $request, $id) {
        $this->validate($request, [
            'status' => 'required|in:'.implode(',', OnlineOrder::allowedStatus())
        ]);

        $order = OnlineOrder::findOrFail($id);
        $order->update(['status' => $request->status]);

        // dd($order->email);

        // Kirim email edit status confirmed
        $emails = Email::where('receipt',1)->get();
        if($request->status == "confirmed"){
                \Mail::to($order->email)->queue(new MailReservationConfirmForm($order));

        // Kirim email status complete
        } elseif($request->status == "complete"){
                \Mail::to($order->email)->queue(new MailReservationCompleteForm($order));
        }

        \Session::flash('notification', ['level' => 'success', 'message' => 'Order has been updated']);
        return redirect()->route('onlineorder.index');
    }

    public function destroy($id) {
        $order = OnlineOrder::findOrFail($id);
        $order->delete();

        \Session::flash('notification', ['level' => 'success', 'message' => 'Order has been deleted']);
        return redirect()->route('onlineorder.index');
    }
}
