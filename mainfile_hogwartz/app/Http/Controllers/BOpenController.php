<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WebConfig;

class BOpenController extends Controller
{
    public function index(){

    	//AM
    	for ($i=1; $i <= 12; $i++) { 
    		$AM[$i . 'AM'] = $i . "AM";
    		$PM[$i . 'PM'] = $i . "PM";

    	}

    	$monday_open = WebConfig::where('config_name', 'monday_open')->first()->value;
    	$monday_close = WebConfig::where('config_name', 'monday_close')->first()->value;
    	$tuesday_open = WebConfig::where('config_name', 'tuesday_open')->first()->value;
    	$tuesday_close = WebConfig::where('config_name', 'tuesday_close')->first()->value;
    	$wednesday_open = WebConfig::where('config_name', 'wednesday_open')->first()->value;
    	$wednesday_close = WebConfig::where('config_name', 'wednesday_close')->first()->value;
    	$thursday_open = WebConfig::where('config_name', 'thursday_open')->first()->value;
    	$thursday_close = WebConfig::where('config_name', 'thursday_close')->first()->value;
    	$friday_open = WebConfig::where('config_name', 'friday_open')->first()->value;
    	$friday_close = WebConfig::where('config_name', 'friday_close')->first()->value;
    	$saturday_open = WebConfig::where('config_name', 'saturday_open')->first()->value;
    	$saturday_close = WebConfig::where('config_name', 'saturday_close')->first()->value;
    	$sunday_open = WebConfig::where('config_name', 'sunday_open')->first()->value;
    	$sunday_close = WebConfig::where('config_name', 'sunday_close')->first()->value;

    	return view('admin.webconfig.open.index',[
    		'monday_open' => $monday_open,
    		'monday_close' => $monday_close,
    		'tuesday_open' => $tuesday_open,
    		'tuesday_close' => $tuesday_close,
    		'wednesday_open' => $wednesday_open,
    		'wednesday_close' => $wednesday_close,
    		'thursday_open' => $thursday_open,
    		'thursday_close' => $thursday_close,
    		'friday_open' => $friday_open,
    		'friday_close' => $friday_close,
    		'saturday_open' => $saturday_open,
    		'saturday_close' => $saturday_close,
    		'sunday_open' => $sunday_open,
    		'sunday_close' => $sunday_close,
    	]);
    }

    public function update(Request $request){

    	$monday_open = WebConfig::where('config_name', 'monday_open')->first();
    	$monday_open->value = $request->monday_open;
    	$monday_open->save();

    	$monday_close = WebConfig::where('config_name', 'monday_close')->first();
    	$monday_close->value = $request->monday_close;
    	$monday_close->save();
    	
    	$tuesday_open = WebConfig::where('config_name', 'tuesday_open')->first();
    	$tuesday_open->value = $request->tuesday_open;
    	$tuesday_open->save();
    	
    	$tuesday_close = WebConfig::where('config_name', 'tuesday_close')->first();
    	$tuesday_close->value = $request->tuesday_close;
    	$tuesday_close->save();
    	
    	$wednesday_open = WebConfig::where('config_name', 'wednesday_open')->first();
    	$wednesday_open->value = $request->wednesday_open;
    	$wednesday_open->save();
    	
    	$wednesday_close = WebConfig::where('config_name', 'wednesday_close')->first();
    	$wednesday_close->value = $request->wednesday_close;
    	$wednesday_close->save();
    	
    	$thursday_open = WebConfig::where('config_name', 'thursday_open')->first();
    	$thursday_open->value = $request->thursday_open;
    	$thursday_open->save();
    	
    	$thursday_close = WebConfig::where('config_name', 'thursday_close')->first();
    	$thursday_close->value = $request->thursday_close;
    	$thursday_close->save();
    	
    	$friday_open = WebConfig::where('config_name', 'friday_open')->first();
    	$friday_open->value = $request->friday_open;
    	$friday_open->save();
    	
    	$friday_close = WebConfig::where('config_name', 'friday_close')->first();
    	$friday_close->value = $request->friday_close;
    	$friday_close->save();
    	
    	$saturday_open = WebConfig::where('config_name', 'saturday_open')->first();
    	$saturday_open->value = $request->saturday_open;
    	$saturday_open->save();
    	
    	$saturday_close = WebConfig::where('config_name', 'saturday_close')->first();
    	$saturday_close->value = $request->saturday_close;
    	$saturday_close->save();
    	
    	$sunday_open = WebConfig::where('config_name', 'sunday_open')->first();
    	$sunday_open->value = $request->sunday_open;
    	$sunday_open->save();
    	
    	$sunday_close = WebConfig::where('config_name', 'sunday_close')->first();
    	$sunday_close->value = $request->sunday_close;
    	$sunday_close->save();

    	\Session::flash('notification', ['level' => 'success', 'message' =>  'Opening & closing hours updated.']);
        return redirect()->route('admin.config.open.index');
    	
    }
}
