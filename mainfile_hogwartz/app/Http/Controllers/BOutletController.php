<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Outlet;
use App\Article;
use App\Services\MyImage;



class BOutletController extends Controller
{
  	public function index(){
      $outlet = Outlet::where('lang', config('app.default_locale'))->get();
      return view('admin.webconfig.outlet.index', compact('outlet'));
    }

    public function create()
    {
        return view('admin.webconfig.outlet.create');
    }

    public function store(Request $request)
    {
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');
        $eqid = 'OT'.Article::MakeDBId();

        $rowRules = [
            'image' => 'required|mimes:jpeg,png|max:1024',
            'lat' => 'required',
            'long' => 'required',
            'email' => 'required|email'
        ];
        foreach($alllangs as $lang) {
            $langRules = [
                'title_'.$lang => 'required',
                'description_'.$lang => 'required',
                'address_info_'.$lang => 'required',
                'meta_title_'.$lang => 'required',
                'meta_keyword_'.$lang => 'required',
                'meta_description_'.$lang => 'required',
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            $data = [];
            $data['title'] = $request->{'title_'.$lang};
            $data['description'] = $request->{'description_'.$lang};
            $data['lat'] = $request->lat;
            $data['lng'] = $request->long;
            $data['address_info'] = $request->{'address_info_'.$lang};
            $data['slug'] = str_slug($request->{'title_'.$lang});
            $data['lang'] = $lang;
            $data['equal_id'] = $eqid;
            $data['meta_title'] = $request->{'meta_title_'.$lang};
            $data['meta_keyword'] = $request->{'meta_keyword_'.$lang};
            $data['meta_description'] = $request->{'meta_description_'.$lang};
            $data['email'] = $request->email;

            if ($lang == $deflang) {
                if ($request->hasFile('image')) {
                    $myimage = new MyImage();
                    $data['image'] = $myimage->saveImage($request->file('image'), $request->{'title_'.$lang});
                }
            } else {
                $data['image'] = ${'image_'.$deflang}->image;
            }

            ${'image_'.$lang} = Outlet::create($data);
        }

        \Session::flash('notification', ['level' => 'success', 'message' => 'Outlet '.${'image_'.$deflang}->name.' saved.']);
       	return redirect()->route('admin.config.outlet.index');
    }

    public function edit($id){
        $outlet = Outlet::where('equal_id',$id)->get();
        return view('admin.webconfig.outlet.edit', compact('outlet'));
    }

    public function update(Request $request, $equalid)
    {
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');
        $images = Outlet::where('equal_id', $equalid)->get();

        $rowRules = [
            'image' => 'mimes:jpeg,png|max:1024',
            'lat' => 'required',
            'long' => 'required',
            'email' => 'required|email'
        ];
        foreach($alllangs as $lang) {
            $langRules = [
                'title_'.$lang => 'required',
                'description_'.$lang => 'required',
                'address_info_'.$lang => 'required',
                'meta_title_'.$lang => 'required',
                'meta_keyword_'.$lang => 'required',
                'meta_description_'.$lang => 'required',
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);


        foreach($alllangs as $lang) {
            ${'image_'.$lang} = $images->where('lang', $lang)->first();

            $data = [];
            $data['title'] = $request->{'title_'.$lang};
            $data['description'] = $request->{'description_'.$lang};
            $data['lat'] = $request->lat;
            $data['lng'] = $request->long;
            $data['address_info'] = $request->{'address_info_'.$lang};
            $data['slug'] = str_slug($request->{'title_'.$lang});
            $data['meta_title'] = $request->{'meta_title_'.$lang};
            $data['meta_keyword'] = $request->{'meta_keyword_'.$lang};
            $data['meta_description'] = $request->{'meta_description_'.$lang};
            $data['email'] = $request->email;
           
            if ($lang == $deflang) {
                if ($request->hasFile('image')) {
                    $myimage = new MyImage();
                    if (${'image_'.$deflang}->image != '') {
                        $myimage->deleteImage(${'image_'.$deflang}->image);
                    }
                    $data['image'] = $myimage->saveImage($request->file('image'), $request->{'title_'.$lang});
                }
            } else {
                $data['image'] = ${'image_'.$deflang}->image;
            }

        

            ${'image_'.$lang}->update($data);
        }

         \Session::flash('notification', ['level' => 'success', 'message' => 'Outlet '.${'image_'.$deflang}->name.' updated.']);
        return redirect()->route('admin.config.outlet.index');
    }

    public function destroy($equal_id){

    	$images = Outlet::where('equal_id', $equal_id)->groupBy('image')->pluck('image');
        foreach ($images as $image) {
            if ($image !== '') {
                $myimage = new MyImage();
                $myimage->deleteImage($image);
            }
        }
    	 Outlet::where('equal_id', $equal_id)->delete();
        \Session::flash('notification', ['level' => 'success', 'message' => 'Outlet deleted.']);
       	return redirect()->route('admin.config.outlet.index');

    }







}
