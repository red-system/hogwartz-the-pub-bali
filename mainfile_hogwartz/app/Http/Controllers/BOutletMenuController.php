<?php

namespace App\Http\Controllers;

use App\Menus;
use App\Outlet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BOutletMenuController extends Controller
{
    public function index($equal_id) {
        $outlet = Outlet::where('equal_id',$equal_id)->where('lang', config('app.default_locale'))->firstOrFail();
        $allmenus = Menus::where('lang', config('app.default_locale'))->get();
        $outletmenu = $outlet->menus()->pluck('menus.id')->toArray();
        return view('admin.webconfig.outlet.menu.index', compact('outlet', 'allmenus', 'outletmenu'));
    }

    public function update(Request $request, $equal_id) {
        $this->validate($request, [
            'menu' => 'required'
        ]);

        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');

        foreach($alllangs as $lang) {
            ${'outlet_'.$lang} = Outlet::where('equal_id', $equal_id)->where('lang', $lang)->first();
            if (!empty(${'outlet_'.$lang})) {
                ${'menu_'.$lang} = Menus::whereIn('equal_id', $request->menu)->where('lang', $lang)->pluck('id');
                ${'outlet_'.$lang}->menus()->sync(${'menu_'.$lang});
            }
        }

        \Session::flash('notification', ['level' => 'success', 'message' => 'Outlet menu updated.']);
        return back();
    }
}
