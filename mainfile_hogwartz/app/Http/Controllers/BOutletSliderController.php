<?php

namespace App\Http\Controllers;

use App\Images;
use App\Outlet;
use App\Services\MyImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BOutletSliderController extends Controller
{
    public function index($equal_id) {
        $outlet = Outlet::where('equal_id', $equal_id)->where('lang', config('app.default_locale'))->firstOrFail();
        $sliders = $outlet->images()->where('type', 'outlet-slider')->where('lang', config('app.default_locale'))->get();
        return view('admin.webconfig.outlet.slider.index', compact('outlet', 'sliders'));
    }

    public function create($equal_id) {
        $outlet = Outlet::where('equal_id', $equal_id)->where('lang', config('app.default_locale'))->firstOrFail();
        return view('admin.webconfig.outlet.slider.create', compact('outlet'));
    }

    public function store(Request $request, $equal_id) {
        $outlet = Outlet::where('equal_id', $equal_id)->get();
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');
        $eqid = uniqid('IMG', true);

        $rowRules = [
            'image' => 'required|mimes:jpeg,png|max:1024'
        ];

        foreach($alllangs as $lang) {
            $langRules = [
                'name_'.$lang => 'required',
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            $data = [];
            $data['name'] = $request->{'name_'.$lang};
            $data['lang'] = $lang;
            $data['equal_id'] = $eqid;
            $data['type'] = 'outlet-slider';
            $data['outlet_id'] = $outlet->where('lang', $lang)->first()->id;

            if ($lang == $deflang) {
                if ($request->hasFile('image')) {
                    $myimage = new MyImage();
                    $data['image'] = $myimage->saveImage($request->file('image'), $request->{'name_'.$lang});
                }
            } else {
                $data['image'] = ${'image_'.$deflang}->image;
            }

            ${'image_'.$lang} = Images::create($data);
        }

        \Session::flash('notification', ['level' => 'success', 'message' => 'Outlet slider saved.']);
        return redirect()->route('slider-outlet.index', $outlet->first()->equal_id);
    }

    public function edit($equal_id) {
        $slider = Images::where('type', 'outlet-slider')->where('equal_id', $equal_id)->get();
        return view('admin.webconfig.outlet.slider.edit', compact('slider'));
    }

    public function update(Request $request, $equal_id) {
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');

        $slider = Images::where('type', 'outlet-slider')->where('equal_id', $equal_id)->get();
        $rowRules = [
            'image' => 'mimes:jpeg,png|max:1024'
        ];
        foreach($alllangs as $lang) {
            $langRules = [
                'name_'.$lang => 'required'
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            ${'slider_'.$lang} = $slider->where('lang', $lang)->first();

            $data = [];
            $data['name'] = $request->{'name_'.$lang};

            if ($lang == $deflang) {
                if ($request->hasFile('image')) {
                    $myimage = new MyImage();
                    if (${'slider_'.$deflang}->image != '') {
                        $myimage->deleteImage(${'slider_'.$deflang}->image);
                    }
                    $data['image'] = $myimage->saveImage($request->file('image'), $request->{'name_'.$lang});
                }
            } else {
                $data['image'] = ${'slider_'.$deflang}->image;
            }
            ${'slider_'.$lang}->update($data);
        }

        \Session::flash('notification', ['level' => 'success', 'message' => 'Outlet slider updated.']);
        return redirect()->route('slider-outlet.index', ${'slider_'.$lang}->outlet->equal_id);
    }

    public function destroy($equal_id) {
        $images = Images::where('type', 'outlet-slider')->where('equal_id', $equal_id)->groupBy('image')->pluck('image');
        foreach ($images as $image) {
            if ($image !== '') {
                $myimage = new MyImage();
                $myimage->deleteImage($image);
            }
        }
        $outlet_eqid = Images::where('type', 'outlet-slider')->where('equal_id', $equal_id)->where('lang', config('app.default_locale'))->first()->outlet->equal_id;
        Images::where('type', 'outlet-slider')->where('equal_id', $equal_id)->delete();
        \Session::flash('notification', ['level' => 'success', 'message' => 'Outlet slider deleted.']);
        return redirect()->route('slider-outlet.index', $outlet_eqid);
    }
}
