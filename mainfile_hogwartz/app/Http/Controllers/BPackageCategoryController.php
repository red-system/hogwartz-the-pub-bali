<?php

namespace App\Http\Controllers;

use App\Article;
use App\Services\MyImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BPackageCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pcategories = Article::where('position', 'sub-menu-utama')->where('lang', config('app.default_locale'))->where('link', \Lang::get('route.package',[], config('app.default_locale')))->get();
        return view('admin.webconfig.packages.category.index', compact('pcategories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.webconfig.packages.category.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');
        $eqid = uniqid('ART', true);

        $rowRules = [
            'thumb_image' => 'required|mimes:jpeg,png|max:1024',
            'published' => 'required|in:1,0'
        ];

        foreach($alllangs as $lang) {
            $langRules = [
                'title_'.$lang => 'required',
                'short_description_'.$lang => 'required',
                'description_'.$lang => 'required',
                'meta_title_'.$lang => 'required',
                'meta_keyword_'.$lang => 'required',
                'meta_description_'.$lang => 'required',
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            $data = [];
            $data['title'] = $request->{'title_'.$lang};
            $data['short_description'] = $request->{'short_description_'.$lang};
            $data['conten'] = $request->{'description_'.$lang};
            $data['meta_title'] = $request->{'meta_title_'.$lang};
            $data['meta_keyword'] = $request->{'meta_keyword_'.$lang};
            $data['meta_description'] = $request->{'meta_description_'.$lang};
            $data['position'] = 'sub-menu-utama';
            $data['published'] = $request->published;
            $data['link'] = \Lang::get('route.package',[], $lang);
            $data['slug'] = str_slug($request->{'title_'.$lang});

            $articlepackage = Article::where('link', \Lang::get('route.package',[], $lang))->where('position', 'menu-utama')->where('lang', $lang)->first();
            $data['parent_id'] = $articlepackage->id;
            $data['lang'] = $lang;
            $data['equal_id'] = $eqid;


            if ($lang == $deflang) {
                if ($request->hasFile('thumb_image')) {
                    $myimage = new MyImage();
                    $data['thumb_image'] = $myimage->saveImage($request->file('thumb_image'), $request->{'title_'.$lang});
                }
            } else {
                $data['thumb_image'] = ${'package_'.$deflang}->thumb_image;
            }

            ${'package_'.$lang} = Article::create($data);
        }

        \Session::flash('notification', ['level' => 'success', 'message' => 'Packages '.${'package_'.$deflang}->title.' saved.']);
        return redirect()->route('admin.config.packages.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $pcategory = Article::where('position', 'sub-menu-utama')->where('equal_id', $id)->get();
        return view('admin.webconfig.packages.category.edit', compact('pcategory'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');

        $pcategory = Article::where('position', 'sub-menu-utama')->where('equal_id', $id)->get();
        $rowRules = [
            'thumb_image' => 'mimes:jpeg,png|max:1024',
            'published' => 'required|in:1,0'
        ];

        foreach($alllangs as $lang) {
            $langRules = [
                'title_'.$lang => 'required',
                'short_description_'.$lang => 'required',
                'description_'.$lang => 'required',
                'meta_title_'.$lang => 'required',
                'meta_keyword_'.$lang => 'required',
                'meta_description_'.$lang => 'required',
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            ${'pcat_'.$lang} = $pcategory->where('lang', $lang)->first();

            $data = [];
            $data['title'] = $request->{'title_'.$lang};
            $data['short_description'] = $request->{'short_description_'.$lang};
            $data['conten'] = $request->{'description_'.$lang};
            $data['meta_title'] = $request->{'meta_title_'.$lang};
            $data['meta_keyword'] = $request->{'meta_keyword_'.$lang};
            $data['meta_description'] = $request->{'meta_description_'.$lang};
            $data['published'] = $request->published;
            $data['slug'] = str_slug($request->{'title_'.$lang});

            if ($lang == $deflang) {
                if ($request->hasFile('thumb_image')) {
                    $myimage = new MyImage();
                    if (${'pcat_'.$deflang}->thumb_image != '') {
                        $myimage->deleteImage(${'pcat_'.$deflang}->thumb_image);
                    }
                    $data['thumb_image'] = $myimage->saveImage($request->file('thumb_image'), $request->{'title_'.$lang});
                }
            } else {
                $data['thumb_image'] = ${'pcat_'.$deflang}->thumb_image;
            }

            ${'pcat_'.$lang}->update($data);
        }

        \Session::flash('notification', ['level' => 'success', 'message' => 'Package '.${'pcat_'.$deflang}->title.' updated.']);
        return redirect()->route('admin.config.packages.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $images = Article::where('equal_id', $id)->groupBy('thumb_image')->pluck('thumb_image');
        foreach ($images as $image) {
            if ($image !== '') {
                $myimage = new MyImage();
                $myimage->deleteImage($image);
            }
        }
        Article::where('equal_id', $id)->where('position', 'sub-menu-utama')->delete();
        \Session::flash('notification', ['level' => 'success', 'message' => 'Package deleted.']);
        return redirect()->route('admin.config.packages.index');
    }
}
