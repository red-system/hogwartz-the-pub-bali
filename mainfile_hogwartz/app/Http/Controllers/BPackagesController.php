<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Packages;
use App\Services\MyImage;

class BPackagesController extends Controller
{

   public function index($equal_id) {
        $articlepackage = Article::where('link', \Lang::get('route.package',[], config('app.default_locale')))->where('position', 'sub-menu-utama')->where('lang', config('app.default_locale'))->where('equal_id', $equal_id)->firstOrFail();
        $packages = $articlepackage->packages()->where('lang', config('app.default_locale'))->get();
        return view('admin.webconfig.packages.index', compact('packages','article','articlepackage'));
    }


   public function createRecommend(){
        $list_packages = Article::where('featured', '0')->where('position', 'sub-menu-utama')->where('lang', config('app.default_locale'))->where('link', \Lang::get('route.package',[], config('app.default_locale')))->pluck('title','equal_id');
        return view('admin.webconfig.packages.create_recommend', compact('list_packages'));
    }

   public function create($equal_id)
    {
        $articlepackage = Article::where('link', \Lang::get('route.package',[], config('app.default_locale')))->where('position', 'sub-menu-utama')->where('lang', config('app.default_locale'))->where('equal_id', $equal_id)->firstOrFail();
        return view('admin.webconfig.packages.create', compact('articlepackage'));
    }

    public function store(Request $request, $equal_id)
    {
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');
        $eqid = 'IMG'.Article::MakeDBId();

        $rowRules = [
            'image' => 'required|mimes:jpeg,png|max:1024',
        ];
        foreach($alllangs as $lang) {
            $langRules = [
                'title_'.$lang => 'required',
                'description_'.$lang => 'required',
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            $data = [];
            $data['title'] = $request->{'title_'.$lang};
            $data['description'] = $request->{'description_'.$lang};
            $data['price'] = $request->price;
            $data['lang'] = $lang;
            $data['equal_id'] = $eqid;

            ${'articlepackage_'.$lang} = Article::where('link', \Lang::get('route.package',[], $lang))->where('position', 'sub-menu-utama')->where('lang', $lang)->where('equal_id', $equal_id)->first();
            $data['article_id'] = ${'articlepackage_'.$lang}->id;

            if ($lang == $deflang) {
                if ($request->hasFile('image')) {
                    $myimage = new MyImage();
                    $data['image'] = $myimage->saveImage($request->file('image'), $request->{'title_'.$lang});
                }
            } else {
                $data['image'] = ${'image_'.$deflang}->image;
            }

            ${'image_'.$lang} = Packages::create($data);
        }

        \Session::flash('notification', ['level' => 'success', 'message' => 'Packages '.${'image_'.$deflang}->name.' saved.']);
       	return redirect()->route('packageslist.index', $equal_id);
    }

    public function destroy($equal_id){
    	$images = Packages::where('equal_id', $equal_id)->groupBy('image')->pluck('image');
        foreach ($images as $image) {
            if ($image !== '') {
                $myimage = new MyImage();
                $myimage->deleteImage($image);
            }
        }
        $eqid = Packages::where('equal_id', $equal_id)->first()->pcategory->equal_id;
    	 Packages::where('equal_id', $equal_id)->delete();
        \Session::flash('notification', ['level' => 'success', 'message' => 'Package deleted.']);
       	return redirect()->route('packageslist.index', $eqid);

    }
    public function edit($id){
        $packages = Packages::where('equal_id',$id)->get();
        return view('admin.webconfig.packages.edit', compact('packages'));
    }

    public function update(Request $request, $equalid)
    {
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');

        $images = Packages::where('equal_id', $equalid)->get();

        $rowRules = [
            'image' => 'mimes:jpeg,png|max:1024',
        ];
        foreach($alllangs as $lang) {
            $langRules = [
                'title_'.$lang => 'required',
                'description_'.$lang => 'required',
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            ${'image_'.$lang} = $images->where('lang', $lang)->first();

            $data = [];
            $data['title'] = $request->{'title_'.$lang};
            $data['description'] = $request->{'description_'.$lang};
           
            if ($lang == $deflang) {
                if ($request->hasFile('image')) {
                    $myimage = new MyImage();
                    if (${'image_'.$deflang}->image != '') {
                        $myimage->deleteImage(${'image_'.$deflang}->image);
                    }
                    $data['image'] = $myimage->saveImage($request->file('image'), $request->{'title_'.$lang});
                }
            } else {
                $data['image'] = ${'image_'.$deflang}->image;
            }

            ${'image_'.$lang}->update($data);
        }

         \Session::flash('notification', ['level' => 'success', 'message' => 'Package '.${'image_'.$deflang}->name.' updated.']);
        return redirect()->route('packageslist.index', ${'image_'.$lang}->pcategory->equal_id);
    }

    public function updateRecommend(Request $request){
        $this->validate($request, [
            'package' => 'required|exists:article,equal_id,position,sub-menu-utama'
        ]);
        $equal_id = $request->package;
        $recommend = '1';
        
        $update = Article::where('equal_id',$equal_id)->update([
                'featured' => $recommend
            ]);

          \Session::flash('notification', ['level' => 'success', 'message' => 'Recommend Package  updated.']);
        return redirect()->route('admin.config.home-konten.index');
    }

    public function destroyRecommend($id){
        $equal_id = $id;
        $recommend = '0';
        
        $update = Article::where('equal_id',$equal_id)->update([
            'featured' => $recommend
        ]);
       \Session::flash('notification', ['level' => 'success', 'message' => 'Recommend Package  deleted.']);
        return redirect()->route('admin.config.home-konten.index');
    }
}
