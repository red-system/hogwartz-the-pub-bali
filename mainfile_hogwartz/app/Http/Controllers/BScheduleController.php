<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Schedule;
use App\Article;
use App\Services\MyImage;

class BScheduleController extends Controller
{
    public function index(){
     	$schedules = Schedule::where('lang', config('app.default_locale'))->orderBy('id', 'DESC')->get();
	      return view('admin.webconfig.schedule.index', compact('schedules'));
    }

    public function create(){
    	return view('admin.webconfig.schedule.create');
    }

    public function store(Request $request){
    	$alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');
        $eqid = 'NWS'.Article::MakeDBId();

        $rowRules = [
            'image' => 'required|mimes:jpeg,png|max:1500',
        ];

        foreach($alllangs as $lang) {
            $langRules = [
                'title_'.$lang => 'required',
                'content_'.$lang => 'required',
                'schedule_'.$lang => 'required',
                'meta_title_'.$lang => 'required',
                'meta_keyword_'.$lang => 'required',
                'meta_description_'.$lang => 'required',
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }

        // $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            $data = [];
            $data['title'] = $request->{'title_'.$lang};
            $data['content'] = $request->{'content_'.$lang};
            $data['schedule'] = $request->{'schedule_'.$lang};
            $data['equal_id'] = $eqid;
            $data['lang'] =$lang;
            $data['meta_title'] = $request->{'meta_title_'.$lang};
            $data['meta_keyword'] = $request->{'meta_keyword_'.$lang};
            $data['meta_description'] = $request->{'meta_description_'.$lang};
            $data['slug'] = str_slug($request->{'title_'.$lang});          

            if ($lang == $deflang) {
                if ($request->hasFile('image')) {
                    $myimage = new MyImage();
                    $data['image'] = $myimage->saveImage($request->file('image'), $request->{'title_'.$lang});
                }
            } else {
                $data['image'] = ${'data_'.$deflang}->image;
            }

            // dd($data);

            ${'data_'.$lang} = Schedule::create($data);
        }

        \Session::flash('notification', ['level' => 'success', 'message' => 'Schedule '.${'data_'.$deflang}->title.' saved.']);
        return redirect()->route('admin.config.schedule.index');
    }

    public function edit($id){
    	$schedule = Schedule::where('equal_id',$id)->get();
		return view('admin.webconfig.schedule.edit', compact('schedule'));
    }

    public function update($equalid, Request $request){
    	$alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');

        $news = Schedule::where('equal_id', $equalid)->get();

        $rowRules = [
            'image' => 'mimes:jpeg,png|max:1500',
        ];

        foreach($alllangs as $lang) {
            $langRules = [
                'title_'.$lang => 'required',
                'content_'.$lang => 'required',
                'schedule_'.$lang => 'required',
                'meta_title_'.$lang => 'required',
                'meta_keyword_'.$lang => 'required',
                'meta_description_'.$lang => 'required',
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        // $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            ${'data_'.$lang} = $news->where('lang', $lang)->first();

            $data = [];
            $data['title'] = $request->{'title_'.$lang};
            $data['content'] = $request->{'content_'.$lang};
            $data['schedule'] = $request->{'schedule_'.$lang};
            $data['meta_title'] = $request->{'meta_title_'.$lang};
            $data['meta_keyword'] = $request->{'meta_keyword_'.$lang};
            $data['meta_description'] = $request->{'meta_description_'.$lang};
            $data['slug'] = str_slug($request->{'title_'.$lang});           
           
            if ($lang == $deflang) {
                if ($request->hasFile('image')) {
                    $myimage = new MyImage();
                    if (${'data_'.$deflang}->image != '') {
                        $myimage->deleteImage(${'data_'.$deflang}->image);
                    }
                    $data['image'] = $myimage->saveImage($request->file('image'), $request->{'title_'.$lang});
                }
            } else {
                $data['image'] = ${'data_'.$deflang}->image;
            }

            ${'data_'.$lang}->update($data);

        }

         \Session::flash('notification', ['level' => 'success', 'message' =>  ${'data_'.$deflang}->title.' '.${'data_'.$deflang}->name.' updated.']);
        return redirect()->route('admin.config.schedule.index');

    }

    public function destroy($equal_id){
    	$images = Schedule::where('equal_id', $equal_id)->groupBy('image')->pluck('image');
        foreach ($images as $image) {
            if ($image !== '') {
                $myimage = new MyImage();
                $myimage->deleteImage($image);
            }
        }
    	 Schedule::where('equal_id', $equal_id)->delete();
        \Session::flash('notification', ['level' => 'success', 'message' => 'Show Schedule deleted.']);
       	return redirect()->route('admin.config.schedule.index');
    }
}
