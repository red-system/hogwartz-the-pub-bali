<?php

namespace App\Http\Controllers;

use App\Services\MyImage;
use Illuminate\Http\Request;
use App\SisterCompany;
use App\Article;

class BSisterCompanyController extends Controller
{
    public function index(){
    	$company = SisterCompany::where('lang', config('app.default_locale'))->get();
        return view('admin.webconfig.sistercompany.index', compact('company'));
    }

    public function create(){
        return view('admin.webconfig.sistercompany.create');
    }

    public function store(Request $request){
    	$alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');
        $eqid = 'LK'.Article::MakeDBId();

        $rowRules = [
            'link' => 'required|url',
            'published' => 'required|in:1,0',
            'image' => 'required|mimes:jpeg,png|max:1024'
        ];

        foreach($alllangs as $lang) {
            $langRules = [
                'title_'.$lang => 'required',
                'short_description_'.$lang => 'required',
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            $data = [];
            $data['title'] = $request->{'title_'.$lang};
            $data['short_description'] = $request->{'short_description_'.$lang};
           	$data['published'] = $request->published;
            $data['lang'] = $lang;
            $data['link'] = $request->link;
            $data['equal_id'] = $eqid;

            if ($lang == $deflang) {
                if ($request->hasFile('image')) {
                    $myimage = new MyImage();
                    $data['image'] = $myimage->saveImage($request->file('image'), $request->{'title_'.$lang});
                }
            } else {
                $data['image'] = ${'data_'.$deflang}->image;
            }

            ${'data_'.$lang} = SisterCompany::create($data);
        }

        \Session::flash('notification', ['level' => 'success', 'message' => 'Packages '.${'data_'.$deflang}->name.' saved.']);
       	return redirect()->route('admin.config.sistercompany.index');
    }

    public function edit($id){
        $company = SisterCompany::where('equal_id',$id)->get();
        return view('admin.webconfig.sistercompany.edit', compact('article','company'));
    }

     public function update(Request $request, $equalid)
    {
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');

        $company = SisterCompany::where('equal_id', $equalid)->get();
        $rowRules = [
            'link' => 'required|url',
            'published' => 'required|in:1,0',
           'image' => 'mimes:jpeg,png|max:1024'
        ];

        foreach($alllangs as $lang) {
            $langRules = [
               'title_'.$lang => 'required',
                'short_description_'.$lang => 'required',
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);


        foreach($alllangs as $lang) {
            ${'data_'.$lang} = $company->where('lang', $lang)->first();

            $data = [];
            $data['title'] = $request->{'title_'.$lang};
            $data['short_description'] = $request->{'short_description_'.$lang};
            $data['published'] = $request->published;
            $data['lang'] = $lang;
            $data['link'] = $request->link;

            if ($lang == $deflang) {
                if ($request->hasFile('image')) {
                    $myimage = new MyImage();
                    if (${'data_'.$deflang}->image != '') {
                        $myimage->deleteImage(${'data_'.$deflang}->image);
                    }
                    $data['image'] = $myimage->saveImage($request->file('image'), $request->{'title_'.$lang});
                }
            } else {
                $data['image'] = ${'data_'.$deflang}->image;
            }

            ${'data_'.$lang}->update($data);
        }

         \Session::flash('notification', ['level' => 'success', 'message' => 'Sister Company '.${'data_'.$deflang}->name.' updated.']);
        return redirect()->route('admin.config.sistercompany.index');
    }

    public function destroy($equal_id){
        SisterCompany::where('equal_id', $equal_id)->delete();
        \Session::flash('notification', ['level' => 'success', 'message' => 'Sister Company deleted.']);
       	return redirect()->route('admin.config.sistercompany.index');
    }
}
