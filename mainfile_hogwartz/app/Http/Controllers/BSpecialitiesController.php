<?php

namespace App\Http\Controllers;

use App\Services\MyImage;
use App\Specialities;
use App\Article;
use Illuminate\Http\Request;


class BSpecialitiesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.webconfig.specialities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');
        $eqid = uniqid('SP', true);

        $rowRules = [
            'icon' => 'required|mimes:png,jpeg,jpg|max:300',
        ];
        foreach($alllangs as $lang) {
            $langRules = [
                'title_'.$lang => 'required',
                'description_'.$lang => 'required',
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            $data = [];
            $data['title'] = $request->{'title_'.$lang};
            $data['description'] = $request->{'description_'.$lang};
            $data['lang'] = $lang;
            $data['equal_id'] = $eqid;

            if ($lang == $deflang) {
                if ($request->hasFile('icon')) {
                    $myimage = new MyImage();
                    $data['icon'] = $myimage->saveImage($request->file('icon'), 'Icon '.$request->{'title_'.$lang});
                }
            } else {
                $data['icon'] = ${'specialties_'.$deflang}->icon;
            }

            ${'specialties_'.$lang} = Specialities::create($data);
        }

        \Session::flash('notification', ['level' => 'success', 'message' => 'Data specialties saved.']);
        return redirect()->route('admin.config.home-konten.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $special = Specialities::where('equal_id', $id)->get();
         return view('admin.webconfig.specialities.edit', compact('special'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $equalid)
    {
        $alllangs = config('app.all_langs');
        $deflang = config('app.default_locale');

        $special = Specialities::where('equal_id', $equalid)->get();

        $rowRules = [
            'icon' => 'mimes:png,jpeg,jpg|max:300'
        ];

        foreach($alllangs as $lang) {
            $langRules = [
                'title_'.$lang => 'required',
                'description_'.$lang => 'required',
            ];
            $rowRules = array_merge($rowRules, $langRules);
        }
        $this->validate($request, $rowRules);

        foreach($alllangs as $lang) {
            ${'special_'.$lang} = $special->where('lang', $lang)->first();

            $data = [];
            $data['title'] = $request->{'title_'.$lang};
            $data['description'] = $request->{'description_'.$lang};

            if ($lang == $deflang) {
                if ($request->hasFile('icon')) {
                    $myimage = new MyImage();
                    if (${'special_'.$lang}->icon != '') {
                        $myimage->deleteImage(${'special_'.$lang}->icon);
                    }
                    $data['icon'] = $myimage->saveImage($request->file('icon'), 'Icon '.$request->{'title_'.$lang});
                }
            } else {
                $data['icon'] = ${'special_'.$deflang}->icon;
            }
      
            ${'special_'.$lang}->update($data);
        }

         \Session::flash('notification', ['level' => 'success', 'message' => 'Data Specialities success updated.']);
        return redirect()->route('admin.config.home-konten.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($equal_id)
    {
        $icons = Specialities::where('equal_id', $equal_id)->groupBy('icon')->pluck('icon');
        foreach ($icons as $icon) {
            if ($icon !== '') {
                $myimage = new MyImage();
                $myimage->deleteImage($icon);
            }
        }
        Specialities::where('equal_id', $equal_id)->delete();
        \Session::flash('notification', ['level' => 'success', 'message' => 'Data specialities deleted.']);
        return redirect()->route('admin.config.home-konten.index');
    }
}
