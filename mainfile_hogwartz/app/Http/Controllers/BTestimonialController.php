<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Testimonial;
use App\Services\MyImage;

class BTestimonialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.webconfig.testimonial.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    	// dd($request);

        $rowRules = [
            'picture' => 'required|mimes:png,jpeg,jpg|max:300',
            'name'	=> 'required',
            'job'	=> 'required',
            'testimonial'	=> 'required'
        ];

        $this->validate($request, $rowRules);

        $testi = new Testimonial();
        $testi->name = $request->name;
        $testi->job = $request->job;
        $testi->testimonial = $request->testimonial;
        $testi->equal_id = uniqid('SP', true);

        if ($request->hasFile('picture')) {
            $myimage = new MyImage();
            $testi->picture = $myimage->saveImage($request->file('picture'), 'Picture ');
        }

        $testi->save();

        \Session::flash('notification', ['level' => 'success', 'message' => 'Data testimonial saved.']);
        return redirect()->route('admin.config.home-konten.index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $testimonial = Testimonial::where('equal_id', $id)->first();
         return view('admin.webconfig.testimonial.edit', compact('testimonial'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $equalid)
    {
    	$rowRules = [
            'picture' => 'mimes:png,jpeg,jpg|max:300',
            'name'	=> 'required',
            'job'	=> 'required',
            'testimonial'	=> 'required'
        ];

        $this->validate($request, $rowRules);

        $testi = Testimonial::where('equal_id', $equalid);
        $testi->name = $request->name;
        $testi->job = $request->job;
        $testi->testimonial = $request->testimonial;

        if ($request->hasFile('picture')) {
            $myimage = new MyImage();
            $testi->picture = $myimage->saveImage($request->file('picture'), 'Picture ');
        }

        $testi->save();

         \Session::flash('notification', ['level' => 'success', 'message' => 'Data Specialities success updated.']);
        return redirect()->route('admin.config.home-konten.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($equal_id)
    {
        $icons = Testimonial::where('equal_id', $equal_id)->groupBy('picture')->pluck('picture');
        foreach ($icons as $icon) {
            if ($icon !== '') {
                $myimage = new MyImage();
                $myimage->deleteImage($icon);
            }
        }
        Testimonial::where('equal_id', $equal_id)->delete();
        \Session::flash('notification', ['level' => 'success', 'message' => 'Data specialities deleted.']);
        return redirect()->route('admin.config.home-konten.index');
    }
}
