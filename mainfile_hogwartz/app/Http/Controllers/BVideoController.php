<?php

namespace App\Http\Controllers;

use App\VideoHome;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class BVideoController extends Controller
{
    public function update(Request $request, $id)
    {
        $video = VideoHome::where('id', $id)->firstOrFail();
        $this->validate($request, [
            'link' => 'required|url',
        ]);

        $data = $request->all();
        $video->update($data);
        \Session::flash('notification', ['level' => 'success', 'message' => 'Video Success updated.']);
        return redirect()->route('admin.config.home-konten.index');
    }
}
