<?php

namespace App\Http\Controllers;

use App\WebConfig;
use Illuminate\Http\Request;

class BWebConfigController extends Controller
{
    public function index() {
        $configs = WebConfig::all();
        return view('admin.config.index', compact('configs'));
    }

    public function update(Request $request) {
        $this->validate($request, [
            'email_config' => 'sometimes|required',
            'no_tlpconfig' => 'sometimes|required',
            'headertext_config' => 'sometimes|required',
            'news_pagination' => 'sometimes|required|integer',
            'tax_menu_info' => 'sometimes|required',
        ]);
        $configs = WebConfig::all();

        if ($request->has('no_tlpconfig')) {
            $tlpcfg = $configs->where('config_name', 'no_tlpconfig')->first();
            $tlpcfg->update(['value' => $request->no_tlpconfig]);
        }

        if ($request->has('headertext_config')) {
            $htxtcfg = $configs->where('config_name', 'headertext_config')->first();
            $htxtcfg->update(['value' => $request->headertext_config]);
        }

        if ($request->has('email_config')) {
            $emailcfg = $configs->where('config_name', 'email')->first();
            $emailcfg->update(['value' => $request->email_config]);
        }

        if ($request->has('news_pagination')) {
            $newscfg = $configs->where('config_name', 'news-pagination')->first();
            $newscfg->update(['value' => $request->news_pagination]);
        }

        if ($request->has('tax_menu_info')) {
            $taxcfg = $configs->where('config_name', 'tax-menu-info')->first();
            $taxcfg->update(['value' => $request->tax_menu_info]);
        }

        \Session::flash('notification', ['level' => 'success', 'message' => 'Configuration updated.']);
        return back();
    }

    public function filemanager() {
        return view('admin.config.filemanager');
    }
}
