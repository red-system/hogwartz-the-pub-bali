<?php

namespace App\Http\Controllers;

use App\About_us;
use App\Article;
use Illuminate\Http\Request;
use App\Service;

class FAboutController extends Controller
{
    public function index() {
        $lang = \App::getLocale();
        $about = Article::where('link', \Lang::get('route.about',[], $lang))->where('position', 'menu-utama')->where('published', '1')->where('lang', $lang)->firstOrFail();

        $restaurantoverview = About_us::where('type', 'restaurant-overview')->where('lang', $lang)->first();
        $founderprofile = About_us::where('type', 'founder-profile')->where('lang', $lang)->first();
        $companyprofile = About_us::where('type', 'company-profile')->where('lang', $lang)->first();
        $cooporateinformation = About_us::where('type', 'coorporate-information')->where('lang', $lang)->first();
        $corebuisiness = About_us::where('type', 'core-business')->where('lang', $lang)->get();

        $alt_langs = array_diff(config('app.all_langs'), array($lang));
        $altlink = [];
        foreach ($alt_langs as $altlang) {
            $altabout = Article::where('link', \Lang::get('route.about',[], $altlang))->where('position', 'menu-utama')->where('published', '1')->where('lang', $altlang)->select('link', 'slug')->first();
            $altprefix = $altlang;
            if (!empty($altabout)) {
                $altlink[$altlang] = url(preg_replace('#/+#','/', $altprefix.'/'.$altabout->link));
            } else {
                $altlink[$altlang] = '#';
            }
        }
        $services = Service::where('lang', $lang)->get();
        //$altlink = json_encode($altlink);
        return view('front.about', compact('about', 'altlink', 'restaurantoverview', 'founderprofile', 'companyprofile', 'cooporateinformation', 'corebuisiness', 'services'));
    }

    public function mindex() {
        $lang = \App::getLocale();
        $about = Article::where('link', \Lang::get('route.about',[], $lang))->where('position', 'menu-utama')->where('published', '1')->where('lang', $lang)->firstOrFail();

        $restaurantoverview = About_us::where('type', 'restaurant-overview')->where('lang', $lang)->first();
        $founderprofile = About_us::where('type', 'founder-profile')->where('lang', $lang)->first();
        $companyprofile = About_us::where('type', 'company-profile')->where('lang', $lang)->first();
        $cooporateinformation = About_us::where('type', 'coorporate-information')->where('lang', $lang)->first();
        $corebuisiness = About_us::where('type', 'core-business')->where('lang', $lang)->get();
        return view('webview.about', compact('about', 'restaurantoverview', 'founderprofile', 'companyprofile', 'cooporateinformation', 'corebuisiness'));
    }
}
