<?php

namespace App\Http\Controllers;

use App\Menus;
use App\News;
use App\Outlet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FAjaxJsonController extends Controller
{
    public function recmenu() {
        $lang = \App::getLocale();
        $recmenus = Menus::where('lang', $lang)->where('recommend','1')->select('id', 'name', 'short_description', 'description', 'price', 'image', 'type', 'lang', 'created_at', 'updated_at')->get();
        $recomendedmenus = [];

        foreach ($recmenus as $rm) {
            $rm['link_image'] = asset('assets/front/images/'.$rm->image);
            array_push($recomendedmenus, $rm);
        }
        return response()->json($recomendedmenus);
    }

    public function outlet() {
        $lang = \App::getLocale();
        $outlet = Outlet::where('lang', $lang)->select('title', 'email', 'description', 'image', 'lat', 'lng', 'address_info', 'lang', 'slug')->get();

        $allaoutlet = [];
        foreach ($outlet as $ot) {
            $ot['link_image'] = asset('assets/front/images/'.$ot->image);
            $ot['link_detail_mobile'] = url(preg_replace('#/+#','/', 'mob/'.config('app.locale_prefix').'/'.\Lang::get('route.outlet',[], $lang).'/'.$ot->slug));
            array_push($allaoutlet, $ot);
        }
        return response()->json($outlet);
    }

    public function news() {
        $lang = \App::getLocale();
        $news = News::where('lang', $lang)->orderBy('created_at', 'desc')->select('id', 'title', 'description', 'image', 'lang', 'slug', 'created_at', 'updated_at')->get();

        $allnews = [];
        foreach ($news as $nw) {
            $nw['link_image'] = asset('assets/front/images/'.$nw->image);
            $nw['link_detail_mobile'] = url(preg_replace('#/+#','/', 'mob/'.config('app.locale_prefix').'/'.\Lang::get('route.news',[], config('app.locale_prefix')).'/'.$nw->slug));
            array_push($allnews, $nw);
        }
        return response()->json($allnews);
    }
}
