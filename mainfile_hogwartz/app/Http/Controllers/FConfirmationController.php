<?php

namespace App\Http\Controllers;

use App\OnlineOrder;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Article;

class FConfirmationController extends Controller
{
    public function index()
    {
        $lang = \App::getLocale();
        $reservation = Article::where('link', \Lang::get('route.reservation', [], $lang))->where('position', 'menu-utama')->where('published', '1')->where('lang', $lang)->firstOrFail();
        $term = Article
            ::where([
                'lang' => $lang,
                'link' => 'term'
            ])
            ->first();

        $alt_langs = array_diff(config('app.all_langs'), array($lang));
        $altlink = [];
        foreach ($alt_langs as $altlang) {
            $altreservation = Article::where('link', \Lang::get('route.reservation', [], $altlang))->where('position', 'menu-utama')->where('published', '1')->where('lang', $altlang)->select('link', 'slug')->first();
            $altprefix = $altlang;
            if (!empty($altreservation)) {
                $altlink[$altlang] = url(preg_replace('#/+#', '/', $altprefix . '/' . $altreservation->link));
            } else {
                $altlink[$altlang] = '#';
            }
        }
        //$altlink = json_encode($altlink);
        return view('front.confirmation', compact('reservation', 'lang', 'altlink', 'term'));
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'kode_konfirmasi' => 'required',
            'proof_payment' => 'required'
        ]);

        $name = $request->input('name');
        $kode_konfirmasi = $request->input('kode_konfirmasi');

        $file = $request->file('proof_payment');
        $file->move('assets/front/images', $file->getClientOriginalName());
        $proof_payment = $file->getClientOriginalName();


        $data_where = [
            'kode_konfirmasi' => $kode_konfirmasi
        ];
        $data_update = [
            'proof_payment' => $proof_payment
        ];

        OnlineOrder::where($data_where)->update($data_update);

        return back()->with('notification', ['level' => 'success', 'altlevel' => 'success', 'title' => 'Order Success', 'message' => "Confirmation Successfull"]);
    }
}
