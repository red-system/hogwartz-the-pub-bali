<?php

namespace App\Http\Controllers;

use App\Article;
use App\ContactMessage;
use App\Email;
use App\Mail\MailContactForm;
use App\Mail\MailContactOutletForm;
use App\Outlet;
use Illuminate\Http\Request;
use App\WebConfig;

class FContactController extends Controller
{
    public function index() {
        $lang = \App::getLocale();
        $contact = Article::where('link', \Lang::get('route.contact',[], $lang))->where('position', 'menu-utama')->where('published', '1')->where('lang', $lang)->firstOrFail();
        $outlets = Outlet::where('lang', $lang)->select('equal_id', 'address_info', 'title')->get();
        $alt_langs = array_diff(config('app.all_langs'), array($lang));
        $altlink = [];
        foreach ($alt_langs as $altlang) {
            $altarticle = Article::where('equal_id', $contact->equal_id)->where('published', '1')->where('lang', $altlang)->select('link', 'slug')->first();
            $altprefix = $altlang;
            if (!empty($altarticle)) {
                $altlink[$altlang] = url(preg_replace('#/+#','/', $altprefix.'/'.$altarticle->link.'/'.$altarticle->slug));
            } else {
                $altlink[$altlang] = '#';
            }
        }
        $web_config = WebConfig::get();
        //$altlink = json_encode($altlink);
        return view('front.contact', compact('contact', 'altlink', 'outlets', 'web_config'));
    }

    public function store(Request $request) {
        $this->validate($request, [
            'subject' => 'required|string|max:255',
            'message' => 'required|string',
            'name' => 'required|string|max:255',
            'email' => 'required|email|max:255',
            'phone' => 'required|string|max:18',
            // 'outlet_id' => 'required|exists:outlet,equal_id',
            //'g-recaptcha-response' => 'required|captcha',
        ]);

        $data = $request->only('subject', 'message', 'name', 'email', 'phone');
        // $outlet = Outlet::where('equal_id', $request->outlet_id)->where('lang', config('app.default_locale'))->first();
        // $data['outlet_id'] = $outlet->id;
        // $data['outlet_name'] = $outlet->title;
        $data['messageLines'] = explode("\n", $request->get('message'));
        ContactMessage::create($data);

        // \Mail::to($outlet->email)->queue(new MailContactOutletForm($data));

        $emails = Email::where('receipt',1)->get();

        foreach($emails as $email) {
            \Mail::to($email->email)->queue(new MailContactForm($data));
        }
        
        \Session::flash('notification', ['level' => 'success', 'message' => \Lang::get('front.ct-submitmess',[], \App::getLocale())]);
        return back();
    }

    public function outlet(Request $request) {
        $lang = \App::getLocale();
        $lat = $request->lat;
        $long = $request->long;

        $dataoutlet = Outlet::where('lang', $lang)->selectRaw('id, title, lat, lng, ( 3959 * ACOS( COS( RADIANS('.$lat.') ) * COS( RADIANS( lat ) ) * 
                                                                COS( RADIANS( lng ) - RADIANS('.$long.') ) + SIN( RADIANS('.$lat.') ) * 
                                                                SIN( RADIANS( lat ) ) ) ) AS distance')->orderBy('distance', 'asc')->get();
        return \Response::json($dataoutlet);
    }

    public function outletwithoutloc() {
        $lang = \App::getLocale();
        $dataoutlet = Outlet::where('lang', $lang)->select('id', 'title', 'lat', 'lng')->get();
        return \Response::json($dataoutlet);
    }

    public function mindex() {
        $lang = \App::getLocale();
        $contact = Article::where('link', \Lang::get('route.contact',[], $lang))->where('position', 'menu-utama')->where('published', '1')->where('lang', $lang)->firstOrFail();
        $outlets = Outlet::where('lang', $lang)->select('equal_id', 'address_info', 'title')->get();
        return view('webview.contact', compact('contact', 'outlets'));
    }
}
