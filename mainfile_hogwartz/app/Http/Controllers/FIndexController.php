<?php

namespace App\Http\Controllers;

use App\Article;
use App\Images;
use App\Menus;
use App\News;
use App\Packages;
use App\Specialities;
use App\VideoHome;
use Illuminate\Http\Request;
use App\Service;
use App\Testimonial;

class FIndexController extends Controller
{
    public function index() {
        $lang = \App::getLocale();
        $home = Article::where('link', '/')->where('position', 'menu-utama')->where('published', '1')->where('lang', $lang)->firstOrFail();

        $homesliders = Images::where('type', 'home-slider')->where('lang', $lang)->get();
        $specialties = Specialities::where('lang', $lang)->get();
        $services = Service::where('lang', $lang)->get();
        $packagesrecomended = Article::where('featured', '1')->where('position', 'sub-menu-utama')->where('lang', $lang)->where('link', \Lang::get('route.package',[], $lang))->get();
        $videoprofile = VideoHome::find(1);
        $recentnews = News::where('lang', $lang)->orderBy('created_at', 'desc')->take(5)->get();
        $recmenus = Menus::where('lang', $lang)->where('recommend','1')->get();
        $testimonials = Testimonial::get();
        $dsimage = Images::where('type', 'dsimage')->where('lang', $lang)->first();

        $link_about = Article::where('id', '27')->value('link');

        $alt_langs = array_diff(config('app.all_langs'), array($lang));
        $altlink = [];
        foreach ($alt_langs as $altlang) {
            $althome = Article::where('link', '/')->where('position', 'menu-utama')->where('published', '1')->where('lang', $altlang)->select('link', 'slug')->first();
            $altprefix = $altlang;
            if (!empty($althome)) {
                $altlink[$altlang] = url(preg_replace('#/+#','/', $altprefix.'/'.$althome->link));
            } else {
                $altlink[$altlang] = '#';
            }
        }
        //$altlink = json_encode($altlink);

        $discover = Article::where('position', 'discover')->where('lang', $lang)->first();
        return view(
            'front.index',
            compact(
                'home',
                'altlink',
                'specialties',
                'packagesrecomended',
                'videoprofile',
                'recentnews',
                'homesliders',
                'recmenus',
                'services',
                'testimonials',
                'discover',
                'dsimage',
                'link_about'
            )
        );
    }

    public function homeslider() {
        $homesliders = Images::where('type', 'home-slider')->where('lang', config('app.default_locale'))->get();
        $dataimage = [];
        foreach ($homesliders as $hs) {
            array_push($dataimage, ['id' => $hs->id, 'img-url' => asset('assets/front/images/'.$hs->image)]);
        }
        return response()->json($dataimage);
    }

    public function mindex() {
        $lang = \App::getLocale();
        $home = Article::where('link', '/')->where('position', 'menu-utama')->where('published', '1')->where('lang', $lang)->firstOrFail();

        $homesliders = Images::where('type', 'home-slider')->where('lang', $lang)->get();
        $specialties = Specialities::where('lang', $lang)->get();
        $packagesrecomended = Article::where('featured', '1')->where('position', 'sub-menu-utama')->where('lang', $lang)->where('link', \Lang::get('route.package',[], $lang))->get();
        $videoprofile = VideoHome::find(1);
        $recentnews = News::where('lang', $lang)->orderBy('created_at', 'desc')->take(5)->get();
        $recmenus = Menus::where('lang', $lang)->where('recommend','1')->get();
        return view('webview.index', compact('home', 'specialties', 'packagesrecomended', 'videoprofile', 'recentnews', 'homesliders', 'recmenus'));
    }
}
