<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Menus;
use App\WebConfig;
use Illuminate\Http\Request;

class FMenusController extends Controller
{
    public function index() {
        $lang = \App::getLocale();
        $articlemenu = Article::where('link', \Lang::get('route.menu',[], $lang))->where('position', 'menu-utama')->where('published', '1')->where('lang', $lang)->firstOrFail();
        // $allmenus = Menus::whereHas('categories', function ($query) use ($lang) { $query->where('lang', $lang)->groupBy('category_id'); })->where('type', 'food')->where('lang', $lang)->get();

//        $categories = Category::where('lang', $lang)->get();
//
//        $allmenus = [];
//
//        foreach($categories as $category){
//            $foods = Menus::where('lang', $lang)->get();
//
//            foreach ($foods as &$food) {
//                $cat = $food->categories;
//
//                foreach ($cat as $ct) {
//                    if($ct->id == $category->id){
//                        $allmenus[$category->id][] = $food;
//                    }
//                }
//            }
//        }
//
//
//        $menucategories = Category::where('type', 'menu')->whereHas('menus', function ($query) use ($lang) { $query->where('lang', $lang)->where('type', 'food'); })->where('lang', $lang)->get();
//        $drinkcategories = Category::where('type', 'menu')->whereHas('menus', function ($query) use ($lang) { $query->where('lang', $lang)->where('type', 'drink'); })->where('lang', $lang)->get();
//        $othercategories = Category::where('type', 'menu')->whereHas('menus', function ($query) use ($lang) { $query->where('lang', $lang)->where('type', 'other'); })->where('lang', $lang)->get();
//
//        // dd($drinkcategories);
//
//        $taxinfo = WebConfig::where('config_name', 'tax-menu-info')->first();
//
//        $alt_langs = array_diff(config('app.all_langs'), array($lang));
//        $altlink = [];
//        foreach ($alt_langs as $altlang) {
//            $altmenu = Article::where('link', \Lang::get('route.menu',[], $altlang))->where('position', 'menu-utama')->where('published', '1')->where('lang', $altlang)->select('link', 'slug')->first();
//            $altprefix = $altlang;
//            if (!empty($altmenu)) {
//                $altlink[$altlang] = url(preg_replace('#/+#','/', $altprefix.'/'.$altmenu->link));
//            } else {
//                $altlink[$altlang] = '#';
//            }
//        }

        $show_menu_list = FALSE;

        //$altlink = json_encode($altlink);
        return view('front.menu', compact('allmenus', 'menucategories', 'articlemenu', 'altlink', 'drinkcategories', 'taxinfo', 'othercategories', 'show_menu_list'));
    }

    public function mindex() {
        $lang = \App::getLocale();
        $articlemenu = Article::where('link', \Lang::get('route.menu',[], $lang))->where('position', 'menu-utama')->where('published', '1')->where('lang', $lang)->firstOrFail();
        $allmenus = Menus::whereHas('categories', function ($query) use ($lang) { $query->where('lang', $lang); })->where('type', 'food')->where('lang', $lang)->get();
        $menucategories = Category::where('type', 'menu')->whereHas('menus', function ($query) use ($lang) { $query->where('lang', $lang)->where('type', 'food'); })->where('lang', $lang)->get();
        $drinkcategories = Category::where('type', 'menu')->whereHas('menus', function ($query) use ($lang) { $query->where('lang', $lang)->where('type', 'drink'); })->where('lang', $lang)->get();
        $taxinfo = WebConfig::where('config_name', 'tax-menu-info')->first();
        return view('webview.menu', compact('allmenus', 'menucategories', 'articlemenu', 'drinkcategories', 'taxinfo'));
    }
}
