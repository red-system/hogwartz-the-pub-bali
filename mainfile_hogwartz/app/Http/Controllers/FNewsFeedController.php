<?php

namespace App\Http\Controllers;

use App\Article;
use App\News;
use App\NewsCategories;
use App\NewsTags;
use App\WebConfig;
use Illuminate\Http\Request;

class FNewsFeedController extends Controller
{
    public function index() {
        $lang = \App::getLocale();
        $cfg = WebConfig::where('config_name', 'news-pagination')->first();
        $newsfeed = Article::where('link', \Lang::get('route.news',[], $lang))->where('position', 'menu-utama')->where('published', '1')->where('lang', $lang)->firstOrFail();

        $allnews = News::where('lang', $lang)->orderBy('created_at', 'desc')->paginate($cfg->value);

        $alt_langs = array_diff(config('app.all_langs'), array($lang));
        $altlink = [];
        foreach ($alt_langs as $altlang) {
            $altabout = Article::where('link', \Lang::get('route.news',[], $altlang))->where('position', 'menu-utama')->where('published', '1')->where('lang', $altlang)->select('link', 'slug')->first();
            $altprefix = $altlang;
            if (!empty($altabout)) {
                $altlink[$altlang] = url(preg_replace('#/+#','/', $altprefix.'/'.$altabout->link));
            } else {
                $altlink[$altlang] = '#';
            }
        }
        //$altlink = json_encode($altlink);
        return view('front.newsfeed', compact('newsfeed', 'altlink', 'allnews'));
    }

    public function detail($slug) {
        $lang = \App::getLocale();
        $news = News::where('lang', $lang)->where('slug', $slug)->firstOrFail();
        $cats = $news->categories()->where('lang', $lang)->get();
        $tags = $news->tags()->where('lang', $lang)->get();
        $alt_langs = array_diff(config('app.all_langs'), array($lang));
        $altlink = [];
        foreach ($alt_langs as $altlang) {
            $altnews = News::where('lang', $altlang)->where('equal_id', $news->equal_id)->first();
            $altprefix = $altlang;
            if (!empty($altnews)) {
                $altlink[$altlang] = url(preg_replace('#/+#','/', $altprefix.'/'.\Lang::get('route.news',[], $altlang).'/'.$altnews->slug));
            } else {
                $altlink[$altlang] = '#';
            }
        }
        //$altlink = json_encode($altlink);
        return view('front.newsdetail', compact('news', 'altlink', 'cats', 'tags'));
    }

    public function archive($thbln) {
        $lang = \App::getLocale();
        $cfg = WebConfig::where('config_name', 'news-pagination')->first();
        $newsarchive = News::where('lang', $lang)->where(\DB::raw('LEFT(created_at,7)'), $thbln)->orderBy('created_at', 'desc')->paginate($cfg->value);

        $alt_langs = array_diff(config('app.all_langs'), array($lang));
        $altlink = [];
        foreach ($alt_langs as $altlang) {
            $altprefix = $altlang;
            $altlink[$altlang] = url(preg_replace('#/+#','/', $altprefix.'/'.\Lang::get('route.news',[], $altlang).'/archive/'.$thbln));
        }
        //$altlink = json_encode($altlink);
        return view('front.newsarchive', compact('newsarchive', 'thbln', 'altlink'));
    }

    public function category($slug) {
        $lang = \App::getLocale();
        $cfg = WebConfig::where('config_name', 'news-pagination')->first();
        $category = NewsCategories::where('lang', $lang)->where('slug', $slug)->firstOrFail();
        $newscategories = $category->news(function ($query) use ($lang) {$query->where('lang', $lang);})->orderBy('news.created_at', 'desc')->paginate($cfg->value);

        $alt_langs = array_diff(config('app.all_langs'), array($lang));
        $altlink = [];
        foreach ($alt_langs as $altlang) {
            $altcat = NewsCategories::where('lang', $altlang)->where('equal_id', $category->equal_id)->first();
            $altprefix = $altlang;
            if (!empty($altcat)) {
                $altlink[$altlang] = url(preg_replace('#/+#','/', $altprefix.'/'.\Lang::get('route.news',[], $altlang).'/category/'.$altcat->slug));
            } else {
                $altlink[$altlang] = '#';
            }
        }
        //$altlink = json_encode($altlink);
        return view('front.newscategory', compact('newscategories', 'altlink', 'category'));
    }

    public function tag($slug) {
        $lang = \App::getLocale();
        $cfg = WebConfig::where('config_name', 'news-pagination')->first();
        $tag = NewsTags::where('lang', $lang)->where('slug', $slug)->firstOrFail();
        $newstags = $tag->news(function ($query) use ($lang) {$query->where('lang', $lang);})->orderBy('news.created_at', 'desc')->paginate($cfg->value);

        $alt_langs = array_diff(config('app.all_langs'), array($lang));
        $altlink = [];
        foreach ($alt_langs as $altlang) {
            $alttag = NewsTags::where('lang', $altlang)->where('equal_id', $tag->equal_id)->first();
            $altprefix = $altlang;
            if (!empty($alttag)) {
                $altlink[$altlang] = url(preg_replace('#/+#','/', $altprefix.'/'.\Lang::get('route.news',[], $altlang).'/tag/'.$alttag->slug));
            } else {
                $altlink[$altlang] = '#';
            }
        }
        //$altlink = json_encode($altlink);
        return view('front.newstag', compact('newstags', 'altlink', 'tag'));
    }

    public function mindex() {
        $lang = \App::getLocale();
        $cfg = WebConfig::where('config_name', 'news-pagination')->first();
        $newsfeed = Article::where('link', \Lang::get('route.news',[], $lang))->where('position', 'menu-utama')->where('published', '1')->where('lang', $lang)->firstOrFail();

        $allnews = News::where('lang', $lang)->orderBy('created_at', 'desc')->paginate($cfg->value);
        return view('webview.newsfeed', compact('newsfeed', 'allnews'));
    }

    public function mdetail($slug) {
        $lang = \App::getLocale();
        $news = News::where('lang', $lang)->where('slug', $slug)->firstOrFail();
        $cats = $news->categories()->where('lang', $lang)->get();
        $tags = $news->tags()->where('lang', $lang)->get();
        return view('webview.newsdetail', compact('news', 'cats', 'tags'));
    }

    public function marchive($thbln) {
        $lang = \App::getLocale();
        $cfg = WebConfig::where('config_name', 'news-pagination')->first();
        $newsarchive = News::where('lang', $lang)->where(\DB::raw('LEFT(created_at,7)'), $thbln)->orderBy('created_at', 'desc')->paginate($cfg->value);
        return view('webview.newsarchive', compact('newsarchive', 'thbln'));
    }

    public function mcategory($slug) {
        $lang = \App::getLocale();
        $cfg = WebConfig::where('config_name', 'news-pagination')->first();
        $category = NewsCategories::where('lang', $lang)->where('slug', $slug)->firstOrFail();
        $newscategories = $category->news(function ($query) use ($lang) {$query->where('lang', $lang);})->orderBy('news.created_at', 'desc')->paginate($cfg->value);
        return view('webview.newscategory', compact('newscategories', 'category'));
    }

    public function mtag($slug) {
        $lang = \App::getLocale();
        $cfg = WebConfig::where('config_name', 'news-pagination')->first();
        $tag = NewsTags::where('lang', $lang)->where('slug', $slug)->firstOrFail();
        $newstags = $tag->news(function ($query) use ($lang) {$query->where('lang', $lang);})->orderBy('news.created_at', 'desc')->paginate($cfg->value);
        return view('webview.newstag', compact('newstags', 'tag'));
    }
}
