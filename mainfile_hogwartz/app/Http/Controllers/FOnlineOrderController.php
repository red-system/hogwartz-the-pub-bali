<?php

namespace App\Http\Controllers;

use App\Email;
use App\Mail\MailReservationForm;
use App\Mail\MailReservationOutletForm;
use App\OnlineOrder;
use App\Outlet;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Mail;

class FOnlineOrderController extends Controller
{
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required|string|max:18',
            // 'outlet_id' => 'required|exists:outlet,equal_id',
            'reservation_datetime' => 'required',
            'remark' => 'required|string',
            'pax' => 'required|numeric',
            'party' => 'required',
            'cake' => 'required'
            //'g-recaptcha-response' => 'required|captcha',
        ]);

        $date = strtotime($request->reservation_datetime);
        $request->reservation_datetime = date("Y-m-d H:i:s", $date);

        $request->remark = $request->remark . '<br><br>Party: ' . $request->party;


        $request->remark = $request->remark . '<br>' . 'Cake: ' . $request->cake;

        $kode_konfirmasi = $this->kode_konfirmasi();

        $data = $request->only('name', 'email', 'phone', 'reservation_datetime', 'remark', 'pax', 'party', 'cake');
        $data['kode_konfirmasi'] = $kode_konfirmasi;


        $order = new OnlineOrder();
        $order->name = $request->name;
        $order->email = $request->email;
        $order->phone = $request->phone;
        $order->reservation_datetime = $request->reservation_datetime;
        $order->remark = $request->remark;
        $order->pax = $request->pax;
        $order->kode_konfirmasi = $kode_konfirmasi;

        $order->save();

        $data['remarkLines'] = explode("\n", $request->get('remark'));


        try {

            /**
             * Send Email to Admin
             */
            $emails = Email::where('receipt', 1)->get();
            foreach ($emails as $email) {
                Mail::send('emails.onlineorder', $data, function ($message) use ($email) {
                    $message->subject('Online Order Hogwartz The Pub');
                    $message->from('hogwartzthepub@gmail.com', 'Hogwartz The Pub');
                    $message->to($email->email);
                });
            }

            /**
             * Send Email to User
             */
            Mail::send('emails.onlineorder', $data, function ($message) use ($email, $request) {
                $message->subject('Online Order Hogwartz The Pub');
                $message->from('hogwartzthepub@gmail.com', 'Hogwartz The Pub');
                $message->to($request->email);
            });

            return 'Berhasil Kirim Email';
        } catch (Exception $e) {
            return response(['status' => false, 'errors' => $e->getMessage()]);
        }

        \Session::flash('notification', ['level' => 'success', 'message' => \Lang::get('front.rsf-submitorder', [], \App::getLocale())]);
        return redirect('' . preg_replace('#/+#', '/', config('app.locale_prefix')) . '#order-form');
    }

    function tryEmail(Request $request)
    {
        try {
            Mail::send('email', ['nama' => 'Mahendra Wardana', 'pesan' => 'pesan'], function ($message) use ($request) {
                $message->subject('Judul');
                $message->from('hogwartzthepub@gmail.com', 'Hogwartz The Pub');
                $message->to('mahendra.adi.wardana@gmail.com');
            });
            return 'Berhasil Kirim Email';
        } catch (Exception $e) {
            return response(['status' => false, 'errors' => $e->getMessage()]);
        }
    }

    public function storeReservation(Request $request)
    {
        $rules = [
            'name' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required|string|max:18',
            'active_socmed' => 'required',
            'reservation_datetime' => 'required',
            'remark' => 'required|string',
            'pax' => 'required|numeric',
            'party' => 'required',
            'name_for_party' => 'required_if:party,Birthday,Wedding Anniversary',
            'cake' => 'required'
        ];

        $validator = \Validator::make($request->all(), $rules);
        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator)->with('notification', ['level' => 'error', 'altlevel' => 'error', 'title' => 'Order Failed', 'message' => \Lang::get('front.rsf-submitorderfailed', [], \App::getLocale())]);
        } else {
            $date = strtotime($request->reservation_datetime);
            $request->reservation_datetime = date("Y-m-d H:i:s", $date);

            $request->remark = $request->remark . '<br><br>Party: ' . $request->party;

            if (in_array($request->party, ['Birthday', 'Wedding Anniversary'])) {
                $request->remark = $request->remark . '<br>' . 'Party For: ' . $request->name_for_party;
            }

            $request->remark = $request->remark . '<br>' . 'Cake: ' . $request->cake;

            $kode_konfirmasi = $this->kode_konfirmasi();

            $data = $request->only('name', 'email', 'phone', 'reservation_datetime', 'remark', 'pax', 'party', 'cake', 'active_socmed');

            $order = new OnlineOrder();
            $order->name = $request->name;
            $order->email = $request->email;
            $order->phone = $request->phone;
            $order->active_socmed = $request->active_socmed;
            $order->reservation_datetime = $request->reservation_datetime;
            $order->remark = $request->remark;
            $order->pax = $request->pax;
            $order->kode_konfirmasi = $kode_konfirmasi;
            $order->save();

            $data['remarkLines'] = explode("\n", $request->get('remark'));
            $data['socmedLines'] = explode("\n", $request->get('active_socmed'));
            $data['name_for_party'] = $request->name_for_party;

            try {

                /**
                 * Send Email to Admin
                 */
                $emails = Email::where('receipt', 1)->get();
                foreach ($emails as $email) {
                    Mail::send('emails.onlineorder', ['data' => $data], function ($message) use ($email) {
                        $message->subject('Online Order Hogwartz The Pub');
                        $message->from('hogwartzthepub@gmail.com', 'Hogwartz The Pub');
                        $message->to($email->email);
                    });
                }

                /**
                 * Send Email to User
                 */
                Mail::send('emails.onlineorder_user', ['data' => $data], function ($message) use ($email, $request) {
                    $message->subject('Online Order Hogwartz The Pub');
                    $message->from('hogwartzthepub@gmail.com', 'Hogwartz The Pub');
                    $message->to($request->email);
                });

                //return 'Berhasil Kirim Email';
            } catch (Exception $e) {
                //return response(['status' => false, 'errors' => $e->getMessage()]);
            }

            //\Mail::to($request->email)->queue(new MailReservationForm($data));

            if ($request->pax >= 10) {
                $message = \Lang::get('front.rsf-submitorder10ka', [], \App::getLocale());
            } else {
                $message = \Lang::get('front.rsf-submitorder', [], \App::getLocale());
            }
            return back()->with('notification', ['level' => 'success', 'altlevel' => 'success', 'title' => 'Order Success', 'message' => $message]);
        }
    }

    public function kode_konfirmasi($length = 5)
    {
        $characters = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }
}
