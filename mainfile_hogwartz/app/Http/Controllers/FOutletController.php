<?php

namespace App\Http\Controllers;

use App\Article;
use App\Category;
use App\Email;
use App\Mail\MailReservationForm;
use App\Mail\MailReservationOutletForm;
use App\OnlineOrder;
use App\Outlet;
use App\WebConfig;
use Illuminate\Http\Request;

class FOutletController extends Controller
{
    public function index() {
        $lang = \App::getLocale();
        $articleoutlet = Article::where('link', \Lang::get('route.outlet',[], $lang))->where('position', 'menu-utama')->where('published', '1')->where('lang', $lang)->firstOrFail();

        $alloutlet = Outlet::where('lang', $lang)->get();

        $alt_langs = array_diff(config('app.all_langs'), array($lang));
        $altlink = [];
        foreach ($alt_langs as $altlang) {
            $altoutlet = Article::where('link', \Lang::get('route.outlet',[], $altlang))->where('position', 'menu-utama')->where('published', '1')->where('lang', $altlang)->select('link', 'slug')->first();
            $altprefix = $altlang;
            if (!empty($altoutlet)) {
                $altlink[$altlang] = url(preg_replace('#/+#','/', $altprefix.'/'.$altoutlet->link));
            } else {
                $altlink[$altlang] = '#';
            }
        }
        //$altlink = json_encode($altlink);
        return view('front.outlet', compact('articleoutlet', 'alloutlet', 'altlink'));
    }

    public function onlineorder(Request $request) {
        $this->validate($request, [
            'name' => 'required|string',
            'email' => 'required|email',
            'phone' => 'required|string|max:18',
            'outlet_id' => 'required|exists:outlet,equal_id',
            'reservation_datetime' => 'required',
            'remark' => 'required|string',
            //'g-recaptcha-response' => 'required|captcha',
        ]);

        $data = $request->only('name', 'email', 'phone', 'reservation_datetime', 'remark');
        $outlet = Outlet::where('equal_id', $request->outlet_id)->where('lang', config('app.default_locale'))->first();
        $data['outlet_id'] = $outlet->id;
        $order = OnlineOrder::create($data);

        $data['remarkLines'] = explode("\n", $request->get('remark'));
        $data['outlet_name'] = $outlet->title;

        \Mail::to($outlet->email)->queue(new MailReservationOutletForm($data));

        $emails = Email::where('receipt',1)->get();
        foreach($emails as $email) {
            \Mail::to($email->email)->queue(new MailReservationForm($data));
        }
        \Session::flash('notification', ['level' => 'success', 'message' => \Lang::get('front.rsf-submitorder',[], \App::getLocale())]);
        return back();
    }

    public function detail($slug) {
        $lang = \App::getLocale();
        $outlet = Outlet::where('lang', $lang)->where('slug', $slug)->firstOrFail();
        $outletmenu = $outlet->menus;
        $sliders = $outlet->images()->where('type', 'outlet-slider')->where('lang', $lang)->get();
        $otheroutlets = Outlet::where('lang', $lang)->where('id', '!=', $outlet->id)->get();
        $menucategories = Category::where('type', 'menu')->whereHas('menus', function ($query) use ($lang, $outletmenu) { $query->where('lang', $lang)->where('type', 'food')->whereIn('menus.id', $outletmenu->pluck('id')); })->where('lang', $lang)->get();
        $drinkcategories = Category::where('type', 'menu')->whereHas('menus', function ($query) use ($lang, $outletmenu) { $query->where('lang', $lang)->where('type', 'drink')->whereIn('menus.id', $outletmenu->pluck('id')); })->where('lang', $lang)->get();
        $taxinfo = WebConfig::where('config_name', 'tax-menu-info')->first();

        $alt_langs = array_diff(config('app.all_langs'), array($lang));
        $altlink = [];
        foreach ($alt_langs as $altlang) {
            $altoutlet = Outlet::where('lang', $altlang)->where('equal_id', $outlet->equal_id)->first();
            $altprefix = $altlang;
            if (!empty($altoutlet)) {
                $altlink[$altlang] = url(preg_replace('#/+#','/', $altprefix.'/'.\Lang::get('route.outlet',[], $altlang).'/'.$altoutlet->slug));
            } else {
                $altlink[$altlang] = '#';
            }
        }
        //$altlink = json_encode($altlink);
        return view('front.outletdetail', compact('outlet', 'altlink', 'sliders', 'otheroutlets', 'outletmenu', 'menucategories', 'drinkcategories', 'taxinfo'));
    }

    public function mindex() {
        $lang = \App::getLocale();
        $articleoutlet = Article::where('link', \Lang::get('route.outlet',[], $lang))->where('position', 'menu-utama')->where('published', '1')->where('lang', $lang)->firstOrFail();
        $alloutlet = Outlet::where('lang', $lang)->get();
        return view('webview.outlet', compact('articleoutlet', 'alloutlet'));
    }

    public function mdetail($slug) {
        $lang = \App::getLocale();
        $outlet = Outlet::where('lang', $lang)->where('slug', $slug)->firstOrFail();
        $outletmenu = $outlet->menus;
        $sliders = $outlet->images()->where('type', 'outlet-slider')->where('lang', $lang)->get();
        $otheroutlets = Outlet::where('lang', $lang)->where('id', '!=', $outlet->id)->get();
        $menucategories = Category::where('type', 'menu')->whereHas('menus', function ($query) use ($lang, $outletmenu) { $query->where('lang', $lang)->where('type', 'food')->whereIn('menus.id', $outletmenu->pluck('id')); })->where('lang', $lang)->get();
        $drinkcategories = Category::where('type', 'menu')->whereHas('menus', function ($query) use ($lang, $outletmenu) { $query->where('lang', $lang)->where('type', 'drink')->whereIn('menus.id', $outletmenu->pluck('id')); })->where('lang', $lang)->get();
        $taxinfo = WebConfig::where('config_name', 'tax-menu-info')->first();
        return view('webview.outletdetail', compact('outlet', 'sliders', 'otheroutlets', 'outletmenu', 'menucategories', 'drinkcategories', 'taxinfo'));
    }
}
