<?php

namespace App\Http\Controllers;

use App\Article;
use App\Packages;
use App\WebConfig;
use Illuminate\Http\Request;

class FPackageController extends Controller
{
    public function index($slug) {
        $lang = \App::getLocale();
        $articlepackage = Article::where('link', \Lang::get('route.package',[], $lang))->where('position', 'sub-menu-utama')->where('lang', $lang)->where('published', '1')->where('slug', $slug)->firstOrFail();
        $allpackages = $articlepackage->packages()->where('lang', $lang)->get();
        $taxinfo = WebConfig::where('config_name', 'tax-menu-info')->first();

        $alt_langs = array_diff(config('app.all_langs'), array($lang));
        $altlink = [];
        foreach ($alt_langs as $altlang) {
            $altpackage = Article::where('link', \Lang::get('route.package',[], $altlang))->where('position', 'menu-utama')->where('published', '1')->where('lang', $altlang)->select('link', 'slug')->first();
            $altprefix = $altlang;
            if (!empty($altpackage)) {
                $altlink[$altlang] = url(preg_replace('#/+#','/', $altprefix.'/'.$altpackage->link));
            } else {
                $altlink[$altlang] = '#';
            }
        }
        //$altlink = json_encode($altlink);
        return view('front.packages', compact('articlepackage', 'allpackages', 'altlink', 'taxinfo'));
    }

    public function mindex($slug) {
        $lang = \App::getLocale();
        $articlepackage = Article::where('link', \Lang::get('route.package',[], $lang))->where('position', 'sub-menu-utama')->where('lang', $lang)->where('published', '1')->where('slug', $slug)->firstOrFail();
        $allpackages = $articlepackage->packages()->where('lang', $lang)->get();
        $taxinfo = WebConfig::where('config_name', 'tax-menu-info')->first();
        return view('webview.packages', compact('articlepackage', 'allpackages', 'taxinfo'));
    }
}
