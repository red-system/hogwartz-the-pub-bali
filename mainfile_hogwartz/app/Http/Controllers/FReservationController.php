<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Article;

class FReservationController extends Controller
{
    public function index()
    {
        $lang = \App::getLocale();
        $reservation = Article::where('link', \Lang::get('route.reservation', [], $lang))->where('position', 'menu-utama')->where('published', '1')->where('lang', $lang)->firstOrFail();
        $term = Article
            ::where([
                'lang' => $lang,
                'link'=>'term'
            ])
            ->first();

        $alt_langs = array_diff(config('app.all_langs'), array($lang));
        $altlink = [];
        foreach ($alt_langs as $altlang) {
            $altreservation = Article::where('link', \Lang::get('route.reservation', [], $altlang))->where('position', 'menu-utama')->where('published', '1')->where('lang', $altlang)->select('link', 'slug')->first();
            $altprefix = $altlang;
            if (!empty($altreservation)) {
                $altlink[$altlang] = url(preg_replace('#/+#', '/', $altprefix . '/' . $altreservation->link));
            } else {
                $altlink[$altlang] = '#';
            }
        }
        //$altlink = json_encode($altlink);
        return view('front.reservation', compact('reservation', 'lang', 'altlink', 'term'));
    }
}
