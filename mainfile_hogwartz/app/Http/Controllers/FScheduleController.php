<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\WebConfig;
use App\Article;
use App\Schedule;

class FScheduleController extends Controller
{
    public function index() {
        $lang = \App::getLocale();
        $cfg = WebConfig::where('config_name', 'news-pagination')->first();
        $showfeed = Article::where('link', \Lang::get('route.schedule',[], $lang))->where('position', 'menu-utama')->where('published', '1')->where('lang', $lang)->firstOrFail();

        $allshow = Schedule::where('lang', $lang)->orderBy('created_at', 'desc')->paginate($cfg->value);

        $alt_langs = array_diff(config('app.all_langs'), array($lang));
        $altlink = [];
        foreach ($alt_langs as $altlang) {
            $altabout = Article::where('link', \Lang::get('route.schedule',[], $altlang))->where('position', 'menu-utama')->where('published', '1')->where('lang', $altlang)->select('link')->first();
            $altprefix = $altlang;
            if (!empty($altabout)) {
                $altlink[$altlang] = url(preg_replace('#/+#','/', $altprefix.'/'.$altabout->link));
            } else {
                $altlink[$altlang] = '#';
            }
        }
        //$altlink = json_encode($altlink);
        return view('front.schedule', compact('showfeed', 'altlink', 'allshow'));
    }

    public function detail($slug) {
        $lang = \App::getLocale();
        $show = Schedule::where('lang', $lang)->where('slug', $slug)->firstOrFail();
        $alt_langs = array_diff(config('app.all_langs'), array($lang));
        $altlink = [];
        foreach ($alt_langs as $altlang) {
            $altnews = Schedule::where('lang', $altlang)->where('equal_id', $show->equal_id)->first();
            $altprefix = $altlang;
            if (!empty($altnews)) {
                $altlink[$altlang] = url(preg_replace('#/+#','/', $altprefix.'/'.\Lang::get('route.news',[], $altlang).'/'.$altnews->slug));
            } else {
                $altlink[$altlang] = '#';
            }
        }
        //$altlink = json_encode($altlink);
        return view('front.showdetail', compact('show', 'altlink'));
    }
}
