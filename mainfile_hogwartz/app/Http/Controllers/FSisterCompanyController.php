<?php

namespace App\Http\Controllers;

use App\Article;
use App\SisterCompany;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class FSisterCompanyController extends Controller
{
    public function index() {
        $lang = \App::getLocale();
        $sistercompany = Article::where('link', \Lang::get('route.sistercompany',[], $lang))->where('position', 'menu-utama')->where('published', '1')->where('lang', $lang)->firstOrFail();
        $listcompany = SisterCompany::where('lang', $lang)->where('published', '1')->get();

        $alt_langs = array_diff(config('app.all_langs'), array($lang));
        $altlink = [];
        foreach ($alt_langs as $altlang) {
            $altscomp = Article::where('link', \Lang::get('route.sistercompany',[], $altlang))->where('position', 'menu-utama')->where('published', '1')->where('lang', $altlang)->select('link', 'slug')->first();
            $altprefix = $altlang;
            if (!empty($altscomp)) {
                $altlink[$altlang] = url(preg_replace('#/+#','/', $altprefix.'/'.$altscomp->link));
            } else {
                $altlink[$altlang] = '#';
            }
        }
        //$altlink = json_encode($altlink);
        return view('front.sistercompany', compact('sistercompany', 'listcompany', 'altlink'));
    }

    public function mindex() {
        $lang = \App::getLocale();
        $sistercompany = Article::where('link', \Lang::get('route.sistercompany',[], $lang))->where('position', 'menu-utama')->where('published', '1')->where('lang', $lang)->firstOrFail();
        $listcompany = SisterCompany::where('lang', $lang)->where('published', '1')->get();
        return view('webview.sistercompany', compact('sistercompany', 'listcompany'));
    }
}
