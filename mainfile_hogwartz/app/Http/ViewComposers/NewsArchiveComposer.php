<?php namespace App\Http\ViewComposers;

use App\News;
use Illuminate\Contracts\View\View;

class NewsArchiveComposer {
    public function compose(View $view)
    {
        $lang = \App::getLocale();
        $newsarchive = News::where('lang', $lang)->selectRaw('created_at, LEFT(created_at,7) as thbln, COUNT(LEFT(created_at,7)) as jumlah')->orderBy('thbln', 'desc')->take(12)->groupBy('thbln')->get();
        $view->with('newsarchive', $newsarchive);
    }
}
