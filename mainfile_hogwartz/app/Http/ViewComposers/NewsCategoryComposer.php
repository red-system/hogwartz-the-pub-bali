<?php namespace App\Http\ViewComposers;

use App\NewsCategories;
use Illuminate\Contracts\View\View;

class NewsCategoryComposer {
    public function compose(View $view)
    {
        $lang = \App::getLocale();
        $newscategories = NewsCategories::where('lang', $lang)->whereHas('news', function($query) use ($lang) {$query->where('lang', $lang); })->orderBy('created_at', 'desc')->get();
        $view->with('newscategories', $newscategories);
    }
}
