<?php namespace App\Http\ViewComposers;

use App\News;
use Illuminate\Contracts\View\View;

class NewsRecentComposer {
    public function compose(View $view)
    {
        $lang = \App::getLocale();
        $recentnews = News::where('lang', $lang)->orderBy('created_at', 'desc')->take(4)->get();
        $view->with('newsrecents', $recentnews);
    }
}
