<?php namespace App\Http\ViewComposers;

use App\NewsTags;
use Illuminate\Contracts\View\View;

class NewsTagComposer {
    public function compose(View $view)
    {
        $lang = \App::getLocale();
        $newstags = NewsTags::where('lang', $lang)->whereHas('news', function($query) use ($lang) {$query->where('lang', $lang); })->orderBy('created_at', 'desc')->take(15)->get();
        $view->with('newstags', $newstags);
    }
}
