<?php namespace App\Http\ViewComposers;

use App\WebConfig;
use Illuminate\Contracts\View\View;

class WebConfigComposer {
    public function compose(View $view)
    {
        $webconfig = WebConfig::get();
        $view->with('webcfg_composer', $webconfig);
    }
}