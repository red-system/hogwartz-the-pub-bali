<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Images extends Model
{
    protected $table = 'images';
    protected $fillable = [
        'name', 'image', 'link', 'link_text', 'with_link', 'type', 'slider_text', 'article_id', 'lang', 'equal_id','slider_type', 'outlet_id', 'link_title'
    ];

    public $incrementing = false ;

    public function getPageHeaderAttribute()
    {
        if ($this->image !== '') {
            return asset('assets/front/images/' . $this->image);
        } else {
            return 'http://placehold.it/1129x306';
        }
    }

    public function getHomeSliderAttribute()
    {
        if ($this->image !== '') {
            return asset('assets/front/images/' . $this->image);
        } else {
            return 'http://placehold.it/1920x1080';
        }
    }

    public function getPartnerImageAttribute()
    {
        if ($this->image !== '') {
            return asset('assets/front/images/' . $this->image);
        } else {
            return 'http://placehold.it/176x108';
        }
    }

    public static function tipeSliderList()
    {
        return [
            'tipe1' => 'Type 1 (Text Align Left)',
            'tipe2' => 'Type 2 (Text Align Justify)',
            'tipe3' => 'Type 3 (Text ALign Rign)',
        ];
    }

    // public function getHumanTipeAttribute()
    // {
    //     return static::tipeSliderList()[$this->slider_type];
    // }

    public static function allowedTipe()
    {
        return array_keys(static::tipeSliderList());
    }

    public function article() {
        return $this->belongsTo('App\Article', 'article_id');
    }

    public function outlet() {
        return $this->belongsTo('App\Outlet', 'outlet_id');
    }
}
