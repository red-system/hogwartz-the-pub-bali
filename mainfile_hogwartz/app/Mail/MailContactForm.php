<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class MailContactForm extends Mailable
{
    use Queueable, SerializesModels;

    public $dataemail;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->dataemail = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $dtEmail = $this->dataemail;
        return $this->subject('Hogwartz the Pub Bali From: '.$dtEmail['name'])
                    ->view('emails.contact');
    }
}
