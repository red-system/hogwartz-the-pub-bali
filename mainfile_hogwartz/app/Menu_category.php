<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menu_category extends Model
{
  	protected $primaryKey = 'id';
    protected $guarded = [];
    protected $table = 'menu_category';
}
