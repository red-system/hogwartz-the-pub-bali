<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Menus extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = [];
    protected $table = 'menus';

    public function categories() {
        return $this->belongsToMany('App\Category', 'menu_category', 'menu_id', 'category_id')->withTimestamps();
    }

    public function outlet() {
        return $this->belongsToMany('App\Outlet', 'menu_outlet', 'menu_id', 'outlet_id')->withTimestamps();
    }

     public function getHomeSliderAttribute()
    {
        if ($this->image !== '') {
            return asset('assets/front/images/' . $this->image);
        } else {
            return 'http://placehold.it/1920x1080';
        }
    }

    public static function menuList()
    {
        return [
            'food' => 'Food',
            'drink' => 'Drink',
            'other' => 'Other'
        ];
    }

    public function getHumanMenuAttribute()
    {
        return static::menuList()[$this->type];
    }

    public static function allowedMenu()
    {
        return array_keys(static::menuList());
    }
}
