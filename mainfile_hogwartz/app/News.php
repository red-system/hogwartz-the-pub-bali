<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = [];
    protected $table = 'news';

    public function getPageHeaderAttribute()
    {
        if ($this->image !== '') {
            return asset('assets/front/images/' . $this->image);
        } else {
            return 'http://placehold.it/1129x306';
        }
    }

    public function getHomeSliderAttribute()
    {
        if ($this->image !== '') {
            return asset('assets/front/images/' . $this->image);
        } else {
            return 'http://placehold.it/1920x1080';
        }
    }

    public function getPartnerImageAttribute()
    {
        if ($this->image !== '') {
            return asset('assets/front/images/' . $this->image);
        } else {
            return 'http://placehold.it/176x108';
        }
    }

    public function categories() {
        return $this->belongsToMany('App\NewsCategories', 'news_categories_relation', 'news_id', 'category_id')->withTimestamps();
    }

    public function tags() {
        return $this->belongsToMany('App\NewsTags', 'news_tags_relation', 'news_id', 'tag_id')->withTimestamps();
    }
}
