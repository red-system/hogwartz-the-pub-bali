<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsCategories extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = [];
    protected $table = 'news_categories';

    public function news() {
        return $this->belongsToMany('App\News', 'news_categories_relation', 'category_id', 'news_id')->withTimestamps();
    }
}
