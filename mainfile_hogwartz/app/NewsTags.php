<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class NewsTags extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = [];
    protected $table = 'news_tags';

    public function news() {
        return $this->belongsToMany('App\News', 'news_tags_relation', 'tag_id', 'news_id')->withTimestamps();
    }
}
