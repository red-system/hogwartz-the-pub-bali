<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OnlineOrder extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = [];
    protected $table = 'online_order';

    public static function statusList()
    {
        return [
            'waiting-confirmation' => 'Waiting Confirmation',
            'confirmed' => 'Confirmed',
            'complete' => 'Completed'
        ];
    }

    public static function statusLabel()
    {
        return [
            'waiting-confirmation' => 'label-warning',
            'confirmed' => 'label-info',
            'complete' => 'label-success'
        ];
    }

    public function getHumanStatusAttribute()
    {
        return static::statusList()[$this->status];
    }

    public function getLabelStatusAttribute()
    {
        return static::statusLabel()[$this->status];
    }

    public static function allowedStatus()
    {
        return array_keys(static::statusList());
    }

    public function outlet() {
        return $this->belongsTo('App\Outlet', 'outlet_id');
    }
}
