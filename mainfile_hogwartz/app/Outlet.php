<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Outlet extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = [];
    protected $table = 'outlet';

    public function getPageHeaderAttribute()
    {
        if ($this->image !== '') {
            return asset('assets/front/images/' . $this->image);
        } else {
            return 'http://placehold.it/1129x306';
        }
    }

    public function getHomeSliderAttribute()
    {
        if ($this->image !== '') {
            return asset('assets/front/images/' . $this->image);
        } else {
            return 'http://placehold.it/1920x1080';
        }
    }

    public function getPartnerImageAttribute()
    {
        if ($this->image !== '') {
            return asset('assets/front/images/' . $this->image);
        } else {
            return 'http://placehold.it/176x108';
        }
    }

    public function images() {
        return $this->hasMany('App\Images', 'outlet_id');
    }

    public function order() {
        return $this->hasMany('App\OnlineOrder', 'outlet_id');
    }

    public function contact_messages() {
        return $this->hasMany('App\ContactMessages', 'outlet_id');
    }

    public function menus() {
        return $this->belongsToMany('App\Menus', 'menu_outlet', 'outlet_id', 'menu_id')->withTimestamps();
    }
}
