<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Packages extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = [];
    protected $table = 'packages';

    public function getPageHeaderAttribute()
    {
        if ($this->image !== '') {
            return asset('assets/front/images/' . $this->image);
        } else {
            return 'http://placehold.it/1129x306';
        }
    }

    public function getHomeSliderAttribute()
    {
        if ($this->image !== '') {
            return asset('assets/front/images/' . $this->image);
        } else {
            return 'http://placehold.it/1920x1080';
        }
    }

    public function getPartnerImageAttribute()
    {
        if ($this->image !== '') {
            return asset('assets/front/images/' . $this->image);
        } else {
            return 'http://placehold.it/176x108';
        }
    }

    public function pcategory()
    {
        return $this->belongsTo('App\Article', 'article_id');
    }
}
