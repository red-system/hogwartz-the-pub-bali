<?php

namespace App\Services;

use App\Article;
use App\News;
use App\Outlet;
use App\Schedule;
use Illuminate\Support\Facades\Cache;

class SiteMap
{
    /**
     * Return the content of the Site Map
     */
    public function getSiteMap()
    {
        if (Cache::has('hogwartztp-site-map')) {
            return Cache::get('hogwartztp-site-map');
        }

        $siteMap = $this->buildSiteMap();
        Cache::add('hogwartztp-site-map', $siteMap, 120);
        return $siteMap;
    }

    /**
     * Build the Site Map
     */
    protected function buildSiteMap()
    {
        $def_lang = config('app.default_locale');
        $alt_langs = config('app.alt_langs');

        $menuUtama = $this->getMenuUtama();
        $xml = [];
        $xml[] = '<?xml version="1.0" encoding="UTF-8"?'.'>';
        $xml[] = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xhtml="http://www.w3.org/1999/xhtml">';

        foreach ($menuUtama as $mu) {
            if($mu->childs()->where('position', 'sub-menu-utama')->where('published', '1')->where('lang', $def_lang)->count() > 0) {
                foreach($mu->childs()->where('position', 'sub-menu-utama')->where('published', '1')->where('lang', $def_lang)->get() as $child) {
                    $xml[] = "  <url>";
                    $xml[] = "    <loc>".url(preg_replace('#/+#','/', $def_lang.'/'.$child->link.'/'.$child->slug))."</loc>";
                    $alt_submu = Article::where('position', 'sub-menu-utama')->where('published', '1')->whereIn('lang', $alt_langs)->where('equal_id', $child->equal_id)->select('id','title','link', 'slug', 'parent_id', 'equal_id', 'lang', 'updated_at')->get();
                    foreach ($alt_langs as $alt_lang) {
                        $altsub = $alt_submu->where('lang', $alt_lang)->first();
                        if (!empty($altsub)) {
                            $urllink = url(preg_replace('#/+#','/', $alt_lang.'/'.$altsub->link.'/'.$altsub->slug));
                            $xml[] = "    <xhtml:link rel=\"alternate\" hreflang=\"$alt_lang\" href=\"$urllink\"/>";
                        }
                    }
                    $xml[] = "    <lastmod>".date('c', strtotime($mu->updated_at))."</lastmod>";
                    $xml[] = "  </url>";
                }
            } else {
                $xml[] = "  <url>";
                $xml[] = "    <loc>".url(preg_replace('#/+#','/', $def_lang.'/'.$mu->link))."</loc>";
                $alt_mu = Article::where('position', 'menu-utama')->where('published', '1')->whereIn('lang', $alt_langs)->where('equal_id', $mu->equal_id)->select('id','title','link', 'slug', 'parent_id', 'equal_id', 'lang', 'updated_at')->get();
                foreach ($alt_langs as $alt_lang) {
                    $altmu = $alt_mu->where('lang', $alt_lang)->first();
                    if (!empty($altmu)) {
                        $urllink = url(preg_replace('#/+#','/', $alt_lang.'/'.$altmu->link.'/'.$altmu->slug));
                        $xml[] = "    <xhtml:link rel=\"alternate\" hreflang=\"$alt_lang\" href=\"$urllink\"/>";
                    }
                }
                $xml[] = "    <lastmod>".date('c', strtotime($mu->updated_at))."</lastmod>";
                $xml[] = "  </url>";

                if ($mu->link == \Lang::get('route.schedule',[], $def_lang)) {
                    $allshow = Schedule::where('lang', $def_lang)->orderBy('created_at', 'desc')->get();
                    foreach($allshow as $as) {
                        $alt_sh = Schedule::whereIn('lang', $alt_langs)->where('equal_id', $as->equal_id)->orderBy('created_at', 'desc')->get();
                        $xml[] = "  <url>";
                        $xml[] = "    <loc>".url(preg_replace('#/+#','/', $def_lang.'/'.\Lang::get('route.schedule',[], $def_lang).'/'.$as->slug))."</loc>";
                        foreach ($alt_langs as $alt_lang) {
                            $altsh = $alt_sh->where('lang', $alt_lang)->first();
                            if (!empty($altsh)) {
                                $urllink = url(preg_replace('#/+#','/', $alt_lang.'/'.\Lang::get('route.schedule',[], $alt_lang).'/'.$altsh->slug));
                                $xml[] = "    <xhtml:link rel=\"alternate\" hreflang=\"$alt_lang\" href=\"$urllink\"/>";
                            }
                        }
                        $xml[] = "    <lastmod>".date('c', strtotime($as->updated_at))."</lastmod>";
                        $xml[] = "  </url>";
                    }
                } elseif ($mu->link == \Lang::get('route.news',[], $def_lang)) {
                    $allnews = News::where('lang', $def_lang)->orderBy('created_at', 'desc')->get();
                    foreach($allnews as $an) {
                        $alt_nw = News::whereIn('lang', $alt_langs)->where('equal_id', $an->equal_id)->orderBy('created_at', 'asc')->get();
                        $xml[] = "  <url>";
                        $xml[] = "    <loc>".url(preg_replace('#/+#','/', $def_lang.'/'.\Lang::get('route.news',[], $def_lang).'/'.$an->slug))."</loc>";
                        foreach ($alt_langs as $alt_lang) {
                            $altnw = $alt_nw->where('lang', $alt_lang)->first();
                            if (!empty($altnw)) {
                                $urllink = url(preg_replace('#/+#','/', $alt_lang.'/'.\Lang::get('route.news',[], $alt_lang).'/'.$altnw->slug));
                                $xml[] = "    <xhtml:link rel=\"alternate\" hreflang=\"$alt_lang\" href=\"$urllink\"/>";
                            }
                        }
                        $xml[] = "    <lastmod>".date('c', strtotime($an->updated_at))."</lastmod>";
                        $xml[] = "  </url>";
                    }
                }
            }
        }

        $xml[] = '</urlset>';

        return join("\n", $xml);
    }

    /**
     * Return all the posts as $url => $date
     */
    protected function getMenuUtama()
    {
        $def_lang = config('app.default_locale');
        return Article::where('position', 'menu-utama')
            ->where('published', '1')
            ->where('lang', $def_lang)
            ->select('id','title','link', 'slug', 'parent_id', 'equal_id', 'updated_at')
            ->get();
    }
}