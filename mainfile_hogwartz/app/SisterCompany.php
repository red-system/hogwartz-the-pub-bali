<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SisterCompany extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = [];
    protected $table = 'sister_company';
}
