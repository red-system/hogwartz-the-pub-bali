<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specialities extends Model
{
    protected $primaryKey = 'id';
    protected $guarded = [];
    protected $table = 'specialities';
}
