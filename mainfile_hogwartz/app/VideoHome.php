<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class VideoHome extends Model
{
    protected $primaryKey = 'id';
    protected $fillable = [
        'id', 'link'
           ];


    protected $table = 'video';
}
