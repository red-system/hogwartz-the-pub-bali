<?php

use Illuminate\Database\Seeder;
use App\Article;
use App\Email;
use App\SocialLink;
use App\WebConfig;

class MasterSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // sample admin
        App\User::create([
            'name' => 'Admin',
            'email' => 'admin@gmail.com',
            'password' => bcrypt('secret'),
            'full_admin' => '1'
        ]);

        $email1 = Email::create(['name' => 'Mariasa Gusti', 'email' => 'gstmariasa@gmail.com', 'receipt' => 1]);

        $tt = SocialLink::create([
            'platform' => 'twitter',
            'link' => 'https://twitter.com',
            'published' => '1'
        ]);

        $fb = SocialLink::create([
            'platform' => 'facebook',
            'link' => 'https://facebook.com',
            'published' => '1'
        ]);

        $ig = SocialLink::create([
            'platform' => 'instagram',
            'link' => 'https://instagram.com',
            'published' => '1'
        ]);

        $arteqid = uniqid('ART', true);
        $homeid = Article::create([
            'title' => 'Halaman Utama',
            'thumb_image' => '',
            'conten' => 'Halaman Utama',
            'meta_title' => '',
            'meta_keyword' => '',
            'meta_description' => '',
            'position' => 'menu-utama',
            'published' => '1',
            'link' => '/',
            'more_config' => '1',
            'admin_config' => '1',
            'lang' => 'id',
            'equal_id' => $arteqid
        ]);
        $homeen = Article::create([
            'title' => 'Home',
            'thumb_image' => '',
            'conten' => 'Home',
            'meta_title' => '',
            'meta_keyword' => '',
            'meta_description' => '',
            'position' => 'menu-utama',
            'published' => '1',
            'link' => '/',
            'more_config' => '1',
            'admin_config' => '1',
            'lang' => 'en',
            'equal_id' => $arteqid
        ]);

        $arteqid = uniqid('ART', true);
        $aboutid = Article::create([
            'title' => 'Tentang Kami',
            'thumb_image' => '',
            'conten' => 'Tentang Kami',
            'meta_title' => '',
            'meta_keyword' => '',
            'meta_description' => '',
            'position' => 'menu-utama',
            'published' => '1',
            'link' => 'tentang-kami',
            'more_config' => '1',
            'admin_config' => '1',
            'lang' => 'id',
            'equal_id' => $arteqid
        ]);
        $abouten = Article::create([
            'title' => 'About Us',
            'thumb_image' => '',
            'conten' => 'About Us',
            'meta_title' => '',
            'meta_keyword' => '',
            'meta_description' => '',
            'position' => 'menu-utama',
            'published' => '1',
            'link' => 'about-us',
            'more_config' => '1',
            'admin_config' => '1',
            'lang' => 'en',
            'equal_id' => $arteqid
        ]);

        $arteqid = uniqid('ART', true);
        $outletid = Article::create([
            'title' => 'Outlet',
            'thumb_image' => '',
            'conten' => 'Outlet',
            'meta_title' => '',
            'meta_keyword' => '',
            'meta_description' => '',
            'position' => 'menu-utama',
            'published' => '1',
            'link' => 'outlet',
            'more_config' => '1',
            'admin_config' => '1',
            'lang' => 'id',
            'equal_id' => $arteqid
        ]);
        $outleten = Article::create([
            'title' => 'Outlet',
            'thumb_image' => '',
            'conten' => 'Outlet',
            'meta_title' => '',
            'meta_keyword' => '',
            'meta_description' => '',
            'position' => 'menu-utama',
            'published' => '1',
            'link' => 'outlet',
            'more_config' => '1',
            'admin_config' => '1',
            'lang' => 'en',
            'equal_id' => $arteqid
        ]);

        $arteqid = uniqid('ART', true);
        $packageid = Article::create([
            'title' => 'Paket',
            'thumb_image' => '',
            'conten' => 'Paket',
            'meta_title' => '',
            'meta_keyword' => '',
            'meta_description' => '',
            'position' => 'menu-utama',
            'published' => '1',
            'link' => '#',
            'more_config' => '1',
            'admin_config' => '1',
            'lang' => 'id',
            'equal_id' => $arteqid
        ]);
        $packageen = Article::create([
            'title' => 'Package',
            'thumb_image' => '',
            'conten' => 'Package',
            'meta_title' => '',
            'meta_keyword' => '',
            'meta_description' => '',
            'position' => 'menu-utama',
            'published' => '1',
            'link' => '#',
            'more_config' => '1',
            'admin_config' => '1',
            'lang' => 'en',
            'equal_id' => $arteqid
        ]);

        $arteqid = uniqid('ART', true);
        $menuid = Article::create([
            'title' => 'Menu',
            'thumb_image' => '',
            'conten' => 'Menu',
            'meta_title' => '',
            'meta_keyword' => '',
            'meta_description' => '',
            'position' => 'menu-utama',
            'published' => '1',
            'link' => '#',
            'more_config' => '1',
            'admin_config' => '1',
            'lang' => 'id',
            'equal_id' => $arteqid
        ]);
        $menuen = Article::create([
            'title' => 'Menus',
            'thumb_image' => '',
            'conten' => 'Menus',
            'meta_title' => '',
            'meta_keyword' => '',
            'meta_description' => '',
            'position' => 'menu-utama',
            'published' => '1',
            'link' => '#',
            'more_config' => '1',
            'admin_config' => '1',
            'lang' => 'en',
            'equal_id' => $arteqid
        ]);

        $arteqid = uniqid('ART', true);
        $newsid = Article::create([
            'title' => 'Berita',
            'thumb_image' => '',
            'conten' => 'Berita',
            'meta_title' => '',
            'meta_keyword' => '',
            'meta_description' => '',
            'position' => 'menu-utama',
            'published' => '1',
            'link' => 'berita',
            'more_config' => '1',
            'admin_config' => '1',
            'lang' => 'id',
            'equal_id' => $arteqid
        ]);
        $newsen = Article::create([
            'title' => 'News',
            'thumb_image' => '',
            'conten' => 'News',
            'meta_title' => '',
            'meta_keyword' => '',
            'meta_description' => '',
            'position' => 'menu-utama',
            'published' => '1',
            'link' => 'news',
            'more_config' => '1',
            'admin_config' => '1',
            'lang' => 'en',
            'equal_id' => $arteqid
        ]);

        $arteqid = uniqid('ART', true);
        $scompanyid = Article::create([
            'title' => 'Sister Company',
            'thumb_image' => '',
            'conten' => 'Sister Company',
            'meta_title' => '',
            'meta_keyword' => '',
            'meta_description' => '',
            'position' => 'menu-utama',
            'published' => '1',
            'link' => '#',
            'more_config' => '1',
            'admin_config' => '1',
            'lang' => 'id',
            'equal_id' => $arteqid
        ]);
        $scompanyen = Article::create([
            'title' => 'Sister Company',
            'thumb_image' => '',
            'conten' => 'Sister Company',
            'meta_title' => '',
            'meta_keyword' => '',
            'meta_description' => '',
            'position' => 'menu-utama',
            'published' => '1',
            'link' => '#',
            'more_config' => '1',
            'admin_config' => '1',
            'lang' => 'en',
            'equal_id' => $arteqid
        ]);

        $arteqid = uniqid('ART', true);
        $contactid = Article::create([
            'title' => 'Hubungi Kami',
            'thumb_image' => '',
            'conten' => '<p class="address"><i class="icon-location"></i>Jalan Hanoman, Padang Tegal, Ubud, Bali 80571 Indonesia</p>
                <p class="address"><i class="fa fa-phone"></i> +62 361 975489 / 977675</p>
                <p class="address"><i class="fa fa-envelope"></i>
                    <a href="mailto:ubud@bebekbengil.co.id">ubud@bebekbengil.co.id</a><br>
                    <a href="mailto:om.ubud@bebekbengil.co.id">om.ubud@bebekbengil.co.id</a><br>
                    <a href="mailto:sales.ubud@bebekbengil.co.id">sales.ubud@bebekbengil.co.id</a><br>
                </p>',
            'meta_title' => '',
            'meta_keyword' => '',
            'meta_description' => '',
            'position' => 'menu-utama',
            'published' => '1',
            'link' => 'hubungi-kami',
            'more_config' => '1',
            'admin_config' => '1',
            'lang' => 'id',
            'equal_id' => $arteqid
        ]);
        $contacten = Article::create([
            'title' => 'Contact Us',
            'thumb_image' => '',
            'conten' => '<p class="address"><i class="icon-location"></i>Jalan Hanoman, Padang Tegal, Ubud, Bali 80571 Indonesia</p>
                <p class="address"><i class="fa fa-phone"></i> +62 361 975489 / 977675</p>
                <p class="address"><i class="fa fa-envelope"></i>
                    <a href="mailto:ubud@bebekbengil.co.id">ubud@bebekbengil.co.id</a><br>
                    <a href="mailto:om.ubud@bebekbengil.co.id">om.ubud@bebekbengil.co.id</a><br>
                    <a href="mailto:sales.ubud@bebekbengil.co.id">sales.ubud@bebekbengil.co.id</a><br>
                </p>',
            'meta_title' => '',
            'meta_keyword' => '',
            'meta_description' => '',
            'position' => 'menu-utama',
            'published' => '1',
            'link' => 'contact-us',
            'more_config' => '1',
            'admin_config' => '1',
            'lang' => 'en',
            'equal_id' => $arteqid
        ]);

        $arteqid = uniqid('ART', true);
        $footeraboutid = Article::create([
            'title' => 'Kenapa Bebek Bengil?',
            'conten' => 'Bebek Bengil offer Food & service as well.
                    everything at Bebek Bengil is freshly prepared,
                    using only the finest quality ingredients.',
            'position' => 'footer-about',
            'published' => '1',
            'lang' => 'id',
            'equal_id' => $arteqid
        ]);
        $footerabouten = Article::create([
            'title' => 'Why Bebek Bengil?',
            'conten' => 'Bebek Bengil offer Food & service as well.
                    everything at Bebek Bengil is freshly prepared,
                    using only the finest quality ingredients.',
            'position' => 'footer-about',
            'published' => '1',
            'lang' => 'en',
            'equal_id' => $arteqid
        ]);

        $arteqid = uniqid('ART', true);
        $footercontactid = Article::create([
            'title' => 'Hubungi Kami',
            'conten' => '<p class="address"><i class="icon-location"></i>Jalan Hanoman, Padang Tegal, Ubud, Bali 80571 Indonesia</p>
                <p class="address"><i class="fa fa-phone"></i> +62 361 975489 / 977675</p>
                <p class="address"><i class="fa fa-envelope"></i>
                    <a href="mailto:ubud@bebekbengil.co.id">ubud@bebekbengil.co.id</a><br>
                    <a href="mailto:om.ubud@bebekbengil.co.id">om.ubud@bebekbengil.co.id</a><br>
                    <a href="mailto:sales.ubud@bebekbengil.co.id">sales.ubud@bebekbengil.co.id</a><br>
                </p>',
            'position' => 'footer-hubungikami',
            'published' => '1',
            'lang' => 'id',
            'equal_id' => $arteqid
        ]);
        $footercontacten = Article::create([
            'title' => 'Get In Touch',
            'conten' => '<p class="address"><i class="icon-location"></i>Jalan Hanoman, Padang Tegal, Ubud, Bali 80571 Indonesia</p>
                <p class="address"><i class="fa fa-phone"></i> +62 361 975489 / 977675</p>
                <p class="address"><i class="fa fa-envelope"></i>
                    <a href="mailto:ubud@bebekbengil.co.id">ubud@bebekbengil.co.id</a><br>
                    <a href="mailto:om.ubud@bebekbengil.co.id">om.ubud@bebekbengil.co.id</a><br>
                    <a href="mailto:sales.ubud@bebekbengil.co.id">sales.ubud@bebekbengil.co.id</a><br>
                </p>',
            'position' => 'footer-hubungikami',
            'published' => '1',
            'lang' => 'en',
            'equal_id' => $arteqid
        ]);

        $cfgtlp = WebConfig::create([
            'config_name' => 'no_tlpconfig',
            'value' => '+6281236439561',
        ]);

        $cfgheadertext = WebConfig::create([
            'config_name' => 'headertext_config',
            'value' => 'Dirty Duck Dinner - The Original Crispy Duck Since 1990',
        ]);
    }
}
