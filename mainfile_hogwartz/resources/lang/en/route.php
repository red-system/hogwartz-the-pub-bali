<?php

return [
    'about' => 'about-us',
    'outlet' => 'outlet',
    'package' => 'package',
    'menu' => 'menu',
    'news' => 'news',
    'sistercompany' => 'sister-company',
    'contact' => 'contact-us',
    'reservation' => 'reservation',
    'schedule'	=> 'show-schedule',
    'gallery'	=> 'gallery',
    'confirmation'	=> 'confirmation',
];