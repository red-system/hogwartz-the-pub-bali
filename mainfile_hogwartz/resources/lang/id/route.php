<?php

return [
    'about' => 'tentang-kami',
    'outlet' => 'outlet',
    'package' => 'paket',
    'menu' => 'menu',
    'news' => 'berita',
    'sistercompany' => 'perusahaan-saudara',
    'contact' => 'hubungi-kami',
    'reservation' => 'reservasi',
    'schedule'	=> 'jadwal-pementasan',
    'gallery'	=> 'galeri',
    'confirmation'	=> 'konfirmasi'
];