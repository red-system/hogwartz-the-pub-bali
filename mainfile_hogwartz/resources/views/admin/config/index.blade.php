@extends('layouts.back')

@section('page-heading')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <span>Web Configuration</span>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Other Setting</span>
            </li>
        </ul>
    </div>
    <h3 class="page-title"> Other Setting</h3>
@endsection

@section('conten')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase"> Other Setting</span>
                    </div>

                </div>
                <div class="portlet-body">
                    {!! Form::open(['route' => 'web-config.update', 'method' => 'patch', 'class' => 'form-horizontal']) !!}
                    <?php $tlpcfg = $configs->where('config_name', 'no_tlpconfig')->first(); ?>
                    @if(!empty($tlpcfg))
                        <div class="form-group {!! $errors->has('no_tlpconfig') ? 'has-error' : '' !!}">
                            {!! Form::label('no_tlpconfig', 'No Tlp', ['class' => 'control-label col-md-2']) !!}
                            <div class="col-md-10">
                                <div class="row col-md-10">{!! Form::text('no_tlpconfig', $tlpcfg->value, ['class'=>'form-control', 'required']) !!}</div>
                                {!! $errors->first('no_tlpconfig', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    @endif

                    <?php $headertxtcfg = $configs->where('config_name', 'headertext_config')->first(); ?>
                    @if(!empty($headertxtcfg))
                        <div class="form-group {!! $errors->has('headertext_config') ? 'has-error' : '' !!}">
                            {!! Form::label('headertext_config', 'Address', ['class' => 'control-label col-md-2']) !!}
                            <div class="col-md-10">
                                <div class="row col-md-10">{!! Form::text('headertext_config', $headertxtcfg->value, ['class'=>'form-control', 'required']) !!}</div>
                                {!! $errors->first('headertext_config', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    @endif

                    <?php $emailcfg = $configs->where('config_name', 'email')->first(); ?>
                    @if(!empty($emailcfg))
                        <div class="form-group {!! $errors->has('email_config') ? 'has-error' : '' !!}">
                            {!! Form::label('email_config', 'Email', ['class' => 'control-label col-md-2']) !!}
                            <div class="col-md-10">
                                <div class="row col-md-10">{!! Form::text('email_config', $emailcfg->value, ['class'=>'form-control', 'required']) !!}</div>
                                {!! $errors->first('email_config', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    @endif

                    <?php $newscfg = $configs->where('config_name', 'news-pagination')->first(); ?>
                    @if(!empty($newscfg))
                        <div class="form-group {!! $errors->has('news_pagination') ? 'has-error' : '' !!}">
                            {!! Form::label('news_pagination', 'News Pagination', ['class' => 'control-label col-md-2']) !!}
                            <div class="col-md-10">
                                <div class=" row col-md-2">{!! Form::number('news_pagination', $newscfg->value, ['class'=>'form-control', 'required']) !!}</div>
                                {!! $errors->first('news_pagination', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    @endif

                    <?php $taxmenucfg = $configs->where('config_name', 'tax-menu-info')->first(); ?>
                    @if(!empty($taxmenucfg))
                        <div class="form-group {!! $errors->has('tax_menu_info') ? 'has-error' : '' !!}">
                            {!! Form::label('tax_menu_info', 'Tax Menu Info', ['class' => 'control-label col-md-2']) !!}
                            <div class="col-md-10">
                                <div class="row col-md-10">{!! Form::textarea('tax_menu_info', $taxmenucfg->value, ['class'=>'form-control', 'rows' => '2']) !!}</div>
                                {!! $errors->first('tax_menu_info', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                    @endif
                    <hr>
                    <div class="form-group">
                        <div class="col-md-2"></div>
                        <div class="col-md-10">
                            {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection