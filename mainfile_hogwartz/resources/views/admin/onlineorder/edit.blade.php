@extends('layouts.back')

@section('conten')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ route('onlineorder.index') }}"><span>Online Order</span></a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Edit Order</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Edit Order</h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase"> Edit Order</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <div class="row">
                        <div class="col-md-12">
                            {!! Form::model($order, ['route' => ['onlineorder.update', $order],'method' =>'patch'])!!}
                            <div class="form-group">
                                <label for="name" class="col-md-2">Name</label>
                                <div class="col-md-10">
                                    <p>{{ $order->name }}</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="email" class="col-md-2">Email</label>
                                <div class="col-md-10">
                                    <p>{{ $order->email }}</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="phone" class="col-md-2">Phone</label>
                                <div class="col-md-10">
                                    <p>{{ $order->phone }}</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="phone" class="col-md-2">Active Socmed</label>
                                <div class="col-md-10">
                                    <p>{{ $order->active_socmed }}</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="date" class="col-md-2">Date and Time</label>
                                <div class="col-md-10">
                                    <p>{{ date('d F Y \a\t H:i', strtotime($order->reservation_datetime)) }}</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="remark" class="col-md-2">Remark</label>
                                <div class="col-md-10">
                                    <p>{!! $order->remark !!}</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="remark" class="col-md-2">Pax</label>
                                <div class="col-md-10">
                                    <p>{!! $order->pax !!}</p>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="status" class="col-md-2">Status</label>
                                <div class="col-md-10">
                                    {!! Form::select('status', []+App\OnlineOrder::statusList(), null, ['class'=>'form-control']) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="status" class="col-md-2">QR Code</label>
                                <div class="col-md-10">
                                    {!! QrCode::size(300)->generate($order->id) !!}
                                </div>
                            </div>
                            <div class="col-md-offset-2 col-md-10" style="margin-top: 15px;">
                                {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>

                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection