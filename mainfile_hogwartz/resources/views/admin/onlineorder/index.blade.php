@extends('layouts.back')

@section('custom-css')
    <link href="{{ asset('assets/back/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet"
          type="text/css"/>
    <link href="{{ asset('assets/sweet-alert/sweetalert.css') }}" rel="stylesheet">
@endsection

@section('custom-js')
    <script src="{{ asset('assets/back/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/back/global/plugins/datatables/datatables.min.js') }}"
            type="text/javascript"></script>
    <script src="{{ asset('assets/back/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}"
            type="text/javascript"></script>
    <script type="text/javascript" src="//cdn.datatables.net/plug-ins/1.10.19/api/page.jumpToData().js"></script>
    <script type="text/javascript">
        var TableDatatablesEditable = function () {

            var handleTable = function () {
                var table = $('#sample_editable_1');
                var oTable = table.dataTable({
                    "responsive": true,
                    "lengthMenu": [
                        [5, 10, 15, 20, -1],
                        [5, 10, 15, 20, "All"] // change per page values here
                    ],

                    // set the initial value
                    "pageLength": 10,

                    "language": {
                        "lengthMenu": " _MENU_ records"
                    },
                    "columnDefs": [{ // set default column settings
                        'orderable': true,
                        'targets': [0]
                    }, {
                        "searchable": true,
                        "targets": [0]
                    }],
                    "order": [
                        [0, "asc"]
                    ], // set first column as a default sort by asc
                });

                var tableWrapper = $("#sample_editable_1_wrapper");
            }

            return {
                //main function to initiate the module
                init: function () {
                    handleTable();
                }

            };

        }();

        jQuery(document).ready(function () {
            //TableDatatablesEditable.init();


            var table = $(".datatable-general").DataTable({
                    responsive: !0,
                    pagingType: "full_numbers",
                    responsive: true,
                    lengthMenu: [
                        [5, 10, 15, 20, -1],
                        [5, 10, 15, 20, "All"] // change per page values here
                    ],

                    // set the initial value
                    pageLength: 10,

                    language: {
                        lengthMenu: " _MENU_ records"
                    },
                    columnDefs: [{ // set default column settings
                        orderable: true,
                        targets: [0]
                    }, {
                        searchable: true,
                        targets: [0]
                    }],
                    order: [
                        [0, "asc"]
                    ],
                }
            );
            table.page.jumpToData(46, 0);
        });
    </script>
    <script src="{{ asset('assets/sweet-alert/sweetalert.min.js') }}"></script>
    <script>
        $(document).ready(function () {


            $('.sort-by-status').change(function () {
                var status = $(this).val();
                console.log(status);
                window.location.href = "?status=" + status;
            });


            $(document.body).on('click', '.js-submit-confirm', function (event) {
                event.preventDefault()
                var $form = $(this).closest('form')
                swal({
                        title: "Are you sure?",
                        text: "You can not undo this process!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: 'Yes, delete it!',
                        cancelButtonText: "Cancel",
                        closeOnConfirm: true
                    },
                    function () {
                        $form.submit()
                    });
            })

            $(document.body).on('click', '.js-confirm', function (event) {
                event.preventDefault()
                var $form = $(this).closest('form')
                swal({
                        title: "Are you sure?",
                        text: "You can not undo this process!",
                        type: "warning",
                        showCancelButton: true,
                        confirmButtonColor: '#3598dc',
                        confirmButtonText: 'Yes, confirm it!',
                        cancelButtonText: "Cancel",
                        closeOnConfirm: true
                    },
                    function () {
                        $form.submit()
                    });
            })
        })
    </script>
@endsection

@section('page-heading')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <span>Online Order</span>
            </li>
        </ul>
    </div>
    <h3 class="page-title"> Online Order</h3>
@endsection

@section('conten')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase"> Online Order</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <form class="form-inline">
                        <div class="form-group">
                            <label for="exampleInputName2">Sort By : </label>
                            <select name="status" class="form-control sort-by-status">
                                <option value="all" {{ $status == 'all'?'selected':'' }}>All Status</option>
                                <option value="waiting-confirmation" {{ $status == 'waiting-confirmation'?'selected':'' }}>
                                    Waiting Confirmation
                                </option>
                                <option value="complete" {{ $status == 'complete'?'selected':'' }}>Complete</option>
                                <option value="confirmed" {{ $status == 'confirmed'?'selected':'' }}>Confirmed</option>
                            </select>
                        </div>
                    </form>

                    <br/><br/>
                    <table class="table table-striped table-hover table-bordered datatable-general">
                        <thead>
                        <tr>
                            <td>No</td>
                            <td>Name</td>
                            <td>Email</td>
                            <td>WhatsApp Number</td>
                            <td class="none">Active Socmed</td>
                            <td>Date and Time</td>
                            <td class="none">Remark</td>
                            <td class="none">Pax</td>
                            <td class="col-md-2">Status</td>
                            <td class="col-md-2 hidden">Proof Payment</td>
                            <td class="text-center col-md-2">#</td>
                        </tr>
                        </thead>
                        <tbody>
                        @php($no = 1)
                        @foreach($onlineorders as $od)
                            <tr>
                                <td>{{ $no++ }}</td>
                                <td>{{ $od->name }}</td>
                                <td><a href="mailto:{{ $od->email }}">{{ $od->email }}</a></td>
                                <td>
                                    <a href="https://api.whatsapp.com/send?phone=<?php echo $od->phone ?>&text=Hello,%20<?php echo $od->name ?>"
                                       target="_blank">
                                        {{ $od->phone }}
                                    </a>
                                </td>
                                <td>
                                    <p>{!! $od->active_socmed !!}</p>
                                </td>
                                <td>{{ $od->reservation_datetime }}</td>
                                <td>
                                    <p>{!! $od->remark !!}</p>
                                </td>
                                <td>{{ $od->pax }}</td>
                                <td>
                                    <span class="label {{ $od->label_status }}">{{ $od->human_status }}</span>
                                </td>
                                <td class="hidden">
                                    @if($od->proof_payment)
                                        <a href="{{ asset('assets/front/images/'.$od->proof_payment) }}" target="_blank"
                                           class="btn btn-success">View Image</a>
                                    @endif
                                </td>
                                <td class="text-center">
                                    {!! Form::model($od, ['route' => ['onlineorder.destroy', $od], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                    <span class="tooltips" data-original-title="Change Status">
                                            <a href="{{ route('onlineorder.edit', $od->id)}}"
                                               class="btn btn-icon-only blue"><i class="fa fa-edit"></i></a>
                                    </span>
                                    <span class="tooltips" data-original-title="Delete">
                                        {!! Form::button('<i class="fa fa-times"></i>', ['type' => 'submit', 'class'=>'btn btn btn-danger js-submit-confirm']) !!}
                                    </span>
                                    {!! Form::close()!!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection