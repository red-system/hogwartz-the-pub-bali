@extends('layouts.back')

@section('page-level-style')
    <link href="{{ asset('assets/back/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('page-js-plugin')
    <script src="{{ asset('assets/back/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection

@section('custom-js')
    @include('admin._editor_textarea_init')
@endsection

@section('page-heading')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <span>Web Connection</span>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('menu-utama.index') }}"><span>Manage Main Menu</span></a>
                <i class="fa fa-circle"></i>
            </li>
            
            <li>
                <a href="{{ route('admin.config.about.index') }}"><span>Manage About Us</span></a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
               <span>Edit  {{ $about->first()->title }}</span>
              
            </li>
        </ul>
    </div>
    <h3 class="page-title"> Edit {{ ucwords(strtolower($about->first()->title)) }}</h3>
@endsection

@section('conten')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" style="display: block; overflow:auto;">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase"> Edit {{ $about->first()->title }}</span>
                    </div>
                </div>
                <div class="portlet-body">
                 {!! Form::open(['route' => ['about.update', $about->first()->equal_id], 'method' => 'patch', 'files' => true, 'id' => 'submit_form', 'novalidate'])!!}
                  
                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('title_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('title_'.$lang, 'Title ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('title_'.$lang, $about->where('lang', $lang)->first()->title, ['class'=>'form-control', 'required']) !!}
                            {!! $errors->first('title_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach
                    
                
                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('description_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('description_'.$lang, 'Description ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::textarea('description_'.$lang, $about->where('lang',$lang)->first()->description, ['class'=>'form-control editor-textarea','rows' => '4']) !!}
                            {!! $errors->first('description_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach
                    
                   
                @if($about->first()->type !=='company-profile')
                     <div class="form-group {!! $errors->has('image') ? 'has-error' : '' !!}">
                        {!! Form::label('image', 'Image (jpeg, png)') !!}
                        {!! Form::file('image') !!}
                        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                        @if (!empty($about->first()) && $about->first()->image !== '')
                            <div class="row" style="margin-top: 15px;">
                                <div class="col-md-8">
                                    <p style="margin: 5px 0;">Current Image:</p>
                                    <div class="thumbnail">
                                        <img src="{{ $about->first()->home_slider }}" class="img-rounded">
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>

                @else
                     <div class="form-group {!! $errors->has('video') ? 'has-error' : '' !!}">
                            {!! Form::label('video', 'Video') !!}
                            {!! Form::text('video', $about->where('lang', $lang)->first()->video, ['class'=>'form-control', 'required']) !!}
                            {!! $errors->first('video', '<p class="help-block">:message</p>') !!}
                        </div>
                @endif


                    <div class="row col-md-12">
                        <hr>
                        <div class="text-right">
                            {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection