@extends('layouts.back')

@section('page-level-style')
    <link href="{{ asset('assets/back/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('page-js-plugin')
    <script src="{{ asset('assets/back/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection

@section('custom-js')
 
@endsection

@section('page-heading')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <span>Web Connection</span>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('menu-utama.index') }}"><span>Manage Main Menu</span></a>
                <i class="fa fa-circle"></i>
            </li>
            
            <li>
                <a href="{{ route('admin.config.menu-konten.index') }}"><span>Manage Menu Content</span></a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
               <span>Manage Category </span>
              
            </li>
        </ul>
    </div>
    <h3 class="page-title"> Edit Category Menu</h3>
@endsection

@section('conten')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" style="display: block; overflow:auto;">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase"> Edit Category Menu</span>
                    </div>
                </div>
                <div class="portlet-body">

                   {!! Form::open(['route' => ['category.update', $category->first()->equal_id], 'method' => 'patch', 'files' => true, 'id' => 'submit_form', 'novalidate'])!!}
                   
                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('category_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('category_'.$lang, 'Category title ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('category_'.$lang, $category->where('lang',$lang)->first()->category, ['class'=>'form-control', 'required']) !!}
                            {!! $errors->first('category_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach

                    <div class="form-group {!! $errors->has('icon') ? 'has-error' : '' !!}">
                        {!! Form::label('icon', 'Icon') !!}
                        {!! Form::select('icon', [
                            'dish'      => 'Dish',
                            'dinner'    => 'Dinner',
                            'dinner-2'  => 'Dinner',
                            'browser'   => 'Menu',
                            'delivery'  => 'Delivery',
                            'pasta'     => 'Pasta',
                            'fish'      => 'Fish',
                            'meat'      => 'Meat',
                            'ice-cream' => 'Ice Cream'
                        ], null, ['class' => 'form-control']) !!}
                        {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}
                    </div>
                    
                
                    <div class="row col-md-12">
                        <hr>
                        <div class="text-right">
                            {!! Form::submit('Save', ['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection