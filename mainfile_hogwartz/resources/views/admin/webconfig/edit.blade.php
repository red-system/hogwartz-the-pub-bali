@extends('layouts.back')

@section('custom-js')
    @include('admin._editor_textarea_init')
@endsection

@section('conten')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <span>Web Configuration</span>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('menu-utama.index') }}"><span>Manage Main Menu</span></a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Edit: {{ $article->where('lang', config('app.default_locale'))->first()->title }}</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Edit: {{ $article->where('lang', config('app.default_locale'))->first()->title }}</h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase"> Edit: {{ $article->where('lang', config('app.default_locale'))->first()->title }}</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(['route' => ['menu-utama.update', $article->first()->equal_id], 'method' => 'patch', 'files' => true, 'id' => 'submit_form', 'novalidate'])!!}
                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('title_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('title_'.$lang, 'Title ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('title_'.$lang, $article->where('lang', $lang)->first()->title, ['class'=>'form-control']) !!}
                            {!! $errors->first('title_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach

                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('conten_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('conten_'.$lang, 'Conten ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::textarea('conten_'.$lang, $article->where('lang', $lang)->first()->conten, ['class'=>'form-control editor-textarea']) !!}
                            {!! $errors->first('conten_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach

                    <div class="form-group {!! $errors->has('image') ? 'has-error' : '' !!}">
                        {!! Form::label('image', 'Image (jpeg, png)') !!}
                        {!! Form::file('image') !!}
                        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                        @if (!empty($article->first()) && $article->first()->thumb_image !== '')
                            <div class="row" style="margin-top: 15px;">
                                <div class="col-md-8">
                                    <p style="margin: 5px 0;">Current Image:</p>
                                    <div class="thumbnail">
                                        <img src="{{ asset('assets/front/images/'.$article->first()->thumb_image) }}" class="img-rounded" width="200">
                                    </div>
                                </div>
                            </div>
                        @endif
                        {{ $article->first()->image }}
                    </div>

                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('meta_title_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('meta_title_'.$lang, 'Meta Title ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('meta_title_'.$lang, $article->where('lang', $lang)->first()->meta_title, ['class'=>'form-control']) !!}
                            {!! $errors->first('meta_title_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {!! $errors->has('meta_keyword_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('meta_keyword_'.$lang, 'Meta Keyword ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('meta_keyword_'.$lang, $article->where('lang', $lang)->first()->meta_keyword, ['class'=>'form-control']) !!}
                            {!! $errors->first('meta_keyword_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {!! $errors->has('meta_description_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('meta_description_'.$lang, 'Meta Description ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('meta_description_'.$lang, $article->where('lang', $lang)->first()->meta_description, ['class'=>'form-control']) !!}
                            {!! $errors->first('meta_description_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach

                    <div class="form-group {!! $errors->has('published') ? 'has-error' : '' !!}">
                        {!! Form::label('published', 'Published') !!}
                        <div class="row">
                            <div class="col-md-12">
                                @if($article->where('lang', config('app.default_locale'))->first()->published == '1')
                                    <label class="radio-inline">{{ Form::radio('published', '1',true) }} Yes</label>
                                    <label class="radio-inline">{{ Form::radio('published', '0',false) }} No</label>
                                @else
                                    <label class="radio-inline">{{ Form::radio('published', '1',false) }} Yes</label>
                                    <label class="radio-inline">{{ Form::radio('published', '0',true) }} No</label>
                                @endif
                            </div>
                        </div>
                        {!! $errors->first('published', '<p class="help-block">:message</p>') !!}
                    </div>
                    <hr>
                    <div class="text-right">
                        {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection