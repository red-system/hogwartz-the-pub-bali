@extends('layouts.back')

@section('custom-js')
    @include('admin._editor_textarea_init')
@endsection

@section('conten')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <span>Web Configuration</span>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('menu-utama.index') }}"><span>Footer Conten</span></a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Edit Footer</span>
            </li>
        </ul>
    </div>
    <!-- END PAGE BAR -->
    <!-- BEGIN PAGE TITLE-->
    <h3 class="page-title"> Edit Footer</h3>
    <!-- END PAGE TITLE-->
    <!-- END PAGE HEADER-->
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase"> Edit Footer</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(['route' => ['footer.update', $article->first()->equal_id], 'method' => 'patch', 'id' => 'submit_form', 'novalidate'])!!}
                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('title_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('title_'.$lang, 'Title ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('title_'.$lang, $article->where('lang', $lang)->first()->title, ['class'=>'form-control']) !!}
                            {!! $errors->first('title_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach

                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('conten_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('conten_'.$lang, 'Conten ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::textarea('conten_'.$lang, $article->where('lang', $lang)->first()->conten, ['class'=>'form-control editor-textarea']) !!}
                            {!! $errors->first('conten_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach

                    <hr>
                    <div class="text-right">
                        {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection