@extends('layouts.back')

@section('page-level-style')
    <link href="{{ asset('assets/back/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('page-js-plugin')
    <script src="{{ asset('assets/back/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection

@section('custom-js')
   <!--  <script>
        $(document).ready(function () {
            if ($(document.getElementById("slidertype")).length > 0) {
                var e = document.getElementById("slidertype");
                var selected = e.options[e.selectedIndex].value;
                if (selected === 'undefined' || selected === 'tipe1') {
                    $('#text-slider').show();
                } else {
                    $('#text-slider').hide();
                }

                $(document.getElementById("slidertype")).change(function () {
                    var e = document.getElementById("slidertype");
                    var selected = e.options[e.selectedIndex].value;
                    if (selected === 'undefined' || selected === 'tipe1') {
                        $('#text-slider').show();
                    } else {
                        $('#text-slider').hide();
                    }
                });
            }
        });
    </script> -->
@endsection

@section('page-heading')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ route('menu-utama.index') }}"><span>Manage Main Menu</span></a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('admin.config.homeslider.index') }}"><span>{{ $gallery->title }}</span></a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Add New</span>
            </li>
        </ul>
    </div>
    <h3 class="page-title"> New Image Gallery</h3>
@endsection

@section('conten')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" style="display: block; overflow:auto;">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase"> New Image Gallery</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(['method' => 'patch', 'route' => ['gallery.update', $image->first()->equal_id], 'files' => true, 'id' => 'submit_form', 'novalidate'])!!}
                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('name_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('name_'.$lang, 'Name ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('name_'.$lang, $image->where('lang', $lang)->first()->name, ['class'=>'form-control', 'required']) !!}
                            {!! $errors->first('name_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach


                    <div class="form-group {!! $errors->has('image') ? 'has-error' : '' !!}">
                        {!! Form::label('image', 'Image (jpeg, png)') !!}
                        {!! Form::file('image') !!}
                        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                        @if (!empty($image->first()) && $image->first()->image !== '')
                            <div class="row" style="margin-top: 15px;">
                                <div class="col-md-8">
                                    <p style="margin: 5px 0;">Current Image:</p>
                                    <div class="thumbnail">
                                        <img src="{{ asset('assets/front/images/' . $image->first()->image) }}" class="img-responsive">
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>
                    <div class="row col-md-12">
                        <hr>
                        <div class="text-right">
                            {!! Form::submit('Save', ['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection