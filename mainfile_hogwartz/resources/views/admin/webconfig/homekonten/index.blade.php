@extends('layouts.back')

@section('custom-css')
    <link href="{{ asset('assets/back/global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/back/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
    <link href="{{ asset('assets/sweet-alert/sweetalert.css') }}" rel="stylesheet">
@endsection

@section('custom-js')
    <script src="{{ asset('assets/back/global/scripts/datatable.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/back/global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/back/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
    <script type="text/javascript">
        var TableDatatablesEditable = function () {

            var handleTable = function () {
                var table = $('#sample_editable_1');
                var oTable = table.dataTable({
                    "lengthMenu": [
                        [5, 10, 15, 20, -1],
                        [5, 10, 15, 20, "All"] // change per page values here
                    ],

                    // set the initial value
                    "pageLength": 10,

                    "language": {
                        "lengthMenu": " _MENU_ records"
                    },
                    "columnDefs": [{ // set default column settings
                        'orderable': true,
                        'targets': [0]
                    }, {
                        "searchable": true,
                        "targets": [0]
                    }],
                    "order": [
                        [0, "asc"]
                    ] // set first column as a default sort by asc
                });

                var tableWrapper = $("#sample_editable_1_wrapper");
            }

            return {
                //main function to initiate the module
                init: function () {
                    handleTable();
                }

            };

        }();

        jQuery(document).ready(function() {
            TableDatatablesEditable.init();
        });

        
    </script>
    <script src="{{ asset('assets/sweet-alert/sweetalert.min.js') }}"></script>
    <script>
        $(document).ready(function () {
            $(document.body).on('click', '.js-submit-confirm', function (event) {
                event.preventDefault()
                var $form = $(this).closest('form')
                swal({
                            title: "Are you sure?",
                            text: "You can not undo this process!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: '#DD6B55',
                            confirmButtonText: 'Yes, delete it!',
                            cancelButtonText: "Cancel",
                            closeOnConfirm: true
                        },
                        function () {
                            $form.submit()
                        });
            })
        })
    </script>

    @include('admin._editor_textarea_init')

@endsection

@section('page-heading')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <span>Web Connection</span>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('menu-utama.index') }}"><span>Manage Main Menu</span></a>
                <i class="fa fa-circle"></i>
            </li>
            
            <li>
                <span>Manage Home Content</span>
            </li>
        </ul>
    </div>
    <h3 class="page-title"> Manage Home Content</h3>
@endsection

@section('conten')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase"> Discover</span>
                    </div>

                </div>
                <div class="portlet-body">
                   {!! Form::open(['route' => ['discover.update', $article->first()->equal_id], 'method' => 'patch', 'id' => 'submit_form', 'novalidate'])!!}
                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('title_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('title_'.$lang, 'Title ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('title_'.$lang, $article->where('lang', $lang)->first()->title, ['class'=>'form-control']) !!}
                            {!! $errors->first('title_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach

                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('conten_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('conten_'.$lang, 'Conten ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::textarea('conten_'.$lang, $article->where('lang', $lang)->first()->conten, ['class'=>'form-control editor-textarea']) !!}
                            {!! $errors->first('conten_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach
                    <div class="text-right">
                        {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase">Video</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <iframe width="854" height="480" src="{{ str_replace('watch?v=','embed/', $video->link) }}" frameborder="0" allowfullscreen></iframe>

                   
                      {!! Form::model($video,['route' => ['videos.update', $video],'method' => 'patch', 'id' => 'submit_form', 'novalidate']) !!}
                   
                        <div class="form-group {!! $errors->has('link') ? 'has-error' : '' !!}">
                            {!! Form::label('Link', 'Link Video') !!}
                            {!! Form::text('link',null, ['class'=>'form-control', 'required']) !!}
                            {!! $errors->first('link', '<p class="help-block">:message</p>') !!}
                        </div>
                   
                    <hr>
                    <div class="text-right">
                        {!! Form::submit('Update Video', ['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                   
                    
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase"> Our Specialities</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a href="{{ route('specialities.create') }}">
                                        <button id="sample_editable_1_new" class="btn sbold green"> Add New
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                        <thead>
                        <tr>
                            <td>Title</td>
                            <td>Description</td>
                            <td class="text-center col-md-2">#</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($specials as $row)
                            <tr>
                                <td>{{ $row->title }}</td>
                                <td>{!! $row->description !!}</td>
                                <td class="text-center col-md-2">
                                    {!! Form::open(['route' => ['specialities.destroy', $row->equal_id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                    <span class="tooltips" data-original-title="Edit">
                                            <a href="{{ route('specialities.edit', $row->equal_id)}}" class="btn btn-icon-only blue"><i class="fa fa-edit"></i></a>
                                    </span>
                                    <span class="tooltips" data-original-title="Delete">
                                        {!! Form::button('<i class="fa fa-times"></i>', ['type' => 'submit', 'class'=>'btn btn-icon-only red js-submit-confirm']) !!}
                                    </span>
                                    {!! Form::close()!!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

        
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase">CHEF CHOICE - Daily Special</span>
                    </div>

                </div>
                <div class="portlet-body">
                <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a href="{{ route('menus.createRecommend') }}">
                                        <button id="sample_editable_1_new" class="btn sbold green"> Add New
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                        <thead>
                        <tr>
                            <td>Name</td>
                            <td>Price</td>
                            <td>Category</td>
                            <td class="text-center col-md-2">#</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($menus as $row)
                            <tr>
                                <td>{{ $row->name }}</td>
                                <td>{{ $row->price }}</td>
                                <td>
                                    <?php $categories =  $row->categories()->where('lang', config('app.default_locale'))->where('type', 'menu')->get(); ?>
                                    @foreach($categories as $cat)
                                        @if($categories->last() == $cat)
                                            {{ $cat->category }}
                                        @else
                                            {{ $cat->category.', ' }}
                                        @endif
                                    @endforeach
                                </td>
                                <td class="text-center col-md-2">
                                {!! Form::open(['route' => ['menus.destroyRecommend', $row->equal_id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                    <span class="tooltips" data-original-title="Delete">
                                        {!! Form::button('<i class="fa fa-times"></i>', ['type' => 'submit', 'class'=>'btn btn btn-danger js-submit-confirm']) !!}
                                    </span>
                                 {!! Form::close()!!}
                                </td>
                            </tr>
                        @endforeach
                      
                        </tbody>
                    </table>
                </div>
                <hr>
                {!! Form::open(['method' => 'patch', 'route' => 'dsimage.update', 'files' => true, 'id' => 'submit_form', 'novalidate'])!!}
                <div class="form-group {!! $errors->has('dsimage') ? 'has-error' : '' !!}">
                    {!! Form::label('dsimage', 'Daily Special Image (jpeg, png)') !!}
                    {!! Form::file('dsimage') !!}
                    {!! $errors->first('dsimage', '<p class="help-block">:message</p>') !!}
                    @if (!empty($dsimage))
                        <div class="row" style="margin-top: 15px;">
                            <div class="col-md-8">
                                <p style="margin: 5px 0;">Current Image:</p>
                                <div class="thumbnail">
                                    <img src="{{ asset('assets/front/images/'.$dsimage->image) }}" class="img-rounded">
                                </div>
                            </div>
                        </div>
                    @endif
                </div>
                <hr>
                <div class="text-right">
                    {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
                </div>
                {!! Form::close() !!}
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase"> Services</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a href="{{ route('services.create') }}">
                                        <button id="sample_editable_1_new" class="btn sbold green"> Add New
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                        <thead>
                        <tr>
                            <td>Title</td>
                            <td>Description</td>
                            <td class="text-center col-md-2">#</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($services as $row)
                            <tr>
                                <td>{{ $row->title }}</td>
                                <td>{!! $row->description !!}</td>
                                <td class="text-center col-md-2">
                                    {!! Form::open(['route' => ['services.destroy', $row->equal_id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                    <span class="tooltips" data-original-title="Edit">
                                            <a href="{{ route('services.edit', $row->equal_id)}}" class="btn btn-icon-only blue"><i class="fa fa-edit"></i></a>
                                    </span>
                                    <span class="tooltips" data-original-title="Delete">
                                        {!! Form::button('<i class="fa fa-times"></i>', ['type' => 'submit', 'class'=>'btn btn-icon-only red js-submit-confirm']) !!}
                                    </span>
                                    {!! Form::close()!!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>

        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase"> Testimonials</span>
                    </div>

                </div>
                <div class="portlet-body">
                    <div class="table-toolbar">
                        <div class="row">
                            <div class="col-md-6">
                                <div class="btn-group">
                                    <a href="{{ route('testimonial.create') }}">
                                        <button id="sample_editable_1_new" class="btn sbold green"> Add New
                                            <i class="fa fa-plus"></i>
                                        </button>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <table class="table table-striped table-hover table-bordered" id="sample_editable_1">
                        <thead>
                        <tr>
                            <td>Title</td>
                            <td>Description</td>
                            <td class="text-center col-md-2">#</td>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach($testimonials as $row)
                            <tr>
                                <td>{{ $row->name }}</td>
                                <td>{!! $row->testimonial !!}</td>
                                <td class="text-center col-md-2">
                                    {!! Form::open(['route' => ['testimonial.destroy', $row->equal_id], 'method' => 'delete', 'class' => 'form-inline'] ) !!}
                                    <span class="tooltips" data-original-title="Edit">
                                            <a href="{{ route('testimonial.edit', $row->equal_id)}}" class="btn btn-icon-only blue"><i class="fa fa-edit"></i></a>
                                    </span>
                                    <span class="tooltips" data-original-title="Delete">
                                        {!! Form::button('<i class="fa fa-times"></i>', ['type' => 'submit', 'class'=>'btn btn-icon-only red js-submit-confirm']) !!}
                                    </span>
                                    {!! Form::close()!!}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
       
    </div>
@endsection