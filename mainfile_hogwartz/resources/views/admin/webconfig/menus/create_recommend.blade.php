@extends('layouts.back')

@section('page-level-style')
    <link href="{{ asset('assets/back/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('page-js-plugin')
    <script src="{{ asset('assets/back/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection

@section('custom-js')
    @include('admin._editor_textarea_init')
@endsection

@section('page-heading')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <span>Web Connection</span>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('menu-utama.index') }}"><span>Manage Main Menu</span></a>
                <i class="fa fa-circle"></i>
            </li>
            
            <li>
                <a href="{{ route('admin.config.home-konten.index') }}"><span>Manage Home Content</span></a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
               <span>Add New Recomend Menus</span>
            </li>
        </ul>
    </div>
    <h3 class="page-title"> Add New Recomend Menus</h3>
@endsection

@section('conten')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" style="display: block; overflow:auto;">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase"> Add New Recomend Menus</span>
                    </div>
                </div>
                <div class="portlet-body">
                 {!! Form::open(['route' => ['menus.updateRecommend'], 'files' => true, 'id' => 'submit_form', 'novalidate'])!!}
                  
                  <div class="form-group {!! $errors->has('menu') ? 'has-error' : '' !!}">
                        {!! Form::label('menu', 'Choose Menu') !!}
                        {!! Form::select('menu',$list_menus, null, ['class'=>'form-control']) !!}
                        {!! $errors->first('menu', '<p class="help-block">:message</p>') !!}
                    </div>

                    <div class="row col-md-12">
                        <hr>
                        <div class="text-right">
                            {!! Form::submit('Save', ['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection