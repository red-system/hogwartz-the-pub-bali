@extends('layouts.back')

@section('custom-js')
    @include('admin._editor_textarea_init')
@endsection

@section('page-heading')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <span>Web Connection</span>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('menu-utama.index') }}"><span>Manage Main Menu</span></a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Edit Additional Menu Conten</span>
            </li>
        </ul>
    </div>
    <h3 class="page-title"> Edit Additional Menu Conten</h3>
@endsection

@section('conten')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" style="display: block; overflow:auto;">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase"> Edit Additional Menu Conten</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(['route' => ['menus-additional.update'], 'method' => 'patch', 'files' => true, 'id' => 'submit_form', 'novalidate'])!!}

                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('additional_conten_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('additional_conten_'.$lang, 'Conten ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::textarea('additional_conten_'.$lang, $artmenus->where('lang', $lang)->first()->additional_conten, ['class'=>'form-control editor-textarea','rows' => '4']) !!}
                            {!! $errors->first('additional_conten_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach

                    <div class="row col-md-12">
                        <hr>
                        <div class="text-right">
                            {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection