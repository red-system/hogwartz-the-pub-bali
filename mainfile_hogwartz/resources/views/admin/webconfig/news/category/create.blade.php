@extends('layouts.back')

@section('page-heading')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ route('menu-utama.index') }}"><span>Manage Main Menu</span></a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('admin.config.news.index') }}"><span>News</span></a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Add Category</span>
            </li>
        </ul>
    </div>
    <h3 class="page-title"> Add Category</h3>
@endsection

@section('conten')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" style="display: block; overflow:auto;">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase"> Add News Category</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(['route' => 'news-category.store', 'id' => 'submit_form', 'novalidate'])!!}

                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('category_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('category_'.$lang, 'Category ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('category_'.$lang, null, ['class'=>'form-control', 'required']) !!}
                            {!! $errors->first('category_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach

                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('meta_title_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('meta_title_'.$lang, 'Meta Title ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('meta_title_'.$lang, null, ['class'=>'form-control']) !!}
                            {!! $errors->first('meta_title_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {!! $errors->has('meta_keyword_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('meta_keyword_'.$lang, 'Meta Keyword ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('meta_keyword_'.$lang, null, ['class'=>'form-control']) !!}
                            {!! $errors->first('meta_keyword_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {!! $errors->has('meta_description_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('meta_description_'.$lang, 'Meta Description ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::textarea('meta_description_'.$lang, null, ['class'=>'form-control', 'rows' => '3']) !!}
                            {!! $errors->first('meta_description_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach

                    <div class="row col-md-12">
                        <hr>
                        <div class="text-right">
                            {!! Form::submit('Save', ['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection