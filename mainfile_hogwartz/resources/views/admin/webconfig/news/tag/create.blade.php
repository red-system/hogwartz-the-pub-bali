@extends('layouts.back')

@section('page-heading')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ route('menu-utama.index') }}"><span>Manage Main Menu</span></a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('admin.config.news.index') }}"><span>News</span></a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Add Tag</span>
            </li>
        </ul>
    </div>
    <h3 class="page-title"> Add Tag</h3>
@endsection

@section('conten')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" style="display: block; overflow:auto;">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase"> Add News Tag</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(['route' => 'news-tag.store', 'id' => 'submit_form', 'novalidate'])!!}

                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('tag_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('tag_'.$lang, 'Tag ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('tag_'.$lang, null, ['class'=>'form-control', 'required']) !!}
                            {!! $errors->first('tag_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach

                    <div class="row col-md-12">
                        <hr>
                        <div class="text-right">
                            {!! Form::submit('Save', ['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection