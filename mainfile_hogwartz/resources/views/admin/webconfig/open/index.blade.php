@extends('layouts.back')

@section('page-level-style')
    <link href="{{ asset('assets/back/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('page-js-plugin')
    <script src="{{ asset('assets/back/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection

@section('custom-js')
 
@endsection

@section('page-heading')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <span>Web Connection</span>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('menu-utama.index') }}"><span>Footer Content</span></a>
                <i class="fa fa-circle"></i>
            </li>
            
            <li>
                <span>Edit Opening & Closing Hours</a>

            </li>
        </ul>
    </div>
    <h3 class="page-title"> Edit Opening & Closing Hours</h3>
@endsection

@section('conten')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" style="display: block; overflow:auto;">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase"> Edit Opening & Closing Hours</span>
                    </div>
                </div>
                <div class="portlet-body">

                   {!! Form::open(['route' => ['admin.config.open.update'], 'method' => 'patch', 'files' => true, 'id' => 'submit_form', 'novalidate'])!!}

                    <div class="col-md-6">
                        <div class="form-group {!! $errors->has('icon') ? 'has-error' : '' !!}">
                            {!! Form::label('monday_open', 'Monday Open') !!}
                            {!! Form::select('monday_open', [
                                "1AM" => "1AM",
                                "2AM" => "2AM",
                                "3AM" => "3AM",
                                "4AM" => "4AM",
                                "5AM" => "5AM",
                                "6AM" => "6AM",
                                "7AM" => "7AM",
                                "8AM" => "8AM",
                                "9AM" => "9AM",
                                "10AM" => "10AM",
                                "11AM" => "11AM",
                                "12AM" => "12AM",
                                "1PM" => "1PM",
                                "2PM" => "2PM",
                                "3PM" => "3PM",
                                "4PM" => "4PM",
                                "5PM" => "5PM",
                                "6PM" => "6PM",
                                "7PM" => "7PM",
                                "8PM" => "8PM",
                                "9PM" => "9PM",
                                "10PM" => "10PM",
                                "11PM" => "11PM",
                                "12PM" => "12PM",
                                "Closed" => "Closed"
                            ], $monday_open , ['class' => 'form-control']) !!}
                            {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group {!! $errors->has('icon') ? 'has-error' : '' !!}">
                            {!! Form::label('monday_close', 'Monday Close') !!}
                            {!! Form::select('monday_close', [
                                "1AM" => "1AM",
                                "2AM" => "2AM",
                                "3AM" => "3AM",
                                "4AM" => "4AM",
                                "5AM" => "5AM",
                                "6AM" => "6AM",
                                "7AM" => "7AM",
                                "8AM" => "8AM",
                                "9AM" => "9AM",
                                "10AM" => "10AM",
                                "11AM" => "11AM",
                                "12AM" => "12AM",
                                "1PM" => "1PM",
                                "2PM" => "2PM",
                                "3PM" => "3PM",
                                "4PM" => "4PM",
                                "5PM" => "5PM",
                                "6PM" => "6PM",
                                "7PM" => "7PM",
                                "8PM" => "8PM",
                                "9PM" => "9PM",
                                "10PM" => "10PM",
                                "11PM" => "11PM",
                                "12PM" => "12PM",
                                "Closed" => "Closed"
                            ], $monday_close, ['class' => 'form-control']) !!}
                            {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group {!! $errors->has('icon') ? 'has-error' : '' !!}">
                            {!! Form::label('tuesday_open', 'Tuesday Open') !!}
                            {!! Form::select('tuesday_open', [
                                "1AM" => "1AM",
                                "2AM" => "2AM",
                                "3AM" => "3AM",
                                "4AM" => "4AM",
                                "5AM" => "5AM",
                                "6AM" => "6AM",
                                "7AM" => "7AM",
                                "8AM" => "8AM",
                                "9AM" => "9AM",
                                "10AM" => "10AM",
                                "11AM" => "11AM",
                                "12AM" => "12AM",
                                "1PM" => "1PM",
                                "2PM" => "2PM",
                                "3PM" => "3PM",
                                "4PM" => "4PM",
                                "5PM" => "5PM",
                                "6PM" => "6PM",
                                "7PM" => "7PM",
                                "8PM" => "8PM",
                                "9PM" => "9PM",
                                "10PM" => "10PM",
                                "11PM" => "11PM",
                                "12PM" => "12PM",
                                "Closed" => "Closed"
                            ], $tuesday_open, ['class' => 'form-control']) !!}
                            {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group {!! $errors->has('icon') ? 'has-error' : '' !!}">
                            {!! Form::label('tuesday_close', 'Tuesday Close') !!}
                            {!! Form::select('tuesday_close', [
                                "1AM" => "1AM",
                                "2AM" => "2AM",
                                "3AM" => "3AM",
                                "4AM" => "4AM",
                                "5AM" => "5AM",
                                "6AM" => "6AM",
                                "7AM" => "7AM",
                                "8AM" => "8AM",
                                "9AM" => "9AM",
                                "10AM" => "10AM",
                                "11AM" => "11AM",
                                "12AM" => "12AM",
                                "1PM" => "1PM",
                                "2PM" => "2PM",
                                "3PM" => "3PM",
                                "4PM" => "4PM",
                                "5PM" => "5PM",
                                "6PM" => "6PM",
                                "7PM" => "7PM",
                                "8PM" => "8PM",
                                "9PM" => "9PM",
                                "10PM" => "10PM",
                                "11PM" => "11PM",
                                "12PM" => "12PM",
                                "Closed" => "Closed"
                            ], $tuesday_close, ['class' => 'form-control']) !!}
                            {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group {!! $errors->has('icon') ? 'has-error' : '' !!}">
                            {!! Form::label('wednesday_open', 'Wednesday Open') !!}
                            {!! Form::select('wednesday_open', [
                                "1AM" => "1AM",
                                "2AM" => "2AM",
                                "3AM" => "3AM",
                                "4AM" => "4AM",
                                "5AM" => "5AM",
                                "6AM" => "6AM",
                                "7AM" => "7AM",
                                "8AM" => "8AM",
                                "9AM" => "9AM",
                                "10AM" => "10AM",
                                "11AM" => "11AM",
                                "12AM" => "12AM",
                                "1PM" => "1PM",
                                "2PM" => "2PM",
                                "3PM" => "3PM",
                                "4PM" => "4PM",
                                "5PM" => "5PM",
                                "6PM" => "6PM",
                                "7PM" => "7PM",
                                "8PM" => "8PM",
                                "9PM" => "9PM",
                                "10PM" => "10PM",
                                "11PM" => "11PM",
                                "12PM" => "12PM",
                                "Closed" => "Closed"
                            ], $wednesday_open, ['class' => 'form-control']) !!}
                            {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group {!! $errors->has('icon') ? 'has-error' : '' !!}">
                            {!! Form::label('wednesday_close', 'Wednesday Close') !!}
                            {!! Form::select('wednesday_close', [
                                "1AM" => "1AM",
                                "2AM" => "2AM",
                                "3AM" => "3AM",
                                "4AM" => "4AM",
                                "5AM" => "5AM",
                                "6AM" => "6AM",
                                "7AM" => "7AM",
                                "8AM" => "8AM",
                                "9AM" => "9AM",
                                "10AM" => "10AM",
                                "11AM" => "11AM",
                                "12AM" => "12AM",
                                "1PM" => "1PM",
                                "2PM" => "2PM",
                                "3PM" => "3PM",
                                "4PM" => "4PM",
                                "5PM" => "5PM",
                                "6PM" => "6PM",
                                "7PM" => "7PM",
                                "8PM" => "8PM",
                                "9PM" => "9PM",
                                "10PM" => "10PM",
                                "11PM" => "11PM",
                                "12PM" => "12PM",
                                "Closed" => "Closed"
                            ], $wednesday_close, ['class' => 'form-control']) !!}
                            {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group {!! $errors->has('icon') ? 'has-error' : '' !!}">
                            {!! Form::label('thursday_open', 'Thursday Open') !!}
                            {!! Form::select('thursday_open', [
                                "1AM" => "1AM",
                                "2AM" => "2AM",
                                "3AM" => "3AM",
                                "4AM" => "4AM",
                                "5AM" => "5AM",
                                "6AM" => "6AM",
                                "7AM" => "7AM",
                                "8AM" => "8AM",
                                "9AM" => "9AM",
                                "10AM" => "10AM",
                                "11AM" => "11AM",
                                "12AM" => "12AM",
                                "1PM" => "1PM",
                                "2PM" => "2PM",
                                "3PM" => "3PM",
                                "4PM" => "4PM",
                                "5PM" => "5PM",
                                "6PM" => "6PM",
                                "7PM" => "7PM",
                                "8PM" => "8PM",
                                "9PM" => "9PM",
                                "10PM" => "10PM",
                                "11PM" => "11PM",
                                "12PM" => "12PM",
                                "Closed" => "Closed"
                            ], $thursday_open, ['class' => 'form-control']) !!}
                            {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group {!! $errors->has('icon') ? 'has-error' : '' !!}">
                            {!! Form::label('thursday_close', 'Thursday Close') !!}
                            {!! Form::select('thursday_close', [
                                "1AM" => "1AM",
                                "2AM" => "2AM",
                                "3AM" => "3AM",
                                "4AM" => "4AM",
                                "5AM" => "5AM",
                                "6AM" => "6AM",
                                "7AM" => "7AM",
                                "8AM" => "8AM",
                                "9AM" => "9AM",
                                "10AM" => "10AM",
                                "11AM" => "11AM",
                                "12AM" => "12AM",
                                "1PM" => "1PM",
                                "2PM" => "2PM",
                                "3PM" => "3PM",
                                "4PM" => "4PM",
                                "5PM" => "5PM",
                                "6PM" => "6PM",
                                "7PM" => "7PM",
                                "8PM" => "8PM",
                                "9PM" => "9PM",
                                "10PM" => "10PM",
                                "11PM" => "11PM",
                                "12PM" => "12PM",
                                "Closed" => "Closed"
                            ], $thursday_close, ['class' => 'form-control']) !!}
                            {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group {!! $errors->has('icon') ? 'has-error' : '' !!}">
                            {!! Form::label('friday_open', 'Friday Open') !!}
                            {!! Form::select('friday_open', [
                                "1AM" => "1AM",
                                "2AM" => "2AM",
                                "3AM" => "3AM",
                                "4AM" => "4AM",
                                "5AM" => "5AM",
                                "6AM" => "6AM",
                                "7AM" => "7AM",
                                "8AM" => "8AM",
                                "9AM" => "9AM",
                                "10AM" => "10AM",
                                "11AM" => "11AM",
                                "12AM" => "12AM",
                                "1PM" => "1PM",
                                "2PM" => "2PM",
                                "3PM" => "3PM",
                                "4PM" => "4PM",
                                "5PM" => "5PM",
                                "6PM" => "6PM",
                                "7PM" => "7PM",
                                "8PM" => "8PM",
                                "9PM" => "9PM",
                                "10PM" => "10PM",
                                "11PM" => "11PM",
                                "12PM" => "12PM",
                                "Closed" => "Closed"
                            ], $friday_open, ['class' => 'form-control']) !!}
                            {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group {!! $errors->has('icon') ? 'has-error' : '' !!}">
                            {!! Form::label('friday_close', 'Friday Close') !!}
                            {!! Form::select('friday_close', [
                                "1AM" => "1AM",
                                "2AM" => "2AM",
                                "3AM" => "3AM",
                                "4AM" => "4AM",
                                "5AM" => "5AM",
                                "6AM" => "6AM",
                                "7AM" => "7AM",
                                "8AM" => "8AM",
                                "9AM" => "9AM",
                                "10AM" => "10AM",
                                "11AM" => "11AM",
                                "12AM" => "12AM",
                                "1PM" => "1PM",
                                "2PM" => "2PM",
                                "3PM" => "3PM",
                                "4PM" => "4PM",
                                "5PM" => "5PM",
                                "6PM" => "6PM",
                                "7PM" => "7PM",
                                "8PM" => "8PM",
                                "9PM" => "9PM",
                                "10PM" => "10PM",
                                "11PM" => "11PM",
                                "12PM" => "12PM",
                                "Closed" => "Closed"
                            ], $friday_close, ['class' => 'form-control']) !!}
                            {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group {!! $errors->has('icon') ? 'has-error' : '' !!}">
                            {!! Form::label('saturday_open', 'Saturday Open') !!}
                            {!! Form::select('saturday_open', [
                                "1AM" => "1AM",
                                "2AM" => "2AM",
                                "3AM" => "3AM",
                                "4AM" => "4AM",
                                "5AM" => "5AM",
                                "6AM" => "6AM",
                                "7AM" => "7AM",
                                "8AM" => "8AM",
                                "9AM" => "9AM",
                                "10AM" => "10AM",
                                "11AM" => "11AM",
                                "12AM" => "12AM",
                                "1PM" => "1PM",
                                "2PM" => "2PM",
                                "3PM" => "3PM",
                                "4PM" => "4PM",
                                "5PM" => "5PM",
                                "6PM" => "6PM",
                                "7PM" => "7PM",
                                "8PM" => "8PM",
                                "9PM" => "9PM",
                                "10PM" => "10PM",
                                "11PM" => "11PM",
                                "12PM" => "12PM",
                                "Closed" => "Closed"
                            ], $saturday_open, ['class' => 'form-control']) !!}
                            {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group {!! $errors->has('icon') ? 'has-error' : '' !!}">
                            {!! Form::label('saturday_close', 'Saturday Close') !!}
                            {!! Form::select('saturday_close', [
                                "1AM" => "1AM",
                                "2AM" => "2AM",
                                "3AM" => "3AM",
                                "4AM" => "4AM",
                                "5AM" => "5AM",
                                "6AM" => "6AM",
                                "7AM" => "7AM",
                                "8AM" => "8AM",
                                "9AM" => "9AM",
                                "10AM" => "10AM",
                                "11AM" => "11AM",
                                "12AM" => "12AM",
                                "1PM" => "1PM",
                                "2PM" => "2PM",
                                "3PM" => "3PM",
                                "4PM" => "4PM",
                                "5PM" => "5PM",
                                "6PM" => "6PM",
                                "7PM" => "7PM",
                                "8PM" => "8PM",
                                "9PM" => "9PM",
                                "10PM" => "10PM",
                                "11PM" => "11PM",
                                "12PM" => "12PM",
                                "Closed" => "Closed"
                            ], $saturday_close, ['class' => 'form-control']) !!}
                            {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group {!! $errors->has('icon') ? 'has-error' : '' !!}">
                            {!! Form::label('sunday_open', 'Sunday Open') !!}
                            {!! Form::select('sunday_open', [
                                "1AM" => "1AM",
                                "2AM" => "2AM",
                                "3AM" => "3AM",
                                "4AM" => "4AM",
                                "5AM" => "5AM",
                                "6AM" => "6AM",
                                "7AM" => "7AM",
                                "8AM" => "8AM",
                                "9AM" => "9AM",
                                "10AM" => "10AM",
                                "11AM" => "11AM",
                                "12AM" => "12AM",
                                "1PM" => "1PM",
                                "2PM" => "2PM",
                                "3PM" => "3PM",
                                "4PM" => "4PM",
                                "5PM" => "5PM",
                                "6PM" => "6PM",
                                "7PM" => "7PM",
                                "8PM" => "8PM",
                                "9PM" => "9PM",
                                "10PM" => "10PM",
                                "11PM" => "11PM",
                                "12PM" => "12PM",
                                "Closed" => "Closed"
                            ], $sunday_open, ['class' => 'form-control']) !!}
                            {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group {!! $errors->has('icon') ? 'has-error' : '' !!}">
                            {!! Form::label('sunday_close', 'Sunday Close') !!}
                            {!! Form::select('sunday_close', [
                                "1AM" => "1AM",
                                "2AM" => "2AM",
                                "3AM" => "3AM",
                                "4AM" => "4AM",
                                "5AM" => "5AM",
                                "6AM" => "6AM",
                                "7AM" => "7AM",
                                "8AM" => "8AM",
                                "9AM" => "9AM",
                                "10AM" => "10AM",
                                "11AM" => "11AM",
                                "12AM" => "12AM",
                                "1PM" => "1PM",
                                "2PM" => "2PM",
                                "3PM" => "3PM",
                                "4PM" => "4PM",
                                "5PM" => "5PM",
                                "6PM" => "6PM",
                                "7PM" => "7PM",
                                "8PM" => "8PM",
                                "9PM" => "9PM",
                                "10PM" => "10PM",
                                "11PM" => "11PM",
                                "12PM" => "12PM",
                                "Closed" => "Closed"
                            ], $sunday_close, ['class' => 'form-control']) !!}
                            {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    
                
                    <div class="row col-md-12">
                        <hr>
                        <div class="text-right">
                            {!! Form::submit('Save', ['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection