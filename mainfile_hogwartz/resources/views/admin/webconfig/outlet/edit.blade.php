@extends('layouts.back')

@section('page-level-style')
    <link href="{{ asset('assets/back/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('page-js-plugin')
    <script src="{{ asset('assets/back/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection

@section('custom-js')
   @include('admin._editor_textarea_init')
@endsection

@section('page-heading')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <span>Web Connection</span>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('menu-utama.index') }}"><span>Manage Main Menu</span></a>
                <i class="fa fa-circle"></i>
            </li>
            
            <li>
                <a href="{{ route('admin.config.outlet.index') }}"><span>Manage Outlet</span></a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
               <span>Edit Outlet </span>
              
            </li>
        </ul>
    </div>
    <h3 class="page-title"> Edit Outlet</h3>
@endsection

@section('conten')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" style="display: block; overflow:auto;">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase"> Edit Bebek Bengil Outlet</span>
                    </div>
                </div>
                <div class="portlet-body">
                 {!! Form::open(['route' => ['outlet.update', $outlet->first()->equal_id], 'method' => 'patch', 'files' => true, 'id' => 'submit_form', 'novalidate'])!!}
                  
                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('title_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('title_'.$lang, 'Outlet title ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('title_'.$lang, $outlet->where('lang', $lang)->first()->title, ['class'=>'form-control', 'required']) !!}
                            {!! $errors->first('title_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach

                    <div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
                        {!! Form::label('email', 'Email Recipient') !!}
                        {!! Form::text('email', $outlet->first()->email, ['class'=>'form-control', 'required']) !!}
                        {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                    </div>

                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('description_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('description_'.$lang, 'Outlet description ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::textarea('description_'.$lang, $outlet->where('lang',$lang)->first()->description, ['class'=>'form-control editor-textarea','size' => '30x5']) !!}
                            {!! $errors->first('description_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach

                     <div class="form-group {!! $errors->has('image') ? 'has-error' : '' !!}">
                        {!! Form::label('image', 'Image (jpeg, png)') !!}
                        {!! Form::file('image') !!}
                        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                        @if (!empty($outlet->first()) && $outlet->first()->image !== '')
                            <div class="row" style="margin-top: 15px;">
                                <div class="col-md-8">
                                    <p style="margin: 5px 0;">Current Image:</p>
                                    <div class="thumbnail">
                                        <img src="{{ $outlet->first()->home_slider }}" class="img-rounded">
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>

                    <div class="form-group {!! $errors->has('lat') ? 'has-error' : '' !!}">
                        {!! Form::label('lat', 'Latitude') !!}
                        {!! Form::text('lat', $outlet->first()->lat, ['class'=>'form-control', 'required']) !!}
                        {!! $errors->first('lat', '<p class="help-block">:message</p>') !!}
                    </div>

                    <div class="form-group {!! $errors->has('long') ? 'has-error' : '' !!}">
                        {!! Form::label('long', 'Longitude') !!}
                        {!! Form::text('long', $outlet->first()->lng, ['class'=>'form-control', 'required']) !!}
                        {!! $errors->first('long', '<p class="help-block">:message</p>') !!}
                    </div>

                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('address_info_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('address_info_'.$lang, 'Address Information ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::textarea('address_info_'.$lang, $outlet->where('lang',$lang)->first()->address_info, ['class'=>'form-control editor-textarea','rows' => '4']) !!}
                            {!! $errors->first('address_info_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach

                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('meta_title_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('meta_title_'.$lang, 'Meta Title ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('meta_title_'.$lang, $outlet->where('lang',$lang)->first()->meta_title, ['class'=>'form-control']) !!}
                            {!! $errors->first('meta_title_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {!! $errors->has('meta_keyword_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('meta_keyword_'.$lang, 'Meta Keyword ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('meta_keyword_'.$lang, $outlet->where('lang',$lang)->first()->meta_keyword, ['class'=>'form-control']) !!}
                            {!! $errors->first('meta_keyword_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {!! $errors->has('meta_description_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('meta_description_'.$lang, 'Meta Description ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::textarea('meta_description_'.$lang, $outlet->where('lang',$lang)->first()->meta_description, ['class'=>'form-control', 'rows' => '3']) !!}
                            {!! $errors->first('meta_description_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach

                    <div class="row col-md-12">
                        <hr>
                        <div class="text-right">
                            {!! Form::submit('Save', ['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection