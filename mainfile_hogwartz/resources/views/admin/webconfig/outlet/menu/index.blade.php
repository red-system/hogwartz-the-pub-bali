@extends('layouts.back')

@section('custom-js')
@endsection

@section('page-heading')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <a href="{{ route('menu-utama.index') }}"><span>Manage Main Menu</span></a>
                <i class="fa fa-circle"></i>
            </li>

            <li>
                <a href="{{ route('admin.config.outlet.index') }}"><span>Manage Outlet</span></a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Manage Outlet Menu</span>
            </li>
        </ul>
    </div>
    <h3 class="page-title"> Manage Outlet Menu</h3>
@endsection

@section('conten')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" style="display: block; overflow:auto;">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase"> Outlet Menu: {{ $outlet->title }}</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(['route' => ['outlet-menu.update', $outlet->equal_id], 'method' => 'post', 'id' => 'submit_form', 'novalidate'])!!}

                    <div class="form-group {!! $errors->has('menu') ? 'has-error' : '' !!}">
                        {!! Form::label('menu', 'List Menu', ['class' => 'col-md-2']) !!}
                        <div class="col-md-10">
                            @foreach($allmenus as $mn)
                                <div class="checkbox-list">
                                    @if(in_array($mn->id, $outletmenu))
                                        <label>{{ Form::checkbox('menu[]', $mn->equal_id, true) }} {{ $mn->name }}</label>
                                    @else
                                        <label>{{ Form::checkbox('menu[]', $mn->equal_id, false) }} {{ $mn->name }}</label>
                                    @endif
                                </div>
                            @endforeach
                            {!! $errors->first('menu', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                    <div class="row col-md-12">
                        <hr>
                        <div class="col-md-10 col-md-offset-2">
                            {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection