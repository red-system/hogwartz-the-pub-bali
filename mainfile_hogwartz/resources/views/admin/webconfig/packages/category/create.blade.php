@extends('layouts.back')

@section('page-level-style')
    <link href="{{ asset('assets/back/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('page-js-plugin')
    <script src="{{ asset('assets/back/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection

@section('custom-js')
    @include('admin._editor_textarea_init')
@endsection

@section('page-heading')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <span>Web Connection</span>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('menu-utama.index') }}"><span>Manage Main Menu</span></a>
                <i class="fa fa-circle"></i>
            </li>

            <li>
                <a href="{{ route('admin.config.packages.index') }}"><span>Manage Packages</span></a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>New Packages </span>
            </li>
        </ul>
    </div>
    <h3 class="page-title"> New Package</h3>
@endsection

@section('conten')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" style="display: block; overflow:auto;">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase"> New Package</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(['route' => ['packages.store'], 'files' => true, 'id' => 'submit_form', 'novalidate'])!!}
                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('title_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('title_'.$lang, 'Package Title ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('title_'.$lang, null, ['class'=>'form-control', 'required']) !!}
                            {!! $errors->first('title_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach


                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('short_description_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('short_description_'.$lang, 'Short Description ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::textarea('short_description_'.$lang, null, ['class'=>'form-control','rows' => '3']) !!}
                            {!! $errors->first('short_description_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {!! $errors->has('description_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('description_'.$lang, 'Description ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::textarea('description_'.$lang, null, ['class'=>'form-control editor-textarea','rows' => '4']) !!}
                            {!! $errors->first('description_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach

                    <div class="form-group {!! $errors->has('thumb_image') ? 'has-error' : '' !!}">
                        {!! Form::label('thumb_image', 'Thumb Image (jpeg, png)') !!}
                        <div class="row col-md-12" style="margin-bottom: 15px;">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                                <div class="fileinput-new thumbnail" style="width: 280px; height: 170px;">
                                    <img src="http://www.placehold.it/280x170/EFEFEF/AAAAAA&amp;text=no+image" alt="" /> </div>
                                <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                                <div>
                                    <span class="btn default btn-file">
                                        <span class="fileinput-new"> Select image </span>
                                        <span class="fileinput-exists"> Change </span>
                                        {!! Form::file('thumb_image', ['required']) !!} </span>
                                    <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                </div>
                            </div>
                            {!! $errors->first('thumb_image', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('meta_title_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('meta_title_'.$lang, 'Meta Title ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('meta_title_'.$lang, null, ['class'=>'form-control']) !!}
                            {!! $errors->first('meta_title_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {!! $errors->has('meta_keyword_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('meta_keyword_'.$lang, 'Meta Keyword ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('meta_keyword_'.$lang, null, ['class'=>'form-control']) !!}
                            {!! $errors->first('meta_keyword_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {!! $errors->has('meta_description_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('meta_description_'.$lang, 'Meta Description ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::textarea('meta_description_'.$lang, null, ['class'=>'form-control', 'rows' => '3']) !!}
                            {!! $errors->first('meta_description_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach

                    <div class="form-group {!! $errors->has('published') ? 'has-error' : '' !!}">
                        {!! Form::label('published', 'Published') !!}
                        <div class="row">
                            <label class="radio-inline">{{ Form::radio('published', '1',false) }} Yes</label>
                            <label class="radio-inline">{{ Form::radio('published', '0',true) }} No</label>
                        </div>
                        {!! $errors->first('published', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="row col-md-12">
                        <hr>
                        <div class="text-right">
                            {!! Form::submit('Save', ['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection