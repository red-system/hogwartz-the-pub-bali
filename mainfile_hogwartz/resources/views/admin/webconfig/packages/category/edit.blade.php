@extends('layouts.back')

@section('page-level-style')
    <link href="{{ asset('assets/back/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('page-js-plugin')
    <script src="{{ asset('assets/back/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection

@section('custom-js')
    @include('admin._editor_textarea_init')
@endsection

@section('page-heading')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <span>Web Connection</span>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('menu-utama.index') }}"><span>Manage Main Menu</span></a>
                <i class="fa fa-circle"></i>
            </li>

            <li>
                <a href="{{ route('admin.config.packages.index') }}"><span>Manage Packages</span></a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Edit Packages </span>
            </li>
        </ul>
    </div>
    <h3 class="page-title"> Edit Package</h3>
@endsection

@section('conten')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" style="display: block; overflow:auto;">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase"> Edit Package</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(['route' => ['packages.update', $pcategory->first()->equal_id], 'method' => 'patch', 'files' => true, 'id' => 'submit_form', 'novalidate'])!!}

                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('title_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('title_'.$lang, 'Package Title ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('title_'.$lang, $pcategory->where('lang', $lang)->first()->title, ['class'=>'form-control', 'required']) !!}
                            {!! $errors->first('title_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach


                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('short_description_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('short_description_'.$lang, 'Short Description ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::textarea('short_description_'.$lang, $pcategory->where('lang', $lang)->first()->short_description, ['class'=>'form-control','rows' => '3']) !!}
                            {!! $errors->first('short_description_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {!! $errors->has('description_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('description_'.$lang, 'Description ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::textarea('description_'.$lang, $pcategory->where('lang', $lang)->first()->conten, ['class'=>'form-control editor-textarea','rows' => '4']) !!}
                            {!! $errors->first('description_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach

                    <div class="form-group {!! $errors->has('thumb_image') ? 'has-error' : '' !!}">
                        {!! Form::label('thumb_image', 'Thumb Image (jpeg, png)') !!}
                        {!! Form::file('thumb_image') !!}
                        {!! $errors->first('thumb_image', '<p class="help-block">:message</p>') !!}
                        @if (!empty($pcategory->first()) && $pcategory->first()->thumb_image !== '')
                            <div class="row" style="margin-top: 15px;">
                                <div class="col-md-8">
                                    <p style="margin: 5px 0;">Current Image:</p>
                                    <div class="thumbnail">
                                        <img src="{{ asset('assets/front/images/'.$pcategory->first()->thumb_image)  }}" class="img-rounded">
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>

                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('meta_title_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('meta_title_'.$lang, 'Meta Title ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('meta_title_'.$lang, $pcategory->where('lang', $lang)->first()->meta_title, ['class'=>'form-control']) !!}
                            {!! $errors->first('meta_title_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {!! $errors->has('meta_keyword_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('meta_keyword_'.$lang, 'Meta Keyword ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('meta_keyword_'.$lang, $pcategory->where('lang', $lang)->first()->meta_keyword, ['class'=>'form-control']) !!}
                            {!! $errors->first('meta_keyword_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {!! $errors->has('meta_description_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('meta_description_'.$lang, 'Meta Description ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::textarea('meta_description_'.$lang, $pcategory->where('lang', $lang)->first()->meta_description, ['class'=>'form-control', 'rows' => '3']) !!}
                            {!! $errors->first('meta_description_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach

                    <div class="form-group {!! $errors->has('published') ? 'has-error' : '' !!}">
                        {!! Form::label('published', 'Published') !!}
                        <div class="row">
                            @if($pcategory->where('lang', config('app.default_locale'))->first()->published == '1')
                                <label class="radio-inline">{{ Form::radio('published', '1',true) }} Yes</label>
                                <label class="radio-inline">{{ Form::radio('published', '0',false) }} No</label>
                            @else
                                <label class="radio-inline">{{ Form::radio('published', '1',false) }} Yes</label>
                                <label class="radio-inline">{{ Form::radio('published', '0',true) }} No</label>
                            @endif
                        </div>
                        {!! $errors->first('published', '<p class="help-block">:message</p>') !!}
                    </div>

                    <div class="row col-md-12">
                        <hr>
                        <div class="text-right">
                            {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection