@extends('layouts.back')

@section('custom-js')

@endsection

@section('page-heading')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <span>Web Connection</span>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('menu-utama.index') }}"><span>Manage Main Menu</span></a>
                <i class="fa fa-circle"></i>
            </li>
            
            <li>
                <a href="{{ route('admin.config.home-konten.index') }}"><span>Manage Home Content</span></a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
               <span>Manage Services</span>
              
            </li>
        </ul>
    </div>
    <h3 class="page-title"> Edit Services</h3>
@endsection

@section('conten')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase"> Edit : Our Services</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(['route' => ['services.update', $service->first()->equal_id], 'method' => 'patch', 'files' => true, 'id' => 'submit_form', 'novalidate'])!!}
                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('title_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('title_'.$lang, 'Title ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('title_'.$lang, $service->where('lang', $lang)->first()->title, ['class'=>'form-control', 'required']) !!}
                            {!! $errors->first('title_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach

                    <div class="form-group {!! $errors->has('icon') ? 'has-error' : '' !!}">
                        {!! Form::label('icon', 'Icon (png)') !!}
                        {!! Form::file('icon') !!}
                        {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}
                        @if (!empty($service->first()) && $service->first()->icon !== '')
                            <div class="row" style="margin-top: 15px;">
                                <div class="col-md-2" >
                                    <p style="margin: 5px 0;">Current Image:</p>
                                    <div class="thumbnail" style="background-color: #f6f6f6;">
                                        <img src="{{ asset('assets/front/images/'.$service->first()->icon) }}" class="img-rounded">
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>

                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('description_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('description_'.$lang, 'Description ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('description_'.$lang, $service->where('lang', $lang)->first()->description, ['class'=>'form-control', 'required']) !!}
                            {!! $errors->first('description_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach

                    
                    <hr>
                    <div class="text-right">
                        {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection