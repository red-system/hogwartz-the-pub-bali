@extends('layouts.back')

@section('page-level-style')
    <link href="{{ asset('assets/back/global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
@endsection

@section('page-js-plugin')
    <script src="{{ asset('assets/back/global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
@endsection

@section('custom-js')
   <!--  <script>
        $(document).ready(function () {
            if ($(document.getElementById("slidertype")).length > 0) {
                var e = document.getElementById("slidertype");
                var selected = e.options[e.selectedIndex].value;
                if (selected === 'undefined' || selected === 'tipe1') {
                    $('#text-slider').show();
                } else {
                    $('#text-slider').hide();
                }

                $(document.getElementById("slidertype")).change(function () {
                    var e = document.getElementById("slidertype");
                    var selected = e.options[e.selectedIndex].value;
                    if (selected === 'undefined' || selected === 'tipe1') {
                        $('#text-slider').show();
                    } else {
                        $('#text-slider').hide();
                    }
                });
            }
        });
    </script> -->
@endsection

@section('page-heading')
  <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <span>Web Connection</span>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('menu-utama.index') }}"><span>Manage Main Menu</span></a>
                <i class="fa fa-circle"></i>
            </li>
            
            <li>
                <a href="{{ route('admin.config.sistercompany.index') }}"><span>Manage Sister Company</span></a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
               <span>Edit Sister Company </span>
              
            </li>
        </ul>
    </div>
    <h3 class="page-title"> Edit Sister Company</h3>
@endsection

@section('conten')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light bordered" style="display: block; overflow:auto;">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase"> Sister Company</span>
                    </div>
                </div>
                <div class="portlet-body">
                 {!! Form::open(['route' => ['sistercompany.update', $company->first()->equal_id], 'method' => 'patch', 'files' => true, 'id' => 'submit_form', 'novalidate'])!!}
                  
                     @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('title_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('title_'.$lang, 'Sister Company title ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('title_'.$lang, $company->where('lang', $lang)->first()->title, ['class'=>'form-control', 'required']) !!}
                            {!! $errors->first('title_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                        <div class="form-group {!! $errors->has('short_description_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('short_description_'.$lang, 'Short Description ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::textarea('short_description_'.$lang, $company->where('lang', $lang)->first()->short_description, ['class'=>'form-control', 'rows' => '4']) !!}
                            {!! $errors->first('short_description_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach
                    
                    <div class="form-group {!! $errors->has('link') ? 'has-error' : '' !!}">
                            {!! Form::label('link', 'Link') !!}
                            {!! Form::text('link', $company->where('lang', $lang)->first()->link, ['class'=>'form-control', 'required']) !!}
                            {!! $errors->first('link', '<p class="help-block">:message</p>') !!}
                    </div>

                    <div class="form-group {!! $errors->has('image') ? 'has-error' : '' !!}">
                        {!! Form::label('image', 'Image (jpeg, png)') !!}
                        {!! Form::file('image') !!}
                        {!! $errors->first('image', '<p class="help-block">:message</p>') !!}
                        @if (!empty($company->first()) && $company->first()->image !== '')
                            <div class="row" style="margin-top: 15px;">
                                <div class="col-md-4">
                                    <p style="margin: 5px 0;">Current Image:</p>
                                    <div class="thumbnail">
                                        <img src="{{ asset('assets/front/images/'.$company->first()->image) }}" class="img-rounded">
                                    </div>
                                </div>
                            </div>
                        @endif
                    </div>

                    <div class="form-group {!! $errors->has('published') ? 'has-error' : '' !!}">
                        {!! Form::label('published', 'Published') !!}
                        <?php 

                      
                        if($company->where('lang', $lang)->first()->published=='1'){

                            ?>

                        <div class="row">
                            <label class="radio-inline">{{ Form::radio('published', '1','true') }} Yes</label>
                            <label class="radio-inline">{{ Form::radio('published', '0',null) }} No</label>
                        </div>


                        <?php

                           
                        }else{
                            ?>

                        <div class="row">
                            <label class="radio-inline">{{ Form::radio('published', '1',null) }} Yes</label>
                            <label class="radio-inline">{{ Form::radio('published', '0','true') }} No</label>
                        </div>


                        <?php
                        }
                        
                        ?>


                        {!! $errors->first('published', '<p class="help-block">:message</p>') !!}
                    </div>
                    <div class="row col-md-12">
                        <hr>
                        <div class="text-right">
                            {!! Form::submit('Update', ['class'=>'btn btn-primary']) !!}
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection