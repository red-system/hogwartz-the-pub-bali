@extends('layouts.back')

@section('custom-js')

@endsection

@section('page-heading')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <span>Web Connection</span>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('menu-utama.index') }}"><span>Manage Main Menu</span></a>
                <i class="fa fa-circle"></i>
            </li>

            <li>
                <a href="{{ route('admin.config.home-konten.index') }}"><span>Manage Home Content</span></a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Manage Specialities</span>
            </li>
        </ul>
    </div>
    <h3 class="page-title"> Add New Specialities</h3>
@endsection

@section('conten')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase"> Add New Specialities</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(['route' => 'specialities.store', 'method' => 'post', 'files' => true, 'id' => 'submit_form', 'novalidate'])!!}
                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('title_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('title_'.$lang, 'Title ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('title_'.$lang, null, ['class'=>'form-control', 'required']) !!}
                            {!! $errors->first('title_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach

                    <div class="form-group {!! $errors->has('icon') ? 'has-error' : '' !!}">
                        {!! Form::label('icon', 'Icon') !!}
                        {!! Form::file('icon') !!}
                        {!! $errors->first('icon', '<p class="help-block">:message</p>') !!}
                    </div>

                    @foreach(config('app.all_langs') as $lang)
                        <div class="form-group {!! $errors->has('description_'.$lang) ? 'has-error' : '' !!}">
                            {!! Form::label('description_'.$lang, 'Description ('.config('app.human_langs')[$lang].')') !!}
                            {!! Form::text('description_'.$lang, null, ['class'=>'form-control', 'required']) !!}
                            {!! $errors->first('description_'.$lang, '<p class="help-block">:message</p>') !!}
                        </div>
                    @endforeach


                    <hr>
                    <div class="text-right">
                        {!! Form::submit('Save', ['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection