@extends('layouts.back')

@section('custom-js')

@endsection

@section('page-heading')
    <div class="page-bar">
        <ul class="page-breadcrumb">
            <li>
                <span>Web Connection</span>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <a href="{{ route('menu-utama.index') }}"><span>Manage Main Menu</span></a>
                <i class="fa fa-circle"></i>
            </li>

            <li>
                <a href="{{ route('admin.config.home-konten.index') }}"><span>Manage Home Content</span></a>
                <i class="fa fa-circle"></i>
            </li>
            <li>
                <span>Manage Testimonials</span>
            </li>
        </ul>
    </div>
    <h3 class="page-title"> Add New Testimonials</h3>
@endsection

@section('conten')
    <div class="row">
        <div class="col-md-12">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <span class="caption-subject bold uppercase"> Add New Testimonials</span>
                    </div>
                </div>
                <div class="portlet-body">
                    {!! Form::open(['route' => 'testimonial.store', 'method' => 'post', 'files' => true, 'id' => 'submit_form', 'novalidate'])!!}
                        <div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
                            {!! Form::label('name', 'Name') !!}
                            {!! Form::text('name', null, ['class'=>'form-control', 'required']) !!}
                            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group {!! $errors->has('job') ? 'has-error' : '' !!}">
                            {!! Form::label('job', 'Job') !!}
                            {!! Form::text('job', null, ['class'=>'form-control', 'required']) !!}
                            {!! $errors->first('job', '<p class="help-block">:message</p>') !!}
                        </div>

                        <div class="form-group {!! $errors->has('testimonial') ? 'has-error' : '' !!}">
                            {!! Form::label('testimonial', 'Testimonial') !!}
                            {!! Form::text('testimonial', null, ['class'=>'form-control', 'required']) !!}
                            {!! $errors->first('testimonial', '<p class="help-block">:message</p>') !!}
                        </div>

                    <div class="form-group {!! $errors->has('picture') ? 'has-error' : '' !!}">
                        {!! Form::label('picture', 'Picture') !!}
                        {!! Form::file('picture') !!}
                        {!! $errors->first('picture', '<p class="help-block">:message</p>') !!}
                    </div>  
                    <hr>
                    <div class="text-right">
                        {!! Form::submit('Save', ['class'=>'btn btn-primary']) !!}
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->
        </div>
    </div>
@endsection