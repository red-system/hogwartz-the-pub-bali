<p>You have received a new message from Hogwartz The Pub Bali contact form.</p>
<p>Here are the details:</p>
<hr>
<p>
<ul>
    <li>Name : {{ $dataemail['name'] }}</li>
    <li>Email : {{ $dataemail['email'] }}</li>
    <li>Phone : {{ $dataemail['phone'] }}</li>
</ul>
</p>
<hr>
<p>Subject : {{ $dataemail['subject'] }}</p>
<hr>
<p>
    @foreach ($dataemail['messageLines'] as $messageLine)
        {{ $messageLine }}<br>
    @endforeach
</p>
<hr>
<p>That is all.</p>
