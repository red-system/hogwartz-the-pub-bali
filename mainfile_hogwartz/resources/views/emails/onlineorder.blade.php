<p>You have received a new order from Hogwartz The Pub Bali website reservation form.</p>
<p>Here are the details:</p>
<hr>
<p>
<ul>
    <li>Name : {{ $data['name'] }}</li>
    <li>Email : {{ $data['email'] }}</li>
    <li>Phone : {{ $data['phone'] }}</li>
    <li>Reservation Date and Time : {{ date('d F Y \a\t H:i', strtotime($data['reservation_datetime'])) }}</li>
</ul>
</p>
<hr>
<p>Remark:</p>
<p>
    @foreach ($data['remarkLines'] as $remarkLine)
        {{ $remarkLine }}<br>
    @endforeach

    <br><br>Party: {{ $data['party'] }} @if(in_array($data['party'], ['Birthday', 'Wedding Anniversary'])) ({{ $data['name_for_party'] }}) @endif
    <br>Cake: {{ $data['cake'] }}
    <br>Pax: {{ $data['pax'] }}

    <br>Active Socmed:
    @foreach ($data['socmedLines'] as $socmedLine)
        {{ $socmedLine }}<br>
    @endforeach
</p>
<hr>
<p>That is all.</p>
