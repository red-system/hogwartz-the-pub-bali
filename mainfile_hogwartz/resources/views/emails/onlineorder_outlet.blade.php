<p>You have received a new order from Hogwartz The Pub Bali website reservation form.</p>
<p>Here are the details:</p>
<hr>
<p>
<ul>
    <li>Name : {{ $dataemail['name'] }}</li>
    <li>Email : {{ $dataemail['email'] }}</li>
    <li>Phone : {{ $dataemail['phone'] }}</li>
    <li>Reservation Date and Time : {{ date('d F Y \a\t H:i', strtotime($dataemail['reservation_datetime'])) }}</li>
</ul>
</p>
<hr>
<p>Remark:</p>
<p>
    @foreach ($dataemail['remarkLines'] as $remarkLine)
        {{ $remarkLine }}<br>
    @endforeach
</p>
<hr>
<p>That is all.</p>
