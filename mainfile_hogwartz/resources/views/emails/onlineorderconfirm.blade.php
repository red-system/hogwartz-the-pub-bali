<h3 align="center">Reservation Confirmation</h3>
<h5 align="center">Confirmation number: {{ $dataemail->id }}</h5>

<p>Thank you for choosing Hogwartz The Pub Bali . We are pleased to confirm your reservation as under:</p>
<ul>
    <li>Name : {{ $dataemail->name}}</li>
    <li>Email : {{ $dataemail->email }}</li>
    <li>Phone : {{ $dataemail->phone }}</li>
    <li>Reservation Date and Time : {{ date('d F Y \a\t H:i', strtotime($dataemail->reservation_datetime)) }}</li>
</ul>
</p>
<hr>
<p>Remark:</p>
<p>
    {!! $dataemail->remark !!}

    <br>Active Socmed:
    <br>{!! $dataemail->active_socmed !!}
</p>
<hr>
<p>
	@php( $qr = QrCode::format('png')->size(300)->generate($dataemail->id) )
	<img src="{!!$message->embedData($qr, 'QrCode.png', 'image/png')!!}">
</p>
