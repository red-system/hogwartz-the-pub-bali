                    <!-- <div class="swin-widget widget-search">
                      <div class="title-widget">
                        <form accept-charset="utf-8" class="search-form">
                          <input type="search" placeholder="Search" name="s" value="" class="search-input"><span class="search-submit"><i class="fa fa-search"></i></span>
                        </form>
                      </div>
                    </div> -->
                    <!-- categories-->
                    <div class="swin-widget widget-categories">
                      <div class="title-widget">{{ \Lang::get('front.news-category',[], App::getLocale()) }}</div>
                      <div class="widget-body widget-content clearfix">
                        @foreach($newscategories as $nc)
                        <a href="{{ url(preg_replace('#/+#','/', config('app.locale_prefix').'/'.\Lang::get('route.news',[], config('app.locale_prefix')).'/category/'.$nc->slug)) }}" class="link"><i class="icons fa fa-angle-right"></i><span class="text">{{ $nc->category }}</span></a>
                        @endforeach
                        </div>
                    </div>
                    <!-- recent post-->
                    <div class="swin-widget widget-recent-post">
                      <div class="title-widget">{{ \Lang::get('front.news-recent',[], App::getLocale()) }}</div>
                      <div class="widget-body widget-content clearfix">

                        @foreach($newsrecents as $newsr)
                        <div class="swin-media">
                          <div class="content-left"><a href="#"><img src="{{ asset('assets/front/images/'.$newsr->image) }}" alt="..." class="media-object" width="100"></a></div>
                          <div class="content-right"><a href="#" class="heading">
                              {{ $newsr->title }}</a>
                            <!-- <div class="info">
                              <div><i class="swin-icon fa fa-clock-o"></i><span class="text">20 minutes ago</span></div>
                            </div> -->
                          </div>
                        </div>
                        @endforeach

                      </div>
                    </div>
                    <!-- tag-->
                    <div class="swin-widget widget-tag">
                      <div class="title-widget">{{ \Lang::get('front.news-tag',[], App::getLocale()) }}</div>
                      <div class="widget-body widget-content clearfix">
                        <ul class="list-unstyled list-inline">

                            @foreach($newstags as $nt)
                          <li><a href="{{ url(preg_replace('#/+#','/', config('app.locale_prefix').'/'.\Lang::get('route.news',[], config('app.locale_prefix')).'/tag/'.$nt->slug)) }}" class="tag">{{ $nt->tag }}</a></li>
                          @endforeach

                        </ul>
                      </div>
                    </div>
