@extends('layouts.front')

@section('page-head-seo')
    <meta name="description" content="{{ $about->meta_description }}">
    <meta name="keywords" content="{{ $about->meta_keyword }}">
    <title>{{ $about->meta_title }} - Hogwartz The Pub Bali</title>
@endsection

@section('conten')

     <div class="page-container">
         
        <!-- Main Content -->          

          <div class="page-content-wrapper">
            <div class="page-content no-padding">

            <!-- Reservation Header -->

              <div data-bottom-top="background-position: 50% 50px;" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% -50px;" class="page-title page-reservation">
                <div class="container">
                  <div class="title-wrapper">
                    <div data-top="transform: translateY(0px);opacity:1;" data--20-top="transform: translateY(-5px);" data--50-top="transform: translateY(-15px);opacity:0.8;" data--120-top="transform: translateY(-30px);opacity:0;" data-anchor-target=".page-title" class="title">{{ $about->title }}</div>
                    <div data-top="opacity:1;" data--120-top="opacity:0;" data-anchor-target=".page-title" class="divider"><span class="line-before"></span><span class="dot"></span><span class="line-after"></span></div>
                  </div>
                </div>
              </div>
            <!-- end Header -->

            <!-- About Us -->

              <section class="ab-timeline-section padding-top-100 padding-bottom-100">
                <div class="container">
                  <div class="swin-sc swin-sc-title style-2">
                    <h3 class="title"><span>Hogwartz The Pub Bali</span></h3>
                  </div>
                  <div data-item="6" class="swin-sc swin-sc-timeline-2">
                    
                    <div class="nav-slider">
                      <div class="slides">
                        <div class="timeline-content-item">
                          <p class="timeline-heading">{{ $restaurantoverview->title }}</p>
                          <div class="timeline-content-detail">
                            <p>{!! $restaurantoverview->description !!}</p>
                          </div>
                        </div>
                        
                      </div>
                    </div>
                  </div>
                </div>
              </section>

            <!-- End About Us -->

            <!-- the Owner -->
            <section data-bottom-top="background-position: 50% 50px;" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% -150px;" class="ab-testimonial-section padding-top-100 padding-bottom-100">
              <div class="container"><img src="{{ asset('assets/front/images/'.$founderprofile->image) }}" alt="" class="img-left img-bg img-deco img-responsive">
                <div class="row">
                  <div class="col-md-8 col-md-offset-4">
                    <div class="swin-sc swin-sc-testimonial style-2 option-2">
                      <div class="main-slider flexslider">
                        <div class="slides">
                          <div class="testi-item item"><i class="testi-icon fa fa-quote-left"></i>
                            <div class="testi-content">
                              <p>{!! $founderprofile->description !!}</p>
                            </div><img src="{{ asset('assets/images/testi/testi-signal.png') }}" alt="" class="testi-signal">
                          </div>
                          
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <!-- end the owner -->                     
              
            <!-- Our Service --> 
              <section class="service-section-02 padding-top-100 padding-bottom-100">
                <div class="container">
                  <div class="swin-sc swin-sc-title">
                    <p class="top-title"><span>Our Service</span></p>
                    <h3 class="title">What We Focus On</h3>
                  </div>
                  <div class="swin-sc swin-sc-iconbox">
                    <div class="row">
                        @php($index = 1)
                        @foreach($services as $service)
                      <div class="col-md-3 col-sm-6 col-xs-12">
                        <div class="item icon-box-02 wow fadeInUpShort">
                          <div class="wrapper-icon"><img src="{{ asset('assets/front/images/'.$service->icon) }}" width="60px"><span class="number">{{ $index }}</span></div>
                          <h4 class="title">{{ $service->title }}</h4>
                          <div class="description">{{ $service->description }}</div>
                        </div>
                      </div>
                        @php($index++)
                      @endforeach

                    </div>
                  </div>
                </div>
              </section>
            <!-- End Our Service -->

          
            </div>
          </div>
        <!-- End Main Content -->
@endsection