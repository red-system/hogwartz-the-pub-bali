@extends('layouts.front')

@section('page-head-seo')
    <meta name="description" content="{{ $reservation->meta_description }}">
    <meta name="keywords" content="{{ $reservation->meta_keyword }}">
    <title>{{ $reservation->meta_title }} - Hogwartz The Pub Bali</title>
@endsection

@section('custom-script')
    <script>
        $(document).ready(function () {
            $('.reservation_date').datetimepicker({
                startDate: new Date(),
                use24hours: true,
                format: 'yyyy-mm-dd hh:i',
                ignoreReadonly: true
            });

            if ($('input[name="party"]').length > 0) {
                var selected = $('input[name="party"]:checked').val()
                if (selected != 'Gathering' && selected != null && selected != '') {
                    $('#nameforparty').removeClass('hide');
                } else {
                    $('#nameforparty').addClass('hide');
                }
            }

            $('input[name="party"]').change(function () {
                var selected = $('input[name="party"]:checked').val()
                if (selected != 'Gathering' && selected != null && selected != '') {
                    $('#nameforparty').removeClass('hide');
                } else {
                    $('#nameforparty').addClass('hide');
                }
            })
        });
    </script>
@endsection

@section('conten')

    <div class="page-container">

        <!-- Main Content -->

        <div class="page-content-wrapper">
            <div class="page-content no-padding">

                <!-- Reservation Header -->

                <div data-bottom-top="background-position: 50% 50px;" data-center="background-position: 50% 0px;"
                     data-top-bottom="background-position: 50% -50px;" class="page-title page-reservation">
                    <div class="container">
                        <div class="title-wrapper">
                            <div data-top="transform: translateY(0px);opacity:1;"
                                 data--20-top="transform: translateY(-5px);"
                                 data--50-top="transform: translateY(-15px);opacity:0.8;"
                                 data--120-top="transform: translateY(-30px);opacity:0;"
                                 data-anchor-target=".page-title"
                                 class="title">{{ \Lang::get('front.confirmation',[], App::getLocale()) }}</div>
                            <div data-top="opacity:1;" data--120-top="opacity:0;" data-anchor-target=".page-title"
                                 class="divider"><span class="line-before"></span><span class="dot"></span><span
                                        class="line-after"></span></div>
                        </div>
                    </div>
                </div>
                <!-- end Header -->

                <section class="section-reservation-form padding-top-100 padding-bottom-100">
                    <div class="container">
                        <div class="section-content">
                            <div class="reservation-form">
                                <div class="swin-sc swin-sc-contact-form light mtl">
                                    {!! Form::open(['url' => preg_replace('#/+#','/',  config('app.locale_prefix').'/confirmation-send'),'method' => 'post', 'class' => 'callus', 'style' => 'margin-top: 0px;', 'files'=>true])!!}
                                    <div class="col-md-12">
                                        @if(Session::has('notification'))
                                            <div class="alert alert-{{ Session::get('notification.level', 'info') }}"
                                                 role="alert">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-hidden="true">&times;
                                                </button>
                                                {{ Session::get('notification.message') }}
                                            </div>
                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group" style="width: 100%;">
                                            <div class="input-group-addon">
                                                <div class="fa fa-user"></div>
                                            </div>
                                            {!! Form::text('name', null, ['placeholder' => \Lang::get('front.rsf-name',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group" style="width: 100%;">
                                            <div class="input-group-addon">
                                                <div class="fa fa-lock"></div>
                                            </div>
                                            {!! Form::text('kode_konfirmasi', null, ['placeholder' => \Lang::get('front.confirmation-code',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                            {!! $errors->first('kode_konfirmasi', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                            {{ Lang::get('front.payment-proof',[], \App::getLocale()) }}
                                            <input type="file" name="proof_payment" accept="image/*">
                                            {!! $errors->first('proof_payment', '<p class="help-block">:message</p>') !!}
                                    </div>

                                    <div class="form-group">
                                        {{-- {!! app('captcha')->render() !!} --}}
                                        <div class="swin-btn-wrap center">

                                            {!! Form::submit(\Lang::get('front.send-confirmation',[], \App::getLocale()), ['class' => 'swin-btn center form-submit']) !!}
                                            <input type="hidden" name="lang" value="{{ App::getLocale() }}">
                                        </div>
                                    </div>

                                    {!! Form::close() !!}
                                </div>
                            </div>
                            <div class="section-deco"><img src="assets/images/pages/reservation-showcase.png"
                                                           alt="fooday" class="img-deco"></div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
        <!-- End Main Content -->
@endsection