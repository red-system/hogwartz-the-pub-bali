@extends('layouts.front')

@section('custom-pluginscript')

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCdXpLSJ3Ibdu-Phs9QOvpqb9d1DtPf7wQ"></script>

@endsection

@section('custom-script')

    <script>
        jQuery(function($) {
    "use strict";
    var fooday = window.fooday || {};
    fooday.map = function(){
        var locations = [
          ['Hogwartz The Pub', -8.724246, 115.175042, 1],
        ];

        if($("#map").length > 0){
            var map = new google.maps.Map(document.getElementById('map'), {
              zoom: 15,
              center: new google.maps.LatLng(-8.724246, 115.175042),
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              styles:[
                        {
                            "featureType": "water",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#e9e9e9"
                                },
                                {
                                    "lightness": 17
                                }
                            ]
                        },
                        {
                            "featureType": "landscape",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#f5f5f5"
                                },
                                {
                                    "lightness": 20
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "lightness": 17
                                }
                            ]
                        },
                        {
                            "featureType": "road.highway",
                            "elementType": "geometry.stroke",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "lightness": 29
                                },
                                {
                                    "weight": 0.2
                                }
                            ]
                        },
                        {
                            "featureType": "road.arterial",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "lightness": 18
                                }
                            ]
                        },
                        {
                            "featureType": "road.local",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "lightness": 16
                                }
                            ]
                        },
                        {
                            "featureType": "poi",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#f5f5f5"
                                },
                                {
                                    "lightness": 21
                                }
                            ]
                        },
                        {
                            "featureType": "poi.park",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#dedede"
                                },
                                {
                                    "lightness": 21
                                }
                            ]
                        },
                        {
                            "elementType": "labels.text.stroke",
                            "stylers": [
                                {
                                    "visibility": "on"
                                },
                                {
                                    "color": "#ffffff"
                                },
                                {
                                    "lightness": 16
                                }
                            ]
                        },
                        {
                            "elementType": "labels.text.fill",
                            "stylers": [
                                {
                                    "saturation": 36
                                },
                                {
                                    "color": "#333333"
                                },
                                {
                                    "lightness": 40
                                }
                            ]
                        },
                        {
                            "elementType": "labels.icon",
                            "stylers": [
                                {
                                    "visibility": "off"
                                }
                            ]
                        },
                        {
                            "featureType": "transit",
                            "elementType": "geometry",
                            "stylers": [
                                {
                                    "color": "#f2f2f2"
                                },
                                {
                                    "lightness": 19
                                }
                            ]
                        },
                        {
                            "featureType": "administrative",
                            "elementType": "geometry.fill",
                            "stylers": [
                                {
                                    "color": "#fefefe"
                                },
                                {
                                    "lightness": 20
                                }
                            ]
                        },
                        {
                            "featureType": "administrative",
                            "elementType": "geometry.stroke",
                            "stylers": [
                                {
                                    "color": "#fefefe"
                                },
                                {
                                    "lightness": 17
                                },
                                {
                                    "weight": 1.2
                                }
                            ]
                        }
                    ]
            });

            var mapOptions = {
                scrollwheel: false,
            };
            map.setOptions(mapOptions);

            var infowindow = new google.maps.InfoWindow();

            var marker, i;

            var domain = "http://" + window.location.hostname;
            console.log(domain);

            for (i = 0; i < locations.length; i++) {
              marker = new google.maps.Marker({
                position: new google.maps.LatLng(locations[i][1], locations[i][2]),
                map: map,
                animation: google.maps.Animation.BOUNCE,
                icon : domain + "/assets/front/images/map.png"
              });

              //marker.addListener('click', toggleBounce);

              google.maps.event.addListener(marker, 'click', (function(marker, i) {
                return function() {
                  infowindow.setContent(locations[i][0]);
                  infowindow.open(map, marker);
                }
              })(marker, i));
            }

        }
    }
 //    function toggleBounce() {
    //   if (marker.getAnimation() !== null) {
    //     marker.setAnimation(null);
    //   } else {
    //     marker.setAnimation(google.maps.Animation.BOUNCE);
    //   }
    // }
    $(document).ready(function(){
        fooday.map();
    });
})
    </script>

@endsection

@section('page-head-seo')
    <meta name="description" content="{{ $contact->meta_description }}">
    <meta name="keywords" content="{{ $contact->meta_keyword }}">
    <title>{{ $contact->meta_title }} - Hogwartz The Pub Bali</title>
@endsection

@section('conten')

<div class="page-container">
         
        <!-- Main Content -->          

          <div class="page-content-wrapper">
            <div class="page-content no-padding">

            <!-- Reservation Header -->

              <div data-bottom-top="background-position: 50% 50px;" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% -50px;" class="page-title page-reservation">
                <div class="container">
                  <div class="title-wrapper">
                    <div data-top="transform: translateY(0px);opacity:1;" data--20-top="transform: translateY(-5px);" data--50-top="transform: translateY(-15px);opacity:0.8;" data--120-top="transform: translateY(-30px);opacity:0;" data-anchor-target=".page-title" class="title">{{ $contact->title }}</div>
                    <div data-top="opacity:1;" data--120-top="opacity:0;" data-anchor-target=".page-title" class="divider"><span class="line-before"></span><span class="dot"></span><span class="line-after"></span></div>
                  </div>
                </div>
              </div>
            <!-- end Header -->

            <!-- Contact Us -->

              <section class="ct-info-section padding-top-100 padding-bottom-100">
              <div class="container">
                <div class="row">
                  <div class="col-md-8 col-sm-12">
                    <div class="swin-sc swin-sc-title style-2 text-left">
                      <p class="title"><span>Get In Touch</span></p>
                    </div>
                    <div class="reservation-form style-02">
                      <div class="swin-sc swin-sc-contact-form light mtl style-full">
                        {!! Form::open(['url' => preg_replace('#/+#','/',  config('app.locale_prefix').'/'.\Lang::get('route.contact',[], \App::getLocale())),'method' => 'post', 'class' => 'callus'])!!}

                                @if(Session::has('notification'))
                                    <div class="alert alert-{{ Session::get('notification.level', 'info') }}" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        {{ Session::get('notification.message') }}
                                    </div>
                                @endif


                                <div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
                                    <div class="input-group">
                                        <div class="input-group-addon"><i class="fa fa-user"></i></div>
                                    {!! Form::text('name', null, ['placeholder' => \Lang::get('front.ctf-name',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>


                                <div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
                                     <div class="input-group">
                                      <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                    {!! Form::email('email', null, ['placeholder' => \Lang::get('front.ctf-email',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                            
                                <div class="form-group {!! $errors->has('phone') ? 'has-error' : '' !!}">
                                    <div class="input-group">
                                      <div class="input-group-addon"><i class="fa fa-phone"></i></div>
                                    {!! Form::text('phone', null, ['placeholder' => \Lang::get('front.ctf-phone',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                            
                                <div class="form-group {!! $errors->has('subject') ? 'has-error' : '' !!}">
                                    <div class="input-group">
                                      <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                    {!! Form::text('subject', null, ['placeholder' => \Lang::get('front.ctf-subject',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                    {!! $errors->first('subject', '<p class="help-block">:message</p>') !!}
                                    </div>
                                </div>
                            
                                
                                <div class="form-group {!! $errors->has('message') ? 'has-error' : '' !!}">
                                    {!! Form::textarea('message', null, ['placeholder' => \Lang::get('front.ctf-message',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                    {!! $errors->first('message', '<p class="help-block">:message</p>') !!}
                                </div>

                                <div class="form-group">
                                    {{-- {!! app('captcha')->render() !!} --}}
                                    <div class="btn-submit button3">
                                        {!! Form::submit(\Lang::get('front.send-mess',[], \App::getLocale())) !!}
                                    </div>
                                </div>

                        {!! Form::close() !!}
                      </div>
                    </div>
                  </div>

                  <div class="col-md-4">
                    <div class="swin-sc swin-sc-title style-2 text-left">
                      <p class="title"><span>Contact Info</span></p>
                    </div>
                    <div class="swin-sc swin-sc-contact">
                      <div class="media item">
                        <div class="media-left">
                          <div class="wrapper-icon"><i class="icons fa fa-map-marker"></i></div>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading title">Restaurant</h4>
                          <div class="description">{{ $web_config->where('config_name', 'headertext_config')->first()->value }}</div>
                        </div>
                      </div>
                      <div class="media item">
                        <div class="media-left">
                          <div class="wrapper-icon"><i class="icons fa fa-phone"></i></div>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading title">Phone Number</h4>
                          <div class="description">{{ $web_config->where('config_name', 'no_tlpconfig')->first()->value }}</div>
                        </div>
                      </div>
                      <div class="media item">
                        <div class="media-left">
                          <div class="wrapper-icon"><i class="icons fa fa-envelope"></i></div>
                        </div>
                        <div class="media-body">
                          <h4 class="media-heading title">Mail</h4>
                          <div class="description">
                            <p>{{ $web_config->where('config_name', 'email')->first()->value }}</p>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </section>
            <section class="map-section padding-bottom-100">
              <div class="container">
                <div id="map">

                </div>
              </div>
            </section>

            <!-- End Contact Us -->

            

          
            </div>
          </div>
        <!-- End Main Content -->
    

    
@endsection