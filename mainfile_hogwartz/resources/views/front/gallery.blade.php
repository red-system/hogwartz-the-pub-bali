@extends('layouts.front')

@section('page-head-seo')
    <meta name="description" content="{{ $gallery->meta_description }}">
    <meta name="keywords" content="{{ $gallery->meta_keyword }}">
    <title>{{ $gallery->meta_title }} - Hogwartz The Pub Bali</title>
@endsection

@section('conten')

     <div class="page-container">
         
        <!-- Main Content -->          

          <div class="page-content-wrapper">
            <div class="page-content no-padding">

            <!-- Reservation Header -->

              <div data-bottom-top="background-position: 50% 50px;" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% -50px;" class="page-title page-reservation">
                <div class="container">
                  <div class="title-wrapper">
                    <div data-top="transform: translateY(0px);opacity:1;" data--20-top="transform: translateY(-5px);" data--50-top="transform: translateY(-15px);opacity:0.8;" data--120-top="transform: translateY(-30px);opacity:0;" data-anchor-target=".page-title" class="title">{{ $gallery->title }}</div>
                    <div data-top="opacity:1;" data--120-top="opacity:0;" data-anchor-target=".page-title" class="divider"><span class="line-before"></span><span class="dot"></span><span class="line-after"></span></div>
                  </div>
                </div>
              </div>
            <!-- end Header -->

            <!-- About Us -->

              <section class="ab-timeline-section padding-top-100 padding-bottom-100">
                <div class="container">
                  @foreach($images as $image)
                  <div class="col-md-3">
                    <a href="{{ asset('assets/front/images/'.$image->image) }}" data-lightbox="image-1" data-title="{{ $image->name }}">
                      <div class="gallery-image" style="background-image: url({{ asset('assets/front/images/'.$image->image) }}); background-size: cover" >
                      </div>
                    </a>
                    <div class="gallery-text">
                      {{ $image->name }}
                    </div>
                  </div>
                  @endforeach
                </div>
              </section>

            <!-- End About Us -->

          
            </div>
          </div>
        <!-- End Main Content -->
@endsection