@extends('layouts.front')

@section('custom-script')
    <script>
        $(document).ready(function () {
            $('.reservation_date').datetimepicker({
                startDate: new Date(),
                use24hours: true,
                format: 'yyyy-mm-dd hh:i',
                ignoreReadonly: true
            });
            if ($('input[name="party"]').length > 0) {
                var selected = $('input[name="party"]:checked').val()
                if (selected != 'Gathering' && selected != null && selected != '') {
                    $('#nameforparty').removeClass('hide');
                } else {
                    $('#nameforparty').addClass('hide');
                }
            }

            $('input[name="party"]').change(function () {
                var selected = $('input[name="party"]:checked').val()
                if (selected != 'Gathering' && selected != null && selected != '') {
                    $('#nameforparty').removeClass('hide');
                } else {
                    $('#nameforparty').addClass('hide');
                }
            })
        });
    </script>
    @if(Session::has('notification'))
        <script>
            $(function () {
                new PNotify({
                    title: '{{ Session::get('notification.title', 'Info') }}',
                    text: '{{ Session::get('notification.message') }}',
                    addclass: 'alert-styled-left',
                    type: '{{ Session::get('notification.level', 'info') }}',
                    delay: 15000
                })
            })
        </script>
    @endif
@endsection

@section('page-head-seo')
    <meta name="description" content="{{ $home->meta_description }}">
    <meta name="keywords" content="{{ $home->meta_keyword }}">
    <title>{{ $home->meta_title }} - Hogwartz The Pub Bali</title>
@endsection

@section('conten')
    <div class="page-container">
        <div class="top-header top-slider">
            @foreach($homesliders as $hs)
                <div class="slides">
                    <div class="slide-content slide-layout-01">
                        <div class="swin-sc swin-sc-title"><img
                                    src="{{ asset('assets/front/images/slider/slider1-icon.png') }}"
                                    data-ani-in="fadeInUp" data-ani-out="fadeOutDown" data-ani-delay="500" alt=""
                                    class="slide-icon animated fadeInUp">
                            <p data-ani-in="fadeInUp" data-ani-out="fadeOutDown" data-ani-delay="800">
                                <span class="white-text">{{ $hs->name }}</span>
                            </p>
                            <h3 data-ani-in="fadeInUp" data-ani-out="fadeOutDown" data-ani-delay="1000">
                                <span class="white-text">{{ $hs->slider_text }}</span>
                            </h3>
                        </div>
                        <div data-ani-in="fadeInUp" data-ani-out="fadeOutDown" data-ani-delay="1200"
                             class="btn-wrap text-center animated fadeInUp">
                            <a href="{{ url(preg_replace('#/+#','/', config('app.locale_prefix').'/'.$link_about)) }}" class="btn swin-btn">
                                <span>{{ $hs->link_title }}</span>
                            </a>
                        </div>
                    </div>
                    <div class="slide-bg"><img src="{{ asset('assets/front/images/'.$hs->image) }}" alt=""
                                               class="img img-responsive"></div>
                </div>
            @endforeach
        </div>

        <div class="page-content-wrapper">
            <div class="page-content no-padding">

                <section class="about-us-session padding-top-100 padding-bottom-100">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-6 colsm-12"><img
                                        src="{{ asset('assets/front/images/pages/home1-about.jpg') }}" alt=""
                                        class="img img-responsive wow zoomIn"></div>
                            <div class="col-md-6 col-sm-12">
                                <div class="swin-sc swin-sc-title style-4 margin-bottom-20 margin-top-50">
                                    <p class="top-title"><span>Discover</span></p>
                                    <h3 class="title">{{ $discover->title }}</h3>
                                </div>
                                <p class="des margin-bottom-20 text-center">
                                    {!! $discover->conten !!}
                                </p>
                                <div class="swin-btn-wrap center"><a href="{{ App::getLocale().'/'.$discover->link }}"
                                                                     class="swin-btn center form-submit btn-transparent">
                                        <span> About Us</span></a></div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="reservation-section-02 padding-top-100 padding-bottom-100">
                    <div class="container"><img src="{{ asset('assets/front/images/background/home2-img-deco-1.png') }}"
                                                alt="" class="img-deco img-responsive">
                        <div class="row">
                            <div class="col-md-6 left-wrapper">
                                <div class="form-dark-wrapper">
                                    <div class="swin-sc swin-sc-title style-3 light" id="order-form">
                                        <p class="title">
                                            <span>{{ \Lang::get('front.reservation',[], App::getLocale()) }}</span></p>
                                    </div>
                                    <div class="swin-sc swin-sc-contact-form dark mtl">


                                        {!! Form::open(['url' => preg_replace('#/+#','/',  config('app.locale_prefix').'/online-order-form'),'method' => 'post', 'class' => 'callus', 'style' => 'margin-top: 0px;'])!!}

                                        <div class="col-md-12">
                                            @if(Session::has('notification'))
                                                <div class="alert alert-{{ Session::get('notification.altlevel', 'info') }}"
                                                     role="alert">
                                                    <button type="button" class="close" data-dismiss="alert"
                                                            aria-hidden="true">&times;
                                                    </button>
                                                    {{ Session::get('notification.message') }}
                                                </div>
                                            @endif
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group" style="width: 100%;">
                                                <div class="input-group-addon">
                                                    <div class="fa fa-user"></div>
                                                </div>
                                                {!! Form::text('name', null, ['placeholder' => \Lang::get('front.rsf-name',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                                {!! Form::email('email', null, ['placeholder' => \Lang::get('front.rsf-email',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                            </div>

                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-users"></i></div>
                                                {!! Form::text('pax', null, ['placeholder' => 'Pax', 'class' => 'form-control']) !!}
                                                {!! $errors->first('pax', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="input-group">
                                                <div class="input-group-addon">
                                                    <div class="fa fa-phone"></div>
                                                </div>
                                                {!! Form::text('phone', null, ['placeholder' => \Lang::get('front.rsf-phone',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                                {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                                            </div>

                                            <div class="input-group">
                                                <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                                {!! Form::text('reservation_datetime', null, ['placeholder' => \Lang::get('front.rsf-date',[], \App::getLocale()), 'class' => 'form-control reservation_date', 'readonly' => true, 'id' => 'reservation_date']) !!}
                                                {!! $errors->first('reservation_datetime', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            {!! Form::textarea('active_socmed', null, ['placeholder' => \Lang::get('front.rsf-contactsosmed',[], \App::getLocale()), 'class' => 'form-control', 'rows' => 2]) !!}
                                            {!! $errors->first('active_socmed', '<p class="help-block">:message</p>') !!}
                                        </div>

                                        <div class="form-group">
                                            <h5>Party Type</h5>
                                            {{ Form::radio('party', 'Birthday') }} Birthday Party<br>
                                            {{ Form::radio('party', 'Wedding Anniversary') }} Wedding Anniversary<br>
                                            {{ Form::radio('party', 'Gathering') }} Gathering
                                        </div>

                                        <div id="nameforparty" class="form-group hide">
                                            <div class="input-group" style="width: 100%;">
                                                <div class="input-group-addon">
                                                    <div class="fa fa-pencil"></div>
                                                </div>
                                                {!! Form::text('name_for_party', null, ['placeholder' => \Lang::get('front.rsf-name-for-party',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                                {!! $errors->first('name_for_party', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <h5>Cake</h5>
                                            {{ Form::radio('cake', 'Without Cake') }} Without Cake<br>
                                            {{ Form::radio('cake', 'Small Cake (Rp. 350.000/35$ AU$)') }} Small Cake
                                            (Rp. 350.000/35$ AU$)<br>
                                            {{ Form::radio('cake', 'Large Cake (Rp. 400.000/45$ AU$)') }} Large Cake
                                            (Rp. 400.000/45$ AU$)<br>
                                        </div>

                                        <div class="form-group">
                                            {!! Form::textarea('remark', null, ['placeholder' => \Lang::get('front.rsf-remark',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                            {!! $errors->first('remark', '<p class="help-block">:message</p>') !!}
                                        </div>

                                        <div class="form-group">
                                            {{-- {!! app('captcha')->render() !!} --}}
                                            <div class="swin-btn-wrap center">
                                                {!! Form::submit(\Lang::get('front.send-reservation',[], \App::getLocale()), ['class' => 'swin-btn center form-submit']) !!}
                                            </div>
                                        </div>
                                        {!! Form::close() !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="video-wrapper equal-height deco-abs">
                        <div class="swin-sc swin-sc-video">
                            <div class="play-wrap">
                                <a href="{!! $videoprofile->link !!}" class="play-btn swipebox">
                                    <i class="play-icon fa fa-play"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="featured-section padding-top-100 padding-bottom-100">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="swin-sc swin-sc-title">
                                    <p class="top-title"><span>Our Special</span></p>
                                    <h3 class="title">Amazing Featured</h3>
                                </div>
                                <div class="row">

                                    @foreach($specialties as $sp)
                                        <div class="col-md-4 col-sm-6 col-xs-12">
                                            <div class="swin-sc sc-featured-box item wow fadeInUp"><img
                                                        src="{{ asset('assets/front/images/'.$sp->icon) }}" alt="fooday"
                                                        class="box-bg">
                                                <div class="box-inner">
                                                    <h4 class="box-title">{!! $sp->title !!}</h4>
                                                    <div class="box-content">{!! $sp->description !!}</div>
                                                    <div class="btn-wrap text-center"><a href="javascript:void(0)"
                                                                                         class="btn swin-btn"><span>Read More</span></a>
                                                    </div>
                                                    <div class="showcase"><img
                                                                src="{{ asset('assets/front/images/'.$sp->icon) }}"
                                                                alt="" class="img-responsive img-showcase">
                                                        <div class="title-showcase">{!! $sp->title !!}</div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach

                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="product-sesction-03-1 padding-top-100 padding-bottom-100"><img
                            src="{{ asset('assets/front/images/'.$dsimage->image) }}" alt=""
                            class="img-responsive img-decorate">
                    <div class="container">
                        <div class="row">
                            <div class="col-lg-6 col-md-4"></div>
                            <div class="col-lg-6 col-md-8">
                                <div class="swin-sc swin-sc-title text-left light">
                                    <p class="top-title"><span>chef choice</span></p>
                                    <h3 class="title">Daily Special</h3>
                                </div>
                                <div class="swin-sc swin-sc-product products-01 style-04 light swin-vetical-slider">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div data-height="200" class="products nav-slider">

                                                @foreach($recmenus as $recmn)
                                                    <div class="item product-01">
                                                        <div class="item-left"><img
                                                                    src="{{ asset('assets/front/images/'.$recmn->image) }}"
                                                                    alt="{{ $recmn->name }}" class="img img-responsive">
                                                            <div class="content-wrapper"><a
                                                                        class="title">{{ $recmn->name }}</a>
                                                                <div class="dot">
                                                                    .....................................................................
                                                                </div>
                                                                <div class="des">{!! $recmn->description !!} </div>
                                                            </div>
                                                        </div>
                                                        <div class="item-right"><span
                                                                    class="price woocommerce-Price-amount amount"><span
                                                                        class="price-symbol"></span>{{ number_format($recmn->price) }}
                                                                K</span></div>
                                                    </div>
                                                @endforeach

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="service-section-02 padding-top-100 padding-bottom-100">
                    <div class="container">
                        <div class="swin-sc swin-sc-title">
                            <p class="top-title"><span>Our Service</span></p>
                            <h3 class="title">What We Focus On</h3>
                        </div>
                        <div class="swin-sc swin-sc-iconbox">
                            <div class="row">

                                @php( $no = 1)
                                @foreach($services as $service)
                                    <div class="col-md-3 col-sm-6 col-xs-12">
                                        <div class="item icon-box-02 wow fadeInUpShort">
                                            <div class="wrapper-icon"><img
                                                        src="{{ asset('assets/front/images/'.$service->icon) }}"
                                                        width="60px"><span class="number">{{ $no }}</span></div>
                                            <h4 class="title">{{ $service->title }}</h4>
                                            <div class="description">{{ $service->description }}</div>
                                        </div>
                                    </div>
                                    @php($no++)
                                @endforeach
                            </div>
                        </div>
                    </div>
                </section>
                <section class="blog-section-02 pbn padding-top-100 padding-bottom-100"><img
                            src="{{ asset('assets/front/images/background/soup1.png') }}" alt=""
                            class="img-left img-bg img-responsive img-decorate"><img
                            src="{{ asset('assets/front/images/background/soup2.png') }}" alt=""
                            class="img-right img-bg img-responsive img-decorate">
                    <div class="container">
                        <div class="swin-sc swin-sc-title">
                            <p class="top-title"><span>Update From</span></p>
                            <h3 class="title white-color">{{ \Lang::get('front.recent-news',[], App::getLocale()) }}</h3>
                        </div>
                        <div class="swin-sc swin-sc-blog-slider style-01 has-slider">
                            <div class="main-slider">
                                <div class="slides">
                                    @foreach($recentnews as $rnews)
                                        <div class="blog-item swin-transition">
                                            <div class="blog-featured-img"><img
                                                        src="{{ asset('assets/front/images/'.$rnews->image) }}"
                                                        alt="fooday" class="img img-responsive"></div>
                                            <div class="blog-content">
                                                <div class="blog-date"><span
                                                            class="day">{{ date('d', strtotime($rnews->created_at)) }}</span><span
                                                            class="month">{{ date('M', strtotime($rnews->created_at)) }}</span>
                                                </div>
                                                <h3 class="blog-title"><a
                                                            href="{{ url(preg_replace('#/+#','/', config('app.locale_prefix').'/'.\Lang::get('route.news',[], config('app.locale_prefix')).'/'.$rnews->slug)) }}"
                                                            class="swin-transition">{{ $rnews->title }}</a></h3>
                                                <p class="blog-description">{!! $rnews->description  !!} </p>
                                                <div class="blog-readmore"><a
                                                            href="{{ url(preg_replace('#/+#','/', config('app.locale_prefix').'/'.\Lang::get('route.news',[], config('app.locale_prefix')).'/'.$rnews->slug)) }}"
                                                            class="swin-transition">Read More <i
                                                                class="fa fa-angle-double-right"></i></a></div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="testimonial-section-02 padding-top-100 padding-bottom-100"><img
                            src="{{ asset('assets/front/images/background/pizza1.png') }}" alt=""
                            class="img-left img-bg img-responsive img-decorate"><img
                            src="{{ asset('assets/front/images/background/food1.png') }}" alt=""
                            class="img-right img-bg img-responsive img-decorate">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-9 col-md-offset-1">
                                <div class="swin-sc swin-sc-title">
                                    <p class="top-title"><span>Testimonial</span></p>
                                    <h3 class="title">Our Customer Says</h3>
                                </div>
                                <div class="row">
                                    <div class="col-md-10 col-md-offset-1">
                                        <div class="swin-sc swin-sc-testimonial style-1 bg-white">
                                            <div class="main-slider flexslider">
                                                <div class="slides">

                                                    @foreach($testimonials as $testimonial)
                                                        <div class="testi-item item"><i
                                                                    class="testi-icon fa fa-quote-left"></i>
                                                            <div class="testi-content">
                                                                <p>{{ $testimonial->testimonial }}</p>
                                                            </div>
                                                            <div class="testi-info"><span
                                                                        class="name">{{ $testimonial->name }}</span>
                                                                <span class="position">{{ $testimonial->job }}</span>
                                                            </div>
                                                        </div>
                                                    @endforeach


                                                </div>
                                            </div>
                                            <div data-width="150" class="nav-slider">
                                                <ul class="slides list-inline">

                                                    @foreach($testimonials as $testimonial)
                                                        <li class="swin-transition"><a href="javascript:void(0)"
                                                                                       class="testimonial-nav-item"><img
                                                                        src="{{ asset('assets/front/images/'.$testimonial->picture) }}"
                                                                        alt="fooday"
                                                                        class="img img-responsive swin-transition"></a>
                                                        </li>
                                                    @endforeach

                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection