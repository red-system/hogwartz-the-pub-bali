@extends('layouts.front')

@section('page-head-seo')
    <meta name="description" content="{{ $articlemenu->meta_description }}">
    <meta name="keywords" content="{{ $articlemenu->meta_keyword }}">
    <title>{{ $articlemenu->meta_title }} - Hogwartz The Pub Bali</title>
@endsection

@section('conten')

    <div class="page-container">

        <!-- Main Content -->

        <div class="page-content-wrapper">
            <div class="page-content no-padding">

                <!-- Page Title -->
                <div data-bottom-top="background-position: 50% 50px;" data-center="background-position: 50% 0px;"
                     data-top-bottom="background-position: 50% -50px;" class="page-title page-menu">
                    <div class="container">
                        <div class="title-wrapper">
                            <div data-top="transform: translateY(0px);opacity:1;"
                                 data--20-top="transform: translateY(-5px);"
                                 data--50-top="transform: translateY(-15px);opacity:0.8;"
                                 data--120-top="transform: translateY(-30px);opacity:0;"
                                 data-anchor-target=".page-title"
                                 class="title">{{ \Lang::get('front.our-menu',[], \App::getLocale()) }}</div>
                            <div data-top="opacity:1;" data--120-top="opacity:0;" data-anchor-target=".page-title"
                                 class="divider"><span class="line-before"></span><span class="dot"></span><span
                                        class="line-after"></span></div>
                        </div>
                    </div>
                </div>
                <!-- End Page title -->

                <!-- Food Menu -->

                <section class="product-sesction-02 padding-top-100 padding-bottom-100">
                    <div class="container">
                        <div class="swin-sc swin-sc-title style-3">
                            <p class="title"><span>{{ \Lang::get('front.our-menu',[], \App::getLocale()) }}</span></p>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 text-center center">
                                @if($articlemenu->thumb_image)
                                    <center>
                                        <img src="{{ asset('assets/front/images/'.$articlemenu->thumb_image) }}" class="img-responsive">
                                    </center>
                                @endif
                            </div>
                        </div>
                    </div>
                </section>

                <!-- End Food Menu -->

                <!-- Span Reservation -->
                <section class="menu-banner-section banner-section padding-top-100 padding-bottom-100">
                    {{--<img--}}
                            {{--src="{{ asset('assets/images/background/lemon.png') }}" alt=""--}}
                            {{--class="img-left img-bg img-deco img-responsive">--}}
                    <img
                            src="{{ asset('assets/images/background/vegetable_03.png') }}" alt=""
                            class="img-right img-bg img-deco img-responsive">
                    <div class="container">
                        <div class="row">
                            <div class="col-md-8 col-md-offset-2">
                                <div class="content-wrapper">
                                    <h2 class="heading-title">Let's Make Your Meal be Fantastic With<span
                                                class="text-large"> Our</span>Awesome Menu!</h2>
                                    <div class="swin-btn-wrap"><a href="#" class="swin-btn"><span>Book Table</span></a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- End SPan Reservation -->




            </div>
        </div>
        <!-- End Main Content -->


@endsection