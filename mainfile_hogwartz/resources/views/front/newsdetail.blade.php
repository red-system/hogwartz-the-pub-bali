@extends('layouts.front')

@section('customcss')
    <style type="text/css">
        #share-buttons img {
            width: 35px;
            padding: 5px;
            border: 0;
            box-shadow: 0;
            display: inline;
        }
    </style>
@endsection

@section('custom-pluginscript')
    <script src="{{ asset('assets/front/js/jquery.parallax-1.1.3.js') }}"></script>
    <script src="{{ asset('assets/front/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('assets/front/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ asset('assets/front/js/slider.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/front/js/jquery.mixitup.min.js') }}"></script>
@endsection

@section('custom-script')

@endsection

@section('page-head-seo')
    <meta name="description" content="{{ $news->meta_description }}">
    <meta name="keywords" content="{{ $news->meta_keyword }}">
    <title>{{ $news->meta_title }} - Hogwartz The Pub Bali</title>
@endsection

@section('conten')

<div class="page-container">
          <div data-bottom-top="background-position: 50% 50px;" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% -50px;" class="page-title page-blog">
            <div class="container">
              <div class="title-wrapper">
                <div data-top="transform: translateY(0px);opacity:1;" data--120-top="transform: translateY(-30px);opacity:0;" data-anchor-target=".page-title" class="title">{{ $news->title }}</div>
                <div data-top="opacity:1;" data--120-top="opacity:0;" data-anchor-target=".page-title" class="divider"><span class="line-before"></span><span class="dot"></span><span class="line-after"></span></div>
              </div>
            </div>
          </div>
          <div class="page-content-wrapper">
            <div class="container">
              <div class="padding-top-100 padding-bottom-100">
                <div class="row">
                  <div class="page-main col-md-8">
                    <div class="blog-wrapper swin-blog-single">
                      <div class="swin-sc-blog-slider style-02">
                        <div class="main-slider">
                          <div class="slides">
                            <div class="blog-item">
                              <div class="blog-featured-img"><img src="{{ asset('assets/front/images/'.$news->image) }}" alt="fooday" class="img img-responsive"></div>
                              <div class="blog-content">
                                <div class="blog-meta-info">
                                  <div class="blog-date"><span class="day">{{ date('d', strtotime($news->created_at)) }}</span><span class="month">{{ date('M', strtotime($news->created_at)) }}</span></div>
                                  <div class="blog-info clearfix">
                                    <!-- <div class="blog-info-item blog-view">
                                      <p><i class="fa fa-eye"></i><span>18</span></p>
                                      <p></p>
                                    </div>
                                    <div class="blog-info-item blog-comment">
                                      <p><i class="fa fa-comment"></i><span>18</span></p>
                                      <p></p>
                                    </div>
                                    <div class="blog-info-item blog-author">
                                      <p><span>Post By </span><a href="#">Admin</a></p>
                                      <p></p>
                                    </div> -->
                                  </div>
                                  <h3 class="blog-title"><a href="#" class="swin-transition">{{ $news->title }}</a></h3>
                                </div>
                                <div class="blog-content-inner">
                                  
                                  {!! $news->description !!}
                                  
                                </div>
                                <div class="blog-footer clearfix">
                                  <div class="blog-share">
                                    <ul class="socials list-unstyled list-inline">
                                      <li class="black"><a href="https://www.facebook.com/sharer.php?u={{ url(preg_replace('#/+#','/', config('app.locale_prefix').'/'.\Lang::get('route.news',[], config('app.locale_prefix')).'/'.$news->slug)) }}" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                <li class="black"><a href="https://twitter.com/share?url={{ url(preg_replace('#/+#','/', config('app.locale_prefix').'/'.\Lang::get('route.news',[], config('app.locale_prefix')).'/'.$news->slug)) }}&amp;text={{ $news->title.' - Bebek Bengil Restaurant' }}" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                <li class="black"><a href="https://plus.google.com/share?url={{ url(preg_replace('#/+#','/', config('app.locale_prefix').'/'.\Lang::get('route.news',[], config('app.locale_prefix')).'/'.$news->slug)) }}" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                <li class="black"><a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                                    </ul>
                                  </div>
                                  <div class="blog-tags"><strong>Tags:</strong>
                                        @foreach($tags as $tag)
                                            @if($tags->last() == $tag)
                                                {{ $tag->tag}}
                                            @else
                                                {{ $tag->tag.', ' }}
                                            @endif
                                        @endforeach
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>                      
                    </div>
                  </div>
                  <div class="page-sidebar col-md-4">
                        
                    @include('front._sidebarnews')
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection