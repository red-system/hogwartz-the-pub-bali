@extends('layouts.front')

@section('page-head-seo')
    <meta name="description" content="{{ $articleoutlet->meta_description }}">
    <meta name="keywords" content="{{ $articleoutlet->meta_keyword }}">
    <title>{{ $articleoutlet->meta_title }} - Hogwartz The Pub Bali</title>
@endsection

@section('conten')
    <!--Page header & Title-->
    <section id="page_header">
        <div class="page_title">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="title">{{ $articleoutlet->title }}</h2>
                    </div>

                </div>
            </div>
        </div>
    </section>

    <section class="padding" id="popular">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="heading">{{ \Lang::get('front.list-outlet',[], App::getLocale()) }}</h2>
                    <hr>
                </div>
            </div>

            <?php $no = 1; ?>
            @foreach($alloutlet as $outlet)
                @if($no == 1 || ($no%3 == 1))<div class="row">@endif
                <div class="col-sm-4">
                    <div class="popular top40 text-center">
                        <div class="image">
                            <img src="{{ asset('assets/front/images/'.$outlet->image) }}" alt="{{ $outlet->title }}">
                            <div class="overlay">
                                <a class="fancybox overlay-inner" href="{{ asset('assets/front/images/'.$outlet->image) }}" data-fancybox-group="gallery">
                                    <i class="icon-eye6"></i>
                                </a>
                            </div>
                        </div>
                        <div class="content">
                            <a href="{{ url(preg_replace('#/+#','/', config('app.locale_prefix').'/'.\Lang::get('route.outlet',[], App::getLocale()).'/'.$outlet->slug)) }}"><h4>{{ $outlet->title }}</h4></a>
                            <p>{{ substr(strip_tags($outlet->description),0,250).'...' }}</p>
                        </div>
                    </div>
                </div>
                @if(($no%3 == 0) || $alloutlet->last() == $outlet)</div>@endif
                <?php $no++; ?>
            @endforeach
        </div>
    </section>
@endsection