@extends('layouts.front')

@section('page-head-seo')
    <meta name="description" content="{{ $articlepackage->meta_description }}">
    <meta name="keywords" content="{{ $articlepackage->meta_keyword }}">
    <title>{{ $articlepackage->meta_title }} - Hogwartz The Pub Bali</title>
@endsection

@section('conten')
    <!--Page header & Title-->
    <section id="page_header">
        <div class="page_title">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="title">{{ $articlepackage->title }}</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="padding" id="popular">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="heading">{{ $articlepackage->title }}</h2>
                    <hr>
                </div>
            </div>

            <?php $no = 1; ?>
            @foreach($allpackages as $package)
                @if($no == 1 || ($no%3 == 1))<div class="row">@endif
                    <div class="col-sm-4">
                        <div class="popular top40 text-center">
                            <div class="image">
                                <img src="{{ asset('assets/front/images/'.$package->image) }}" alt="{{ $package->title }}">
                                <a class="fancybox overlay-inner" href="{{ asset('assets/front/images/'.$package->image) }}" data-fancybox-group="gallery">
                                    <div class="overlay">
                                        <i class="icon-eye6"></i>
                                    </div>
                                </a>
                            </div>
                            <div class="content">
                                <h4>{{ $package->title }}</h4>
                                <p>{!! $package->description !!}</p>
                            </div>
                        </div>
                    </div>
                @if(($no%3 == 0) || $allpackages->last() == $package)</div>@endif
                <?php $no++; ?>
            @endforeach

            <div class="alert alert-info" style="margin-top: 40px;">
                <div class="row">
                    <div class="col-md-10">{!! $taxinfo->value !!}</div>
                    <div class="col-md-2"><a href="{{ url(preg_replace('#/+#','/', config('app.locale_prefix').'/'.\Lang::get('route.contact',[], App::getLocale()))) }}" class="btn-common btn-block" style="border-radius: 0px; text-align: center; border: none; padding: 12px; margin: 0px;">Order Now</a></div>
                </div>
            </div>
        </div>
    </section>
@endsection