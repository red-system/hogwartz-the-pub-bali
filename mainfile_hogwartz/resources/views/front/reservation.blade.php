@extends('layouts.front')

@section('page-head-seo')
    <meta name="description" content="{{ $reservation->meta_description }}">
    <meta name="keywords" content="{{ $reservation->meta_keyword }}">
    <title>{{ $reservation->meta_title }} - Hogwartz The Pub Bali</title>
@endsection

@section('custom-script')
    <script>
        $(document).ready(function () {
            $('.reservation_date').datetimepicker({
                startDate: new Date(),
                use24hours: true,
                format: 'yyyy-mm-dd hh:i',
                ignoreReadonly: true
            });

            if ($('input[name="party"]').length > 0) {
                var selected = $('input[name="party"]:checked').val()
                if (selected != 'Gathering' && selected != null && selected != '') {
                    $('#nameforparty').removeClass('hide');
                } else {
                    $('#nameforparty').addClass('hide');
                }
            }

            $('input[name="party"]').change(function () {
                var selected = $('input[name="party"]:checked').val()
                if (selected != 'Gathering' && selected != null && selected != '') {
                    $('#nameforparty').removeClass('hide');
                } else {
                    $('#nameforparty').addClass('hide');
                }
            })
        });
    </script>
@endsection

@section('conten')

    <div class="page-container">

        <!-- Main Content -->

        <div class="page-content-wrapper">
            <div class="page-content no-padding">

                <!-- Reservation Header -->

                <div data-bottom-top="background-position: 50% 50px;" data-center="background-position: 50% 0px;"
                     data-top-bottom="background-position: 50% -50px;" class="page-title page-reservation">
                    <div class="container">
                        <div class="title-wrapper">
                            <div data-top="transform: translateY(0px);opacity:1;"
                                 data--20-top="transform: translateY(-5px);"
                                 data--50-top="transform: translateY(-15px);opacity:0.8;"
                                 data--120-top="transform: translateY(-30px);opacity:0;"
                                 data-anchor-target=".page-title"
                                 class="title">{{ \Lang::get('front.reservation',[], App::getLocale()) }}</div>
                            <div data-top="opacity:1;" data--120-top="opacity:0;" data-anchor-target=".page-title"
                                 class="divider"><span class="line-before"></span><span class="dot"></span><span
                                        class="line-after"></span></div>
                        </div>
                    </div>
                </div>
                <!-- end Header -->

                <section class="section-reservation-form padding-top-100 padding-bottom-100">
                    <div class="container">
                        <div class="section-content">
                            <div class="reservation-form">
                                <div class="swin-sc swin-sc-contact-form light mtl">
                                    {!! Form::open(['url' => preg_replace('#/+#','/',  config('app.locale_prefix').'/online-order-form'),'method' => 'post', 'class' => 'callus', 'style' => 'margin-top: 0px;'])!!}
                                    <div class="col-md-12">
                                        @if(Session::has('notification'))
                                            <div class="alert alert-{{ Session::get('notification.level', 'info') }}"
                                                 role="alert">
                                                <button type="button" class="close" data-dismiss="alert"
                                                        aria-hidden="true">&times;
                                                </button>
                                                {{ Session::get('notification.message') }}
                                            </div>



                                            <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
                                            <script type="text/javascript">
                                                $(document).ready(function() {
                                                    Swal.fire({
                                                        type: 'success',
                                                        title: 'Reservation Succeed',
                                                    })
                                                });
                                            </script>

                                        @endif
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group" style="width: 100%;">
                                            <div class="input-group-addon">
                                                <div class="fa fa-user"></div>
                                            </div>
                                            {!! Form::text('name', null, ['placeholder' => \Lang::get('front.rsf-name',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                            {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-envelope"></i></div>
                                            {!! Form::email('email', null, ['placeholder' => \Lang::get('front.rsf-email',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                            {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                        </div>

                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-users"></i></div>
                                            {!! Form::text('pax', null, ['placeholder' => 'Pax', 'class' => 'form-control']) !!}
                                            {!! $errors->first('pax', '<p class="help-block">:message</p>') !!}
                                        </div>

                                    </div>


                                    <div class="form-group">
                                        <div class="input-group">
                                            <div class="input-group-addon">
                                                <div class="fa fa-phone"></div>
                                            </div>
                                            {!! Form::text('phone', null, ['placeholder' => \Lang::get('front.rsf-phone',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                            {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                                        </div>

                                        <div class="input-group">
                                            <div class="input-group-addon"><i class="fa fa-calendar"></i></div>
                                            {!! Form::text('reservation_datetime', null, ['placeholder' => \Lang::get('front.rsf-date',[], \App::getLocale()), 'class' => 'form-control reservation_date', 'readonly' => true, 'id' => 'reservation_date']) !!}
                                            {!! $errors->first('reservation_datetime', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::textarea('active_socmed', null, ['placeholder' => \Lang::get('front.rsf-contactsosmed',[], \App::getLocale()), 'class' => 'form-control', 'rows' => 2]) !!}
                                        {!! $errors->first('active_socmed', '<p class="help-block">:message</p>') !!}
                                    </div>

                                    <div class="form-group">
                                        <h5>Party Type</h5>
                                        {{ Form::radio('party', 'Birthday') }} Birthday Party<br>
                                        {{ Form::radio('party', 'Wedding Anniversary') }} Wedding Anniversary<br>
                                        {{ Form::radio('party', 'Gathering') }} Gathering
                                    </div>

                                    <div id="nameforparty" class="form-group hide">
                                        <div class="input-group" style="width: 100%;">
                                            <div class="input-group-addon">
                                                <div class="fa fa-pencil"></div>
                                            </div>
                                            {!! Form::text('name_for_party', null, ['placeholder' => \Lang::get('front.rsf-name-for-party',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                            {!! $errors->first('name_for_party', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <h5>Cake</h5>
                                        {{ Form::radio('cake', 'Without Cake') }} Without Cake<br>
                                        {{ Form::radio('cake', 'Small Cake (Rp. 350.000/35$ AU$)') }} Small Cake (Rp.
                                        350.000/35$ AU$)<br>
                                        {{ Form::radio('cake', 'Large Cake (Rp. 400.000/45$ AU$)') }} Large Cake (Rp.
                                        400.000/45$ AU$)<br>
                                    </div>

                                    <div class="form-group">
                                        {!! Form::textarea('remark', null, ['placeholder' => \Lang::get('front.rsf-remark',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                        {!! $errors->first('remark', '<p class="help-block">:message</p>') !!}
                                    </div>

                                    <div class="form-group">
                                        {{-- {!! app('captcha')->render() !!} --}}
                                        <div class="swin-btn-wrap center">

                                            {{--<button type="button" class="swin-btn center" data-toggle="modal" data-target="#modal-reservation">
                                                Confirm then Submit Order
                                            </button>--}}
                                            {!! Form::submit(\Lang::get('front.send-reservation',[], \App::getLocale()), ['class' => 'swin-btn center form-submit']) !!}
                                            <input type="hidden" name="lang" value="{{ App::getLocale() }}">
                                        </div>
                                    </div>

                                    <!-- Modal -->
                                    <div class="modal" id="modal-reservation" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">
                                                        Term and Condition
                                                    </h4>
                                                </div>
                                                <div class="modal-body">
                                                    {!! $term->conten !!}
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                                                    {!! Form::submit(\Lang::get('front.send-reservation',[], \App::getLocale()), ['class' => 'swin-btn center form-submit']) !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                    {!! Form::close() !!}
                                </div>
                            </div>
                            <div class="section-deco"><img src="assets/images/pages/reservation-showcase.png"
                                                           alt="fooday" class="img-deco"></div>
                        </div>
                    </div>
                </section>

            </div>
        </div>
        <!-- End Main Content -->
@endsection