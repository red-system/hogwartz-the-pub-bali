@extends('layouts.front')

@section('custom-pluginscript')
    <script src="{{ asset('assets/front/js/jquery.parallax-1.1.3.js') }}"></script>
    <script src="{{ asset('assets/front/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('assets/front/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ asset('assets/front/js/slider.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/front/js/jquery.mixitup.min.js') }}"></script>
@endsection

@section('page-head-seo')
    <meta name="description" content="{{ $showfeed->meta_description }}">
    <meta name="keywords" content="{{ $showfeed->meta_keyword }}">
    <title>{{ $showfeed->meta_title }} - Hogwartz The Pub Bali</title>
@endsection

@section('conten')
    <div class="page-container">

        <!-- Main Content -->

        <div class="page-content-wrapper">
            <div class="page-content no-padding">

                <div data-bottom-top="background-position: 50% 50px;" data-center="background-position: 50% 0px;"
                     data-top-bottom="background-position: 50% -50px;" class="page-title page-blog">
                    <div class="container">
                        <div class="title-wrapper">
                            <div data-top="transform: translateY(0px);opacity:1;"
                                 data--120-top="transform: translateY(-30px);opacity:0;"
                                 data-anchor-target=".page-title" class="title">{{ $showfeed->title }}</div>
                            <div data-top="opacity:1;" data--120-top="opacity:0;" data-anchor-target=".page-title"
                                 class="divider"><span class="line-before"></span><span class="dot"></span><span
                                        class="line-after"></span></div>
                        </div>
                    </div>
                </div>

                <!-- Blog Content -->

                <section class="padding-top-100 padding-bottom-100">
                    <div class="container">
                        <div class="row">
                            <div class="page-main col-md-8">
                                <div class="blog-wrapper swin-sc-blog-list">
                                    @foreach($allshow as $news)
                                        <div class="swin-sc-blog-slider style-02">
                                            <div class="main-slider">
                                                <div class="slides">
                                                    <div class="blog-item swin-transition">
                                                        <div class="blog-featured-img"><img
                                                                    src="{{ asset('assets/front/images/'.$news->image) }}"
                                                                    alt="fooday" class="img img-responsive"></div>
                                                        <div class="blog-content">
                                                            <div class="blog-date"><p></p>
                                                                <span class="month">{{ $news->schedule }}</span>
                                                            </div>
                                                            <h3 class="blog-title"><a
                                                                        href="{{ url(preg_replace('#/+#','/', config('app.locale_prefix').'/'.\Lang::get('route.schedule',[], config('app.locale_prefix')).'/'.$news->slug)) }}"
                                                                        class="swin-transition">{{ $news->title }}</a>
                                                            </h3>
                                                            <p class="blog-description">{!! $news->content  !!} </p>
                                                            <div class="blog-readmore"><a
                                                                        href="{{ url(preg_replace('#/+#','/', config('app.locale_prefix').'/'.\Lang::get('route.schedule',[], config('app.locale_prefix')).'/'.$news->slug)) }}"
                                                                        class="swin-transition">{{ \Lang::get('front.bt-readmore',[], App::getLocale()) }}
                                                                    <i class="fa fa-angle-double-right"></i></a></div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                @endforeach
                                </div>
                            </div>
                            <div class="page-sidebar col-md-4">
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>


@endsection