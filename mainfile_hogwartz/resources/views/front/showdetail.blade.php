@extends('layouts.front')

@section('customcss')
    <style type="text/css">
        #share-buttons img {
            width: 35px;
            padding: 5px;
            border: 0;
            box-shadow: 0;
            display: inline;
        }
    </style>
@endsection

@section('custom-pluginscript')
    <script src="{{ asset('assets/front/js/jquery.parallax-1.1.3.js') }}"></script>
    <script src="{{ asset('assets/front/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('assets/front/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ asset('assets/front/js/slider.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/front/js/jquery.mixitup.min.js') }}"></script>
@endsection

@section('custom-script')

@endsection

@section('page-head-seo')
    <meta name="description" content="{{ $show->meta_description }}">
    <meta name="keywords" content="{{ $show->meta_keyword }}">
    <title>{{ $show->meta_title }} - Hogwartz The Pub Bali</title>
@endsection

@section('conten')

<div class="page-container">
          <div data-bottom-top="background-position: 50% 50px;" data-center="background-position: 50% 0px;" data-top-bottom="background-position: 50% -50px;" class="page-title page-blog">
            <div class="container">
              <div class="title-wrapper">
                <div data-top="transform: translateY(0px);opacity:1;" data--120-top="transform: translateY(-30px);opacity:0;" data-anchor-target=".page-title" class="title">{{ $show->title }}</div>
                <div data-top="opacity:1;" data--120-top="opacity:0;" data-anchor-target=".page-title" class="divider"><span class="line-before"></span><span class="dot"></span><span class="line-after"></span></div>
              </div>
            </div>
          </div>
          <div class="page-content-wrapper">
            <div class="container">
              <div class="padding-top-100 padding-bottom-100">
                <div class="row">
                  <div class="page-main col-md-8">
                    <div class="blog-wrapper swin-blog-single">
                      <div class="swin-sc-blog-slider style-02">
                        <div class="main-slider">
                          <div class="slides">
                            <div class="blog-item">
                              <div class="blog-featured-img"><img src="{{ asset('assets/front/images/'.$show->image) }}" alt="fooday" class="img img-responsive"></div>
                              <div class="blog-content">
                                <div class="blog-meta-info">
                                  <div class="blog-date">
                                    <p></p>
                                    <span class="month">{{ $show->schedule }}</span></div>
                                  <div class="blog-info clearfix">
                                    <!-- <div class="blog-info-item blog-view">
                                      <p><i class="fa fa-eye"></i><span>18</span></p>
                                      <p></p>
                                    </div>
                                    <div class="blog-info-item blog-comment">
                                      <p><i class="fa fa-comment"></i><span>18</span></p>
                                      <p></p>
                                    </div>
                                    <div class="blog-info-item blog-author">
                                      <p><span>Post By </span><a href="#">Admin</a></p>
                                      <p></p>
                                    </div> -->
                                  </div>
                                  <h3 class="blog-title"><a href="#" class="swin-transition">{{ $show->title }}</a></h3>
                                </div>
                                <div class="blog-content-inner">
                                  
                                  {!! $show->content !!}
                                  
                                </div>
                                <div class="blog-footer clearfix">
                                  <div class="blog-share">
                                    <ul class="socials list-unstyled list-inline">
                                      <li class="black"><a href="https://www.facebook.com/sharer.php?u={{ url(preg_replace('#/+#','/', config('app.locale_prefix').'/'.\Lang::get('route.schedule',[], config('app.locale_prefix')).'/'.$show->slug)) }}" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                <li class="black"><a href="https://twitter.com/share?url={{ url(preg_replace('#/+#','/', config('app.locale_prefix').'/'.\Lang::get('route.schedule',[], config('app.locale_prefix')).'/'.$show->slug)) }}&amp;text={{ $show->title.' - Bebek Bengil Restaurant' }}" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                <li class="black"><a href="https://plus.google.com/share?url={{ url(preg_replace('#/+#','/', config('app.locale_prefix').'/'.\Lang::get('route.schedule',[], config('app.locale_prefix')).'/'.$show->slug)) }}" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                <li class="black"><a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>                      
                    </div>
                  </div>
                  <div class="page-sidebar col-md-4">
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
@endsection