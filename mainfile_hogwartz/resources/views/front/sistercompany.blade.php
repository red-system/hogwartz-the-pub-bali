@extends('layouts.front')

@section('custom-pluginscript')
    <script src="{{ asset('assets/front/js/jquery.parallax-1.1.3.js') }}"></script>
    <script src="{{ asset('assets/front/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('assets/front/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ asset('assets/front/js/slider.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/front/js/jquery.mixitup.min.js') }}"></script>
@endsection

@section('page-head-seo')
    <meta name="description" content="{{ $sistercompany->meta_description }}">
    <meta name="keywords" content="{{ $sistercompany->meta_keyword }}">
    <title>{{ $sistercompany->meta_title }} - Hogwartz The Pub Bali</title>
@endsection

@section('conten')
    <section id="page_header">
        <div class="page_title">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="title">{{ $sistercompany->title }}</h2>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section class="padding" id="popular">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="heading">{{ $sistercompany->title }}</h2>
                    <hr>
                </div>
            </div>
            <?php $no = 1; ?>
            @foreach($listcompany as $scomp)
                @if($no == 1 || ($no%3 == 1))<div class="row">@endif
                    <div class="col-sm-4">
                        <div class="popular top40 text-center">
                            <div class="image">
                                <img src="{{ asset('assets/front/images/'.$scomp->image) }}" alt="popular1">
                                <a class="fancybox overlay-inner" href="{{ asset('assets/front/images/'.$scomp->image) }}" data-fancybox-group="gallery">
                                    <div class="overlay">
                                        <i class="icon-eye6"></i>
                                    </div>
                                </a>
                            </div>
                            <h4>{{ $scomp->title }}</h4>
                            <p>{!! $scomp->short_description !!}</p>
                            @if($scomp->link != '')<a href="{{ $scomp->link }}" class="btn-common" style="border-radius: 0px; text-align: center;">{{ \Lang::get('front.visit-site',[], App::getLocale()) }}</a>@endif
                        </div>
                    </div>
                @if(($no%3 == 0) || $listcompany->last() == $scomp)</div>@endif
                <?php $no++; ?>
            @endforeach
        </div>
    </section>
@endsection