<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index,follow">
    <meta name="googlebot" content="index,follow">
    <meta name="revisit-after" content="2 days">
    <meta name="author" content="Ganeshcom Studio">
    <meta name="author" content="http://ganeshcomstudio.com">
    <meta name="reply-to" content="{{ $emailcomposer }}">
    <meta name="owner" content="{{ $emailcomposer }}">
    <meta name="copyright" content="Bebek Bengil Restaurant">
    <meta name="expires" content="{{ gmdate(DATE_RFC850, strtotime('+1 year')) }}">
    <meta name="language" content="{{ App::getLocale() }}">
    <meta name="web_author" content="Ganeshcom Studio">
    <meta name="web_author" content="http://ganeshcomstudio.com">
    <meta name="_token" content="{!! csrf_token() !!}">
    @yield('page-head-seo')

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicons/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicons/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('favicons/manifest.json') }}">
    <link rel="mask-icon" href="{{ asset('favicons/safari-pinned-tab.svg') }}" color="#5bbad5">
    <link rel="shortcut icon" href="{{ asset('favicons/favicon.ico') }}">
    <meta name="msapplication-config" content="{{ asset('favicons/browserconfig.xml') }}">
    <meta name="theme-color" content="#ffffff">


    <!-- Bootstrap CSS-->
    <link href="{{ asset('assets/front/vendors/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">
    <!-- Font Awesome-->
    <link rel="stylesheet" href="{{ asset('assets/front/vendors/font-awesome/css/font-awesome.min.css') }}">
    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries-->
    <!-- WARNING: Respond.js doesn't work if you view the page via file://-->
    <!-- IE 9-->
    <!-- Vendors-->
    <link rel="stylesheet" href="{{ asset('assets/front/vendors/flexslider/flexslider.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/front/vendors/swipebox/css/swipebox.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/front/vendors/slick/slick.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/front/vendors/slick/slick-theme.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/front/vendors/animate.min.css') }}">
    <link rel="stylesheet"
          href="{{ asset('assets/front/vendors/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/vendors/lightbox/css/lightbox.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/front/vendors/pageloading/css/component.min.css') }}">
    <!-- Font-icon-->
    <link rel="stylesheet" href="{{ asset('assets/front/fonts/font-icon/style.css') }}">
    <!-- Style-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/elements.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/extra.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/widget.css') }}">
    <!--link#colorpattern(rel='stylesheet', type='text/css', href='assets/css/color/colordefault.css')-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/responsive.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/pnotify.css') }}">
@yield('customcss')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/layout.css') }}">

<!-- Google Font-->
    <link href="https://fonts.googleapis.com/css?family=Merriweather+Sans:400,700,700i" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Rancho" rel="stylesheet">
    <!-- Script Loading Page-->
    <script src="{{ asset('assets/front/vendors/html5shiv.js') }}"></script>
    <script src="{{ asset('assets/front/vendors/respond.min.js') }}"></script>
    <script src="{{ asset('assets/front/vendors/pageloading/js/snap.svg-min.js') }}"></script>
    <script src="{{ asset('assets/front/vendors/pageloading/sidebartransition/js/modernizr.custom.js') }}"></script>
    <!-- jQuery-->
    <script src="{{ asset('assets/front/vendors/jquery-1.10.2.min.js') }}"></script>
</head>
<body>
<div id="pagewrap" class="pagewrap">
    <div id="html-content" class="wrapper-content">

        <!-- Main navbar -->
        <header>
            <div class="header-top top-layout-02">
                <div class="container">
                    <div class="topbar-left">
                        <div class="topbar-content">
                            <?php
                            $headertext = $webcfg_composer->where('config_name', 'headertext_config')->first();
                            $tlp = $webcfg_composer->where('config_name', 'no_tlpconfig')->first();
                            ?>
                            <div class="item">
                                <div class="wg-contact"><i
                                            class="fa fa-map-marker"></i><span>{{ $headertext->value }}</span></div>
                            </div>
                            <div class="item">
                                <div class="wg-contact"><i class="fa fa-phone"></i><span>{!! $tlp->value !!}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="topbar-right">
                        <div class="topbar-content">
                            <div class="item">
                                <ul class="socials-nb list-inline wg-social">
                                    @foreach($socialmedias as $scm)
                                        <li><a href="{{ $scm->link }}"><i
                                                        class="fa {{ $scm->platform_favicon }}"></i></a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="header-main">
                <div class="container">
                    <div class="open-offcanvas">&#9776;</div>

                    <div class="header-logo"><a href="{{ url(preg_replace('#/+#','/', config('app.locale_prefix'))) }}"
                                                class="logo"><img src="{{ asset('assets/front/images/logo-2.png') }}"
                                                                  alt="fooday" class="logo-img" width="150"></a></div>
                    <nav id="main-nav-offcanvas" class="main-nav-wrapper">
                        <div class="close-offcanvas-wrapper"><span class="close-offcanvas">x</span></div>
                        <div class="main-nav">
                            <ul id="main-nav" class="nav nav-pills">
                                @foreach($mutama as $mu)
                                    @if($mu->childs()->where('position', 'sub-menu-utama')->where('published', '1')->where('lang', App::getLocale())->count() > 0)
                                        <li class="dropdown"><a href="#" class="dropdown-toggle"
                                                                data-toggle="dropdown">{{ $mu->title }}</a>
                                            <ul class="dropdown-menu">
                                                @foreach($mu->childs()->where('position', 'sub-menu-utama')->where('published', '1')->where('lang', App::getLocale())->get() as $child)
                                                    <li>
                                                        <a href="{{ url(preg_replace('#/+#','/',config('app.locale_prefix').'/'.$child->link.'/'.$child->slug)) }}">{{ $child->title }}</a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @else
                                        <li>
                                            <a href="{{ url(preg_replace('#/+#','/', config('app.locale_prefix').'/'.$mu->link)) }}">{!! $mu->title !!}</a>
                                        </li>
                                    @endif
                                @endforeach
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button"
                                       aria-expanded="false">
                                        {{ strtoupper(\App::getLocale()) }}</span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        @foreach(config('app.all_langs') as $weblang)
                                            <?php
                                            if (!empty($altlink)) {
                                                if (array_key_exists($weblang, $altlink)) {
                                                    $urlswlang = $altlink[$weblang];
                                                } else {
                                                    $urlswlang = '#';
                                                }
                                            } else {
                                                $urlswlang = '#';
                                            }
                                            ?>
                                            <li><a href="{{ $urlswlang }}" class="language"> <span
                                                            class="flag-icon {{ config('app.flag_langs')[$weblang] }} icon-small"></span>  <span>{{ config('app.human_langs')[$weblang] }}</span></a>
                                            </li>
                                        @endforeach
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </header>
        <!-- /main navbar -->

    @yield('conten')


    <!-- Footer -->
        <footer>
            <?php
            $footer_text = $footerconten->where('position', 'footer-about')->first();
            $email = $webcfg_composer->where('config_name', 'email')->first();
            ?>
            <div class="footer-top"></div>
            <div class="footer-main">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8">
                            <div class="ft-widget-area">
                                <div class="ft-area1">
                                    <div class="swin-wget swin-wget-about">
                                        <div class="clearfix"><a class="wget-logo"><img
                                                        src="{{ asset('assets/front/images/logo-5.png') }}" alt=""
                                                        class="img img-responsive" width="150"></a>
                                            <ul class="socials socials-about list-unstyled list-inline">
                                                @foreach($socialmedias as $scm)
                                                    <li><a href="{{ $scm->link }}"><i
                                                                    class="fa {{ $scm->platform_favicon }}"></i></a>
                                                    </li>
                                                @endforeach
                                            </ul>
                                        </div>
                                        <div class="wget-about-content">
                                            <p>{!! $footer_text->conten !!}</p>
                                        </div>
                                        <div class="about-contact-info clearfix" style="font-size: 12px;">
                                            <div class="address-info">
                                                <div class="info-icon"><i class="fa fa-map-marker"></i></div>
                                                <div class="info-content" style="padding-top: 8px;">
                                                    <p>{{ $headertext->value }} </p>
                                                </div>
                                            </div>
                                            <div class="phone-info">
                                                <div class="info-icon"><i class="fa fa-mobile-phone"></i></div>
                                                <div class="info-content" style="padding-top: 8px;">
                                                    <p> {{ $tlp->value }}</p>
                                                </div>
                                            </div>
                                            <div class="email-info">
                                                <div class="info-icon"><i class="fa fa-envelope"></i></div>
                                                <div class="info-content" style="padding-top: 8px;">
                                                    <p>{{ $email->value }}</p>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="ft-fixed-area">
                                <div class="reservation-box">
                                    <div class="reservation-wrap">
                                        <h3 class="res-title">Open Hour</h3>
                                        <div class="res-date-time">
                                            <?php
                                            $monday_open = $webcfg_composer->where('config_name', 'monday_open')->first()->value;
                                            $monday_close = $webcfg_composer->where('config_name', 'monday_close')->first()->value;
                                            ?>
                                            <div class="res-date-time-item">
                                                <div class="res-date">
                                                    <div class="res-date-item">
                                                        <div class="res-date-text">
                                                            <p>Monday:</p>
                                                        </div>
                                                        <div class="res-date-dot">
                                                            .......................................
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="res-time">
                                                    <div class="res-time-item">
                                                        <p>
                                                            {{ $monday_open }}
                                                            @if( $monday_open != "Closed" )
                                                                - {{ $monday_close }}
                                                            @endif
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <?php
                                            $tuesday_open = $webcfg_composer->where('config_name', 'tuesday_open')->first()->value;
                                            $tuesday_close = $webcfg_composer->where('config_name', 'tuesday_close')->first()->value;
                                            ?>
                                            <div class="res-date-time-item">
                                                <div class="res-date">
                                                    <div class="res-date-item">
                                                        <div class="res-date-text">
                                                            <p>Tuesday:</p>
                                                        </div>
                                                        <div class="res-date-dot">
                                                            .......................................
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="res-time">
                                                    <div class="res-time-item">
                                                        <p>
                                                            {{ $tuesday_open }}
                                                            @if( $tuesday_open != "Closed" )
                                                                - {{ $tuesday_close }}
                                                            @endif
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <?php
                                            $wednesday_open = $webcfg_composer->where('config_name', 'wednesday_open')->first()->value;
                                            $wednesday_close = $webcfg_composer->where('config_name', 'wednesday_close')->first()->value;
                                            ?>
                                            <div class="res-date-time-item">
                                                <div class="res-date">
                                                    <div class="res-date-item">
                                                        <div class="res-date-text">
                                                            <p>Wednesday:</p>
                                                        </div>
                                                        <div class="res-date-dot">
                                                            .......................................
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="res-time">
                                                    <div class="res-time-item">
                                                        <p>
                                                            {{ $wednesday_open }}
                                                            @if( $wednesday_open != "Closed" )
                                                                - {{ $wednesday_close }}
                                                            @endif
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>


                                            <?php
                                            $thursday_open = $webcfg_composer->where('config_name', 'thursday_open')->first()->value;
                                            $thursday_close = $webcfg_composer->where('config_name', 'thursday_close')->first()->value;
                                            ?>
                                            <div class="res-date-time-item">
                                                <div class="res-date">
                                                    <div class="res-date-item">
                                                        <div class="res-date-text">
                                                            <p>Thursday:</p>
                                                        </div>
                                                        <div class="res-date-dot">
                                                            .......................................
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="res-time">
                                                    <div class="res-time-item">
                                                        <p>
                                                            {{ $thursday_open }}
                                                            @if( $thursday_open != "Closed" )
                                                                - {{ $thursday_close }}
                                                            @endif
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <?php
                                            $friday_open = $webcfg_composer->where('config_name', 'friday_open')->first()->value;
                                            $friday_close = $webcfg_composer->where('config_name', 'friday_close')->first()->value;
                                            ?>
                                            <div class="res-date-time-item">
                                                <div class="res-date">
                                                    <div class="res-date-item">
                                                        <div class="res-date-text">
                                                            <p>Friday:</p>
                                                        </div>
                                                        <div class="res-date-dot">
                                                            .......................................
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="res-time">
                                                    <div class="res-time-item">
                                                        <p>
                                                            {{ $friday_open }}
                                                            @if( $friday_open != "Closed" )
                                                                - {{ $friday_close }}
                                                            @endif
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <?php
                                            $saturday_open = $webcfg_composer->where('config_name', 'saturday_open')->first()->value;
                                            $saturday_close = $webcfg_composer->where('config_name', 'saturday_close')->first()->value;
                                            ?>
                                            <div class="res-date-time-item">
                                                <div class="res-date">
                                                    <div class="res-date-item">
                                                        <div class="res-date-text">
                                                            <p>Saturday:</p>
                                                        </div>
                                                        <div class="res-date-dot">
                                                            .......................................
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="res-time">
                                                    <div class="res-time-item">
                                                        <p>
                                                            {{ $saturday_open }}
                                                            @if( $saturday_open != "Closed" )
                                                                - {{ $saturday_close }}
                                                            @endif
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                            <?php
                                            $sunday_open = $webcfg_composer->where('config_name', 'sunday_open')->first()->value;
                                            $sunday_close = $webcfg_composer->where('config_name', 'sunday_close')->first()->value;
                                            ?>
                                            <div class="res-date-time-item">
                                                <div class="res-date">
                                                    <div class="res-date-item">
                                                        <div class="res-date-text">
                                                            <p>Sunday:</p>
                                                        </div>
                                                        <div class="res-date-dot">
                                                            .......................................
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="res-time">
                                                    <div class="res-time-item">
                                                        <p>
                                                            {{ $sunday_open }}
                                                            @if( $sunday_open != "Closed" )
                                                                - {{ $sunday_close }}
                                                            @endif
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="clearfix"></div>
                                            </div>

                                        </div>
                                        <h3 class="res-title">Reservation Numbers</h3>
                                        <p class="res-number">{{ $tlp->value }}</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>
        <a id="totop" href="#" class="animated"><i class="fa fa-angle-double-up"></i></a>
        <!-- End Footer -->

    </div>

</div>
<!-- Bootstrap JavaScript-->
<script src="{{ asset('assets/front/vendors/bootstrap/js/bootstrap.min.js') }}"></script>
<!-- Vendors-->
<script src="{{ asset('assets/front/vendors/flexslider/jquery.flexslider-min.js') }}"></script>
<script src="{{ asset('assets/front/vendors/swipebox/js/jquery.swipebox.min.js') }}"></script>
<script src="{{ asset('assets/front/vendors/slick/slick.min.js') }}"></script>
<script src="{{ asset('assets/front/vendors/isotope/isotope.pkgd.min.js') }}"></script>
<script src="{{ asset('assets/front/vendors/jquery-countTo/jquery.countTo.min.js') }}"></script>
<script src="{{ asset('assets/front/vendors/jquery-appear/jquery.appear.min.js') }}"></script>
<script src="{{ asset('assets/front/vendors/parallax/parallax.min.js') }}"></script>
<script src="{{ asset('assets/front/vendors/gmaps/gmaps.min.js') }}"></script>
<script src="{{ asset('assets/front/vendors/audiojs/audio.min.js') }}"></script>
<script src="{{ asset('assets/front/vendors/vide/jquery.vide.min.js') }}"></script>
<script src="{{ asset('assets/front/vendors/pageloading/js/svgLoader.min.js') }}"></script>
<script src="{{ asset('assets/front/vendors/pageloading/js/classie.min.js') }}"></script>
<script src="{{ asset('assets/front/vendors/pageloading/sidebartransition/js/sidebarEffects.min.js') }}"></script>
<script src="{{ asset('assets/front/vendors/nicescroll/jquery.nicescroll.min.js') }}"></script>
<script src="{{ asset('assets/front/vendors/wowjs/wow.min.js') }}"></script>
<script src="{{ asset('assets/front/vendors/skrollr.min.js') }}"></script>
<script src="{{ asset('assets/front/vendors/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js"
        integrity="sha384-mE6eXfrb8jxl0rzJDBRanYqgBxtJ6Unn4/1F7q4xRRyIw7Vdg9jP4ycT7x1iVsgb"
        crossorigin="anonymous"></script>
<script src="{{ asset('assets/front/vendors/lightbox/js/lightbox.js') }}" type="text/javascript"></script>
<!-- Own script-->
<script src="{{ asset('assets/front/js/layout.js') }}"></script>
<script src="{{ asset('assets/front/js/elements.js') }}"></script>
<script src="{{ asset('assets/front/js/widget.js') }}"></script>
<script src="{{ asset('assets/front/js/jquery-canvas-sparkles.js') }}"></script>
<script src="{{ asset('assets/front/js/pnotify.min.js') }}"></script>
<script type="text/javascript">
    $("a").sparkle();
    $("input").sparkle();
</script>
@yield('custom-pluginscript')
@yield('custom-script')

<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/5cf5d260b534676f32ad35ad/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
</body>
</html>