<!doctype html>
<html lang="{{ App::getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index,follow">
    <meta name="googlebot" content="index,follow">
    <meta name="revisit-after" content="2 days">
    <meta name="author" content="Ganeshcom Studio">
    <meta name="author" content="http://ganeshcomstudio.com">
    <meta name="reply-to" content="{{ $emailcomposer }}">
    <meta name="owner" content="{{ $emailcomposer }}">
    <meta name="copyright" content="Bebek Bengil Restaurant">
    <meta name="expires" content="{{ gmdate(DATE_RFC850, strtotime('+1 year')) }}">
    <meta name="language" content="{{ App::getLocale() }}">
    <meta name="web_author" content="Ganeshcom Studio">
    <meta name="web_author" content="http://ganeshcomstudio.com">
    <meta name="_token" content="{!! csrf_token() !!}">
    @yield('page-head-seo')

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicons/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicons/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('favicons/manifest.json') }}">
    <link rel="mask-icon" href="{{ asset('favicons/safari-pinned-tab.svg') }}" color="#5bbad5">
    <link rel="shortcut icon" href="{{ asset('favicons/favicon.ico') }}">
    <meta name="msapplication-config" content="{{ asset('favicons/browserconfig.xml') }}">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/bistro-icons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/settings.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/navigation.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/owl.transitions.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/jquery.fancybox.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/zerogrid.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/style.css?version=3') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/loader.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/flag/css/flag-icon.min.css') }}" />
    <style>
        .icon-medium
        {
            height:16px;
            margin:0 3px 0 0;
            padding:0;
        }
    </style>
    @yield('customcss')
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

<!--Topbar-->
<div class="topbar">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <?php
                    $headertext = $webcfg_composer->where('config_name', 'headertext_config')->first();
                    $tlp = $webcfg_composer->where('config_name', 'no_tlpconfig')->first();
                ?>
                <p class="pull-left hidden-xs">{{ $headertext->value }}</p>
                <p class="pull-right"><a href="tel:{!! $tlp->value !!}"> <i class="fa fa-phone" style="margin-right: 5px;"></i> {{ \Lang::get('front.callus',[], \App::getLocale()) }}: {!! $tlp->value !!}</a></p>
            </div>
        </div>
    </div>
</div>

<!--Header-->
<header id="main-navigation">
    <div id="navigation" data-spy="affix" data-offset-top="20">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <nav class="navbar navbar-default">
                        <div class="navbar-header">
                            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#fixed-collapse-navbar" aria-expanded="false">
                                <span class="icon-bar top-bar"></span> <span class="icon-bar middle-bar"></span> <span class="icon-bar bottom-bar"></span>
                            </button>
                            <a class="navbar-brand" href="{{ url(preg_replace('#/+#','/', config('app.locale_prefix'))) }}"><img src="{{ asset('assets/front/images/bebek-bengil.jpg') }}" alt="logo" class="img-responsive" style="max-height: 80px;"></a>
                        </div>

                        <div id="fixed-collapse-navbar" class="navbar-collapse collapse navbar-right">
                            <ul class="nav navbar-nav">
                                @foreach($mutama as $mu)
                                    @if($mu->childs()->where('position', 'sub-menu-utama')->where('published', '1')->where('lang', App::getLocale())->count() > 0)
                                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">{{ $mu->title }}</a>
                                            <ul class="dropdown-menu">
                                                @foreach($mu->childs()->where('position', 'sub-menu-utama')->where('published', '1')->where('lang', App::getLocale())->get() as $child)
                                                    <li><a href="{{ url(preg_replace('#/+#','/',config('app.locale_prefix').'/'.$child->link.'/'.$child->slug)) }}">{{ $child->title }}</a></li>
                                                @endforeach
                                            </ul>
                                        </li>
                                    @else
                                        <li><a href="{{ url(preg_replace('#/+#','/', config('app.locale_prefix').'/'.$mu->link)) }}">{!! $mu->title !!}</a></li>
                                    @endif
                                @endforeach
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                        <span class="flag-icon {{ config('app.flag_langs')[\App::getLocale()] }} icon-small"></span> {{ strtoupper(\App::getLocale()) }} <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        @foreach(config('app.all_langs') as $weblang)
                                            <?php
                                                if (!empty($altlink)) {
                                                    if (array_key_exists($weblang, $altlink)) {
                                                        $urlswlang = $altlink[$weblang];
                                                    } else {
                                                        $urlswlang = '#';
                                                    }
                                                } else {
                                                    $urlswlang = '#';
                                                }
                                            ?>
                                            <li><a href="{{ $urlswlang }}" class="language"> <span class="flag-icon {{ config('app.flag_langs')[$weblang] }} icon-small"></span>  <span>{{ config('app.human_langs')[$weblang] }}</span></a></li>
                                        @endforeach
                                    </ul>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
</header>

@yield('conten')

<!--Footer-->
<footer class="padding-top bg_black">
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-sm-6 footer_column">
                <?php $footerabout = $footerconten->where('position', 'footer-about')->first(); ?>
                <h4 class="heading">{{ $footerabout->title }}</h4>
                <hr class="half_space">
                <p class="half_space">{!! $footerabout->conten !!}</p>
            </div>
            <div class="col-md-4 col-sm-6 footer_column">
                <h4 class="heading">Quick Links</h4>
                <hr class="half_space">
                <ul class="widget_links">
                    @foreach($mutama as $mu)
                        <li><a href="{{ url(preg_replace('#/+#','/', config('app.locale_prefix').'/'.$mu->link)) }}">{!! $mu->title !!}</a></li>
                    @endforeach
                </ul>
            </div>
            <div class="col-md-4 col-sm-6 footer_column">
                <?php $footercontact = $footerconten->where('position', 'footer-hubungikami')->first(); ?>
                <h4 class="heading">{!! $footercontact->title !!}</h4>
                <hr class="half_space">
                <div>
                    {!! $footercontact->conten !!}
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="copyright clearfix">
                    <p>2017 &copy;  Bebek Bengil Restaurant powered by <a href="http://ganeshcomstudio.com">Ganeshcom Studio</a></p>
                    <ul class="social_icon">
                        @foreach($socialmedias as $scm)
                            <li><a href="{{ $scm->link }}"><i class="fa {{ $scm->platform_favicon }}"></i></a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

<a href="#" id="back-top"><i class="fa fa-angle-up fa-2x"></i></a>

<script src="{{ asset('assets/front/js/jquery-2.2.3.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/front/js/bootstrap.min.js') }}" type="text/javascript"></script>
@yield('custom-pluginscript')
<script src="{{ asset('assets/front/js/owl.carousel.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/front/js/jquery.parallax-1.1.3.js') }}"></script>
<script src="{{ asset('assets/front/js/jquery.mixitup.min.js') }}"></script>
<script src="{{ asset('assets/front/js/jquery-countTo.js') }}"></script>
<script src="{{ asset('assets/front/js/jquery.appear.js') }}"></script>
<script src="{{ asset('assets/front/js/jquery.fancybox.js') }}"></script>
<script src="{{ asset('assets/front/js/functions.js') }}" type="text/javascript"></script>
@yield('custom-script')
<script type="text/javascript">
    var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
    (function(){
        var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
        s1.async=true;
        s1.src='https://embed.tawk.to/59f493fa4854b82732ff86d9/default';
        s1.charset='UTF-8';
        s1.setAttribute('crossorigin','*');
        s0.parentNode.insertBefore(s1,s0);
    })();
</script>
</body>
</html>
