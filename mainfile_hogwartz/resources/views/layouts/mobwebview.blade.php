<!doctype html>
<html lang="{{ App::getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="robots" content="index,follow">
    <meta name="googlebot" content="index,follow">
    <meta name="revisit-after" content="2 days">
    <meta name="author" content="Ganeshcom Studio">
    <meta name="author" content="http://ganeshcomstudio.com">
    <meta name="reply-to" content="{{ $emailcomposer }}">
    <meta name="owner" content="{{ $emailcomposer }}">
    <meta name="copyright" content="Bebek Bengil Restaurant">
    <meta name="expires" content="{{ gmdate(DATE_RFC850, strtotime('+1 year')) }}">
    <meta name="language" content="{{ App::getLocale() }}">
    <meta name="web_author" content="Ganeshcom Studio">
    <meta name="web_author" content="http://ganeshcomstudio.com">
    <meta name="_token" content="{!! csrf_token() !!}">
    @yield('page-head-seo')

    <link rel="apple-touch-icon" sizes="180x180" href="{{ asset('favicons/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('favicons/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ asset('favicons/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ asset('favicons/manifest.json') }}">
    <link rel="mask-icon" href="{{ asset('favicons/safari-pinned-tab.svg') }}" color="#5bbad5">
    <link rel="shortcut icon" href="{{ asset('favicons/favicon.ico') }}">
    <meta name="msapplication-config" content="{{ asset('favicons/browserconfig.xml') }}">
    <meta name="theme-color" content="#ffffff">

    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/bootstrap.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/font-awesome.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/bistro-icons.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/settings.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/navigation.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/owl.carousel.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/owl.transitions.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/jquery.fancybox.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/zerogrid.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/style.css?version=3') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/css/loader.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/flag/css/flag-icon.min.css') }}" />
    <style>
        .icon-medium
        {
            height:16px;
            margin:0 3px 0 0;
            padding:0;
        }
    </style>
    @yield('customcss')
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>

@yield('conten')

<a href="#" id="back-top"><i class="fa fa-angle-up fa-2x"></i></a>

<script src="{{ asset('assets/front/js/jquery-2.2.3.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/front/js/bootstrap.min.js') }}" type="text/javascript"></script>
@yield('custom-pluginscript')
<script src="{{ asset('assets/front/js/owl.carousel.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('assets/front/js/jquery.parallax-1.1.3.js') }}"></script>
<script src="{{ asset('assets/front/js/jquery.mixitup.min.js') }}"></script>
<script src="{{ asset('assets/front/js/jquery-countTo.js') }}"></script>
<script src="{{ asset('assets/front/js/jquery.appear.js') }}"></script>
<script src="{{ asset('assets/front/js/jquery.fancybox.js') }}"></script>
<script src="{{ asset('assets/front/js/functions.js') }}" type="text/javascript"></script>
@yield('custom-script')
</body>
</html>
