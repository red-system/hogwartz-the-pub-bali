<aside class="sidebar" style="margin-top: 20px;">
    <div class="widget">
        <ul class="tabs" style="text-align: left;">
            <li class="active" rel="tab1">{{ \Lang::get('front.news-recent',[], App::getLocale()) }}</li>
        </ul>
        <div class="tab_container bg_grey">
            <h3 class="d_active tab_drawer_heading" rel="tab1">{{ \Lang::get('front.news-recent',[], App::getLocale()) }}</h3>
            <div id="tab1" class="tab_content">
                @foreach($newsrecents as $newsr)
                    <div class="single_comments">
                        <img alt="{{ $newsr->title }}" src="{{ asset('assets/front/images/'.$newsr->image) }}">
                        <p><a href="{{ url(preg_replace('#/+#','/', 'mob/'.config('app.locale_prefix').'/'.\Lang::get('route.news',[], config('app.locale_prefix')).'/'.$newsr->slug)) }}">{{ $newsr->title }}</a>
                            <span>{{ date('M d, Y', strtotime($newsr->created_at)) }}</span>
                        </p>
                    </div>
                    <div class="clearfix"></div>
                @endforeach
            </div>
        </div>
    </div>
    <div class="widget">
        <h3>{{ \Lang::get('front.news-category',[], App::getLocale()) }}</h3>
        <ul class="widget_links">
            @foreach($newscategories as $nc)
                <li><a href="{{ url(preg_replace('#/+#','/', 'mob/'.config('app.locale_prefix').'/'.\Lang::get('route.news',[], config('app.locale_prefix')).'/category/'.$nc->slug)) }}">{{ $nc->category }}</a></li>
            @endforeach
        </ul>
    </div>
    <div class="widget">
        <h3>{{ \Lang::get('front.news-tag',[], App::getLocale()) }}</h3>
        <ul class="tags">
            @foreach($newstags as $nt)
                <li><a href="{{ url(preg_replace('#/+#','/', 'mob/'.config('app.locale_prefix').'/'.\Lang::get('route.news',[], config('app.locale_prefix')).'/tag/'.$nt->slug)) }}">{{ $nt->tag }}</a></li>
            @endforeach
        </ul>
    </div>
    <div class="widget">
        <h3>{{ \Lang::get('front.news-archive',[], App::getLocale()) }}</h3>
        <ul class="widget_links">
            @foreach($newsarchive as $narchive)
                <li><a href="{{ url(preg_replace('#/+#','/', 'mob/'.config('app.locale_prefix').'/'.\Lang::get('route.news',[], config('app.locale_prefix')).'/archive/'.$narchive->thbln)) }}">{{ date('F Y',strtotime($narchive->created_at)) }} <span style="float: right;">({{ $narchive->jumlah }})</span></a></li>
            @endforeach
        </ul>
    </div>
</aside>