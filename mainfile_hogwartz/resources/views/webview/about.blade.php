@extends('layouts.mobwebview')

@section('page-head-seo')
    <meta name="description" content="{{ $about->meta_description }}">
    <meta name="keywords" content="{{ $about->meta_keyword }}">
    <title>{{ $about->meta_title }} - Bebek Bengil Restaurant</title>
@endsection

@section('conten')
    <section id="overview" class="padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="heading">{{ $restaurantoverview->title }}</h2>
                    <hr class="heading_space">
                </div>
                <div class="col-md-6 col-sm-6">
                    {!! $restaurantoverview->description !!}
                </div>
                <div class="col-md-6 col-sm-6">
                    <img class="img-responsive" src="{{ asset('assets/front/images/'.$restaurantoverview->image) }}">
                </div>
            </div>
        </div>
    </section>

    <section id="overview" class="padding bg_grey">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="heading">{{ $founderprofile->title }}</h2>
                    <hr class="heading_space">
                </div>
                <div class="col-md-5 col-sm-6" style="margin-bottom: 10px;">
                    <img class="img-responsive" src="{{ asset('assets/front/images/'.$founderprofile->image) }}">
                </div>
                <div class="col-md-7 col-sm-6">
                    {!! $founderprofile->description !!}
                </div>
            </div>
        </div>
    </section>

    <section id="overview" class="padding">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="heading">{{ $companyprofile->title }}</h2>
                    <hr class="heading_space">
                </div>
                <div class="col-md-6 col-sm-6">
                    {!! $companyprofile->description !!}
                </div>
                <div class="col-md-6 col-sm-6">
                    <div class="video-container">
                        <iframe width="854" height="480" src="{{ $companyprofile->video }}" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <section id="overview" class="padding bg_grey">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <h2 class="heading">{{ $cooporateinformation->title }}</h2>
                    <hr class="heading_space">
                </div>
                <div class="col-md-5 col-sm-6" style="margin-bottom: 10px;">
                    <img class="img-responsive" src="{{ asset('assets/front/images/'.$cooporateinformation->image) }}">
                </div>
                <div class="col-md-7 col-sm-6">
                    {!! $cooporateinformation->description !!}
                </div>
            </div>
        </div>
    </section>

    <section class="padding" id="popular">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="heading">{{ \Lang::get('front.core-business',[], App::getLocale()) }}</h2>
                    <hr>
                </div>
                @foreach($corebuisiness as $cb)
                <div class="col-sm-4">
                    <div class="popular top40 text-center">
                        <div class="image">
                            <img src="{{ asset('assets/front/images/'.$cb->image) }}" alt="{{ $cb->title }}">
                            <div class="overlay">
                                <a class="fancybox overlay-inner" href="{{ asset('assets/front/images/'.$cb->image) }}" data-fancybox-group="gallery">
                                    <i class="icon-eye6"></i>
                                </a>
                            </div>
                        </div>
                        <div class="content">
                            <h4>{{ $cb->title }}</h4>
                            <p>{!! $cb->description !!}</p>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
@endsection