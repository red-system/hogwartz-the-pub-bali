@extends('layouts.mobwebview')

@section('custom-pluginscript')
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyBjrqLZ_-1xCYMThroQr6tB6S0GruM5x0k"></script>
    <script src="{{ asset('assets/front/js/gmap3.js') }}"></script>
@endsection

@section('custom-script')
    <script>
        var dataListOutlet;
        function getLocation() {
            if (navigator.geolocation) {
                navigator.geolocation.getCurrentPosition(showPosition, showError);
            } else {
                showMapWithoutGeo();
            }
        }

        function showPosition(position) {
            var lat = parseFloat(position.coords.latitude);
            var lng = parseFloat(position.coords.longitude);

            $.post( "{!! url(preg_replace('#/+#','/',  config('app.locale_prefix').'/'.\Lang::get('route.contact',[], \App::getLocale()).'/getdataoutlet')) !!}", { 'lat': lat, 'long': lng, '_token' : $('meta[name="_token"]').attr('content') }, function( response ) {
                dataListOutlet = response;

                var first = response[0];
                $('#map').gmap3({
                    center:[first.lat, first.lng],
                    zoom:17
                }).marker([
                    {position:[first.lat, first.lng]},
                ]).on('click', function (marker) {
                    marker.setIcon('http://maps.google.com/mapfiles/marker_red.png');
                });

                $.each(response, function(index, data){
                    $('#select-outlet').append('<option value="'+data.id+'">'+data.title+'</option>');
                });
                $('#select-outlet').value = first.id;
            }, "json");
        }

        function showError(error) {
            showMapWithoutGeo()
        }
        
        function showMapWithoutGeo() {
            $.get( "{!! url(preg_replace('#/+#','/',  config('app.locale_prefix').'/'.\Lang::get('route.contact',[], \App::getLocale()).'/getdataoutlet')) !!}", function( response ) {
                dataListOutlet = response;

                var first = response[0];
                $('#map').gmap3({
                    center:[first.lat, first.lng],
                    zoom:17
                }).marker([
                    {position:[first.lat, first.lng]},
                ]).on('click', function (marker) {
                    marker.setIcon('http://maps.google.com/mapfiles/marker_red.png');
                });

                $.each(response, function(index, data){
                    $('#select-outlet').append('<option value="'+data.id+'">'+data.title+'</option>');
                });
                $('#select-outlet').value = first.id;
            });
        }

        $( document ).ready(function() {
            getLocation();
        });

        $( "#select-outlet" ).change(function() {
            var k = document.getElementById("select-outlet");
            var outletid = k.options[k.selectedIndex].value;

            var newOutletData = dataListOutlet.filter(function( obj ) {
                return obj.id == outletid;
            });

            $('#map').gmap3({
                action: 'destroy'
            });

            var container = $('#map').parent();
            $('#map').remove();
            container.append('<div id="map"></div>');

            $('#map').gmap3({
                center:[newOutletData[0].lat, newOutletData[0].lng],
                zoom:17
            }).marker([
                {position:[newOutletData[0].lat, newOutletData[0].lng]},
            ]).on('click', function (marker) {
                marker.setIcon('http://maps.google.com/mapfiles/marker_red.png');
            });
        });

        function filterById(jsonObject, id) {
            return jsonObject.filter(function(jsonObject) {
                return (jsonObject['equal_id'] == id);
            })[0];
        }

        var outlets = {!! $outlets->toJson() !!};
        function tampilAddress(outlet) {
            var selectedOutlet = filterById(outlets, outlet);
            var contenaddress = selectedOutlet.address_info;
            document.getElementById("address_info").innerHTML = contenaddress;
        }

        $(document.getElementById("outlet_form")).change(function () {
            var e = document.getElementById("outlet_form");
            var selected = e.options[e.selectedIndex].value;
            tampilAddress(selected);
        });
    </script>

@endsection

@section('page-head-seo')
    <meta name="description" content="{{ $contact->meta_description }}">
    <meta name="keywords" content="{{ $contact->meta_keyword }}">
    <title>{{ $contact->meta_title }} - Bebek Bengil Restaurant</title>
@endsection

@section('conten')
    <!--LOcation Map-->
    <section class="padding">
        <h3 class="hidden">hidden</h3>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div id="map-content">
                        <div id="map"></div>
                    </div>
                    <div class="search_location" style="margin: 20px auto auto;">
                        <select name="select-outlet" class="search" id="select-outlet"></select>
                        <button type="button" class="find-btn"><i class="icon-map"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--Contact Form & Address-->
    <section class="padding bg_grey" id="location">
        <div class="container">
            <div class="row">
                <div class="col-md-7 col-sm-7" style="margin-bottom: 30px;">
                    <h2 class="heading">Get in Touch</h2>
                    <hr class="heading_space">
                    {!! Form::open(['url' => preg_replace('#/+#','/',  config('app.locale_prefix').'/'.\Lang::get('route.contact',[], \App::getLocale())),'method' => 'post', 'class' => 'callus'])!!}
                        <div class="row">
                            <div class="col-md-12">
                                @if(Session::has('notification'))
                                    <div class="alert alert-{{ Session::get('notification.level', 'info') }}" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        {{ Session::get('notification.message') }}
                                    </div>
                                @endif
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
                                    {!! Form::text('name', null, ['placeholder' => \Lang::get('front.ctf-name',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
                                    {!! Form::email('email', null, ['placeholder' => \Lang::get('front.ctf-email',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors->has('phone') ? 'has-error' : '' !!}">
                                    {!! Form::text('phone', null, ['placeholder' => \Lang::get('front.ctf-phone',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                    {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group {!! $errors->has('subject') ? 'has-error' : '' !!}">
                                    {!! Form::text('subject', null, ['placeholder' => \Lang::get('front.ctf-subject',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                    {!! $errors->first('subject', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group {!! $errors->has('outlet_id') ? 'has-error' : '' !!}">
                                    {!! Form::select('outlet_id',['' => 'Select Outlet']+$outlets->pluck('title','equal_id')->all(), null,['class'=>'form-control', 'id' => 'outlet_form']) !!}
                                    {!! $errors->first('outlet_id', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="form-group {!! $errors->has('message') ? 'has-error' : '' !!}">
                                    {!! Form::textarea('message', null, ['placeholder' => \Lang::get('front.ctf-message',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                    {!! $errors->first('message', '<p class="help-block">:message</p>') !!}
                                </div>
                                <div class="form-group">
                                    {{-- {!! app('captcha')->render() !!} --}}
                                    <div class="btn-submit button3">
                                        {!! Form::submit(\Lang::get('front.send-mess',[], \App::getLocale())) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    {!! Form::close() !!}
                </div>

                <div class="col-md-5 col-sm-5 bistro">
                    <h2 class="heading">Bebek Bengil Restaurant</h2>
                    <hr class="heading_space">
                    <div id="address_info">
                        {!! $contact->conten !!}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection