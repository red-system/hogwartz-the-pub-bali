@extends('layouts.mobwebview')

@section('customcss')
    <link type="text/css" href="{{ asset('assets/front/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet"/>
    <style>
        ul.menu_widget li {
            font-size: 12px;
        }
    </style>
@endsection

@section('custom-pluginscript')
    <script type="text/javascript" src="{{ asset('assets/front/js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/front/js/bootstrap-datetimepicker.min.js') }}"></script>

    <script src="{{ asset('assets/front/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('assets/front/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ asset('assets/front/js/revolution.extension.layeranimation.min.js') }}"></script>
    <script src="{{ asset('assets/front/js/revolution.extension.navigation.min.js') }}"></script>
    <script src="{{ asset('assets/front/js/revolution.extension.parallax.min.js') }}"></script>
    <script src="{{ asset('assets/front/js/revolution.extension.slideanims.min.js') }}"></script>
    <script src="{{ asset('assets/front/js/revolution.extension.video.min.js') }}"></script>
    <script src="{{ asset('assets/front/js/slider.js') }}" type="text/javascript"></script>
@endsection

@section('custom-script')
    <script>
        $(document).ready(function () {
            $('#reservation_date').datetimepicker({
                minDate: new Date(),
                format: 'YYYY-MM-DD HH:mm'
            });
        });
    </script>
@endsection

@section('page-head-seo')
    <meta name="description" content="{{ $home->meta_description }}">
    <meta name="keywords" content="{{ $home->meta_keyword }}">
    <title>{{ $home->meta_title }} - Bebek Bengil Restaurant</title>
@endsection

@section('conten')
    <!-- REVOLUTION SLIDER -->
    <div id="rev_slider_34_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container" data-alias="news-gallery34" style="margin:0px auto;background-color:#ffffff;padding:0px;margin-top:0px;margin-bottom:0px;">
        <!-- START REVOLUTION SLIDER 5.0.7 fullwidth mode -->
        <div id="rev_slider_34_1" class="rev_slider fullwidthabanner" style="display:none;" data-version="5.0.7">
            <ul>	<!-- SLIDE  -->
            @foreach($homesliders as $hs)
                @if($hs->slider_type == 'tipe1')
                    <li data-index="rs-129" data-transition="fade" data-responsive_offset="on" data-slotamount="default" data-rotate="0"  data-fstransition="fade" data-fsmasterspeed="1500" data-fsslotamount="7"  data-title="{{ $hs->link_title }}" data-description="{!! $hs->link_text !!}">
                        <!-- MAIN IMAGE -->
                        <img src="{{ asset('assets/front/images/'.$hs->image) }}"  alt=""  data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>

                        <div class="tp-caption"
                           data-x="left" data-hoffset="15"
                           data-y="70"
                           data-transform_idle="o:1;"
                           data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                           data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;"
                           data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                           data-mask_out="x:0;y:0;s:inherit;e:inherit;"
                           data-start="800"
                           style="">{!! $hs->slider_text !!}
                        </div>
                    </li>
                @elseif($hs->slider_type == 'tipe2')
                    <li class="text-center" data-index="rs-130" data-transition="slideleft" data-responsive_offset="on" data-slotamount="default" data-rotate="0"  data-title="{{ $hs->link_title }}" data-description="{!! $hs->link_text !!}">
                        <img src="{{ asset('assets/front/images/'.$hs->image) }}"  alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                        <div class="tp-caption"
                           data-x="center" data-hoffset="15"
                           data-y="70"
                           data-transform_idle="o:1;"
                           data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                           data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;"
                           data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                           data-mask_out="x:0;y:0;s:inherit;e:inherit;"
                           data-start="800"
                           style="z-index: 9;">{!! $hs->slider_text !!}
                        </div>
                    </li>
                @elseif($hs->slider_type == 'tipe3')
                    <li class="text-right" data-index="rs-131" data-transition="slideleft"  data-responsive_offset="on" data-rotate="0" data-title="{{ $hs->link_title }}" data-description="{!! $hs->link_text !!}">
                        <img src="{{ asset('assets/front/images/'.$hs->image) }}" alt="" data-bgposition="center center" data-bgfit="cover" data-bgrepeat="no-repeat" class="rev-slidebg" data-no-retina>
                        <div class="tp-caption"
                           data-x="right" data-hoffset=""
                           data-y="70"
                           data-transform_idle="o:1;"
                           data-transform_in="y:[-100%];z:0;rX:0deg;rY:0;rZ:0;sX:1;sY:1;skX:0;skY:0;s:1500;e:Power3.easeInOut;"
                           data-transform_out="auto:auto;s:1000;e:Power3.easeInOut;"
                           data-mask_in="x:0px;y:0px;s:inherit;e:inherit;"
                           data-mask_out="x:0;y:0;s:inherit;e:inherit;"
                           data-start="800"
                           style="z-index: 9;">{!! $hs->slider_text !!}
                        </div>
                    </li>
                @endif
            @endforeach
                <!-- SLIDE  -->
            </ul>
        </div>
    </div>
    <!-- END REVOLUTION SLIDER -->

    <!--Features Section-->
    <section class="feature_wrap padding-half" id="specialities">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="heading ">{{ \Lang::get('front.our-specialties',[], App::getLocale()) }}</h2>
                    <hr class="heading_space">
                </div>
            </div>

            <?php $no = 1; ?>
            @foreach($specialties as $sp)
                @if($no == 1 || ($no%4 == 1))<div class="row">@endif
                    <div class="col-md-3 col-sm-6 feature text-center">
                        <i><img src="{{ asset('assets/front/images/'.$sp->icon) }}" style="width: 60px;"></i>
                        <h3>{!! $sp->title !!}</h3>
                        <p>{!! $sp->description !!}</p>
                    </div>
                @if(($no%4 == 0) || $specialties->last() == $sp)</div>@endif
                <?php $no++; ?>
            @endforeach
        </div>
    </section>


    <!--Services plus working hours-->
    <section id="services" class="padding-bottom">
        <div class="container">
            <div class="row">
                <div class="col-md-8" style="margin-bottom: 40px;">
                    <h2 class="heading">{{ \Lang::get('front.pcg-rec',[], App::getLocale()) }}</h2>
                    <hr class="heading_space">
                    <div class="slider_wrap">
                        <div id="service-slider" class="owl-carousel">
                            @foreach($packagesrecomended as $pcgr)
                            <div class="item">
                                <div class="item_inner">
                                    <div class="image">
                                        <img src="{{ asset('assets/front/images/'.$pcgr->thumb_image) }}" alt="{{ $pcgr->title }}">
                                    </div>
                                    <h3><a href="{{ url(preg_replace('#/+#','/','mob/'.config('app.locale_prefix').'/'.$pcgr->link.'/'.$pcgr->slug)) }}">{{ $pcgr->title }}</a></h3>
                                    <p>{!! $pcgr->short_description !!}</p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <h2 class="heading">{{ \Lang::get('front.menu-rec',[], App::getLocale()) }}</h2>
                    <hr class="heading_space">
                    <ul class="menu_widget">
                        @foreach($recmenus as $recmn)
                            <li>{{ $recmn->name }} <span>IDR {{ number_format($recmn->price) }}</span></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!-- Order Online -->
    <section id="order-form" class="order_section">
        <div class="container order_form padding">
            <div class="row">
                <div class="col-md-12 appointment_form">
                    <h2 class="heading">{{ \Lang::get('front.reservation',[], App::getLocale()) }}</h2>
                    <hr class="heading_space">
                    <div class="row">
                        <div class="col-md-8">
                            {!! Form::open(['url' => preg_replace('#/+#','/',  config('app.locale_prefix').'/online-order'),'method' => 'post', 'class' => 'callus', 'style' => 'margin-top: 0px;'])!!}
                                <div class="row">
                                    <div class="col-md-12">
                                        @if(Session::has('notification'))
                                            <div class="alert alert-{{ Session::get('notification.level', 'info') }}" role="alert">
                                                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                                {{ Session::get('notification.message') }}
                                            </div>
                                        @endif
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
                                                {!! Form::text('name', null, ['placeholder' => \Lang::get('front.rsf-name',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                                {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
                                                {!! Form::email('email', null, ['placeholder' => \Lang::get('front.rsf-email',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                                {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group {!! $errors->has('phone') ? 'has-error' : '' !!}">
                                            {!! Form::text('phone', null, ['placeholder' => \Lang::get('front.rsf-phone',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                            {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group {!! $errors->has('outlet_id') ? 'has-error' : '' !!}">
                                            {!! Form::select('outlet_id',['' => 'Select Outlet']+App\Outlet::where('lang', App::getLocale())->pluck('title','equal_id')->all(), null,['class'=>'form-control']) !!}
                                            {!! $errors->first('outlet_id', '<p class="help-block">:message</p>') !!}
                                        </div>
                                    </div>
                                    <div class="col-md-12">
                                        <div class="form-group {!! $errors->has('reservation_datetime') ? 'has-error' : '' !!}">
                                            {!! Form::text('reservation_datetime', null, ['placeholder' => \Lang::get('front.rsf-date',[], \App::getLocale()), 'class' => 'form-control', 'id' => 'reservation_date']) !!}
                                            {!! $errors->first('reservation_datetime', '<p class="help-block">:message</p>') !!}
                                        </div>

                                        <div class="form-group {!! $errors->has('remark') ? 'has-error' : '' !!}">
                                            {!! Form::textarea('remark', null, ['placeholder' => \Lang::get('front.rsf-remark',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                            {!! $errors->first('remark', '<p class="help-block">:message</p>') !!}
                                        </div>

                                        <div class="form-group">
                                            {{-- {!! app('captcha')->render() !!} --}}
                                            <div class="btn-submit button3">
                                                {!! Form::submit(\Lang::get('front.send-reservation',[], \App::getLocale())) !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                </div>
            </div>
            <div class="col-md-3"></div>
        </div>
    </section>

    <section id="testinomial" class="padding">
        <div class="container">
            <div class="row">
                <div class="col-md-offset-1 col-md-10 col-sm-12 text-center">
                    <h2 class="heading">{{ \Lang::get('front.bebekbengil-profile',[], App::getLocale()) }}</h2>
                    <hr class="heading_space" style="margin-bottom: 30px;">
                </div>
            </div>
            <div class="row">
                <div class="col-md-offset-1 col-md-10 col-sm-12">
                    <div class="video-container">
                        <iframe width="854" height="480" src="{!! $videoprofile->link !!}" frameborder="0" allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--Featured Receipes -->
    <section id="news" class="bg_grey padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h2 class="heading">{{ \Lang::get('front.recent-news',[], App::getLocale()) }}</h2>
                    <hr class="heading_space">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="cheffs_wrap_slider">
                        <div id="news-slider" class="owl-carousel">
                            @foreach($recentnews as $rnews)
                            <div class="item">
                                <div class="news_content">
                                    <img src="{{ asset('assets/front/images/'.$rnews->image) }}" alt="{{ $rnews->title }}">
                                    <div class="date_comment">
                                        <span>{{ date('d', strtotime($rnews->created_at)) }}<small>{{ date('M', strtotime($rnews->created_at)) }}</small></span>
                                    </div>
                                    <div class="comment_text">
                                        <h3><a href="{{ url(preg_replace('#/+#','/', 'mob/'.config('app.locale_prefix').'/'.\Lang::get('route.news',[], config('app.locale_prefix')).'/'.$rnews->slug)) }}">{{ $rnews->title }}</a></h3>
                                        <div style="margin-left: 80px;">
                                            <p>{{ substr(strip_tags($rnews->description),0,145).'...' }}</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection