@extends('layouts.mobwebview')

@section('page-head-seo')
    <meta name="description" content="{{ $articlemenu->meta_description }}">
    <meta name="keywords" content="{{ $articlemenu->meta_keyword }}">
    <title>{{ $articlemenu->meta_title }} - Bebek Bengil Restaurant</title>
@endsection

@section('conten')
    <section id="food" class="padding bg_grey">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="heading">{{ \Lang::get('front.our-menu',[], \App::getLocale()) }}</h2>
                    <hr class="heading_space">
                </div>
            </div>
            <div class="row">
                <div class="col-md-12" id="gallery">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="work-filter">
                                <ul class="text-center">
                                    <li><a href="javascript:;" data-filter="all" class="active filter">All</a></li>
                                    @foreach($menucategories as $mcat)
                                        <li><a href="javascript:;" data-filter="{{ '.'.str_slug($mcat->category) }}" class="filter"> {{ $mcat->category }}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="zerogrid">
                            <div class="wrap-container">
                                <div class="wrap-content clearfix">
                                    @foreach($allmenus as $am)
                                        <div class="col-1-4 mix work-item @foreach($am->categories()->where('type', 'menu')->get() as $oct) {{ str_slug($oct->category).' ' }} @endforeach heading_space">
                                            <div class="wrap-col">
                                                <div class="item-container">
                                                    <div class="image">
                                                        <img src="{{ asset('assets/front/images/'.$am->image) }}" alt="{{ $am->image }}"/>
                                                        <a href="#" data-toggle="modal" data-target="{{ '#dmenu'.$am->id }}">
                                                            <div class="overlay">
                                                                <i class=" icon-eye6"></i>
                                                            </div>
                                                        </a>
                                                    </div>
                                                    <div class="gallery_content" style="height: 140px;">
                                                        <h3 style="margin-top: 25px;"><a href="#" data-toggle="modal" data-target="{{ '#dmenu'.$am->id }}">{{ $am->name }}</a></h3>
                                                        <p style="margin-top: 10px;">{{ $am->short_description }}</p>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>
                    </div>

                    @if($drinkcategories->count() == 0)
                        <div class="alert alert-info">
                            <div class="row">
                                <div class="col-md-10">{!! $taxinfo->value !!}</div>
                                <div class="col-md-2"><a href="{{ url(preg_replace('#/+#','/', 'mob/'.config('app.locale_prefix').'/'.\Lang::get('route.contact',[], App::getLocale()))) }}" class="btn-common btn-block" style="border-radius: 0px; text-align: center; border: none; padding: 12px; margin: 0px;">Order Now</a></div>
                            </div>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </section>

    @if($drinkcategories->count() > 0)
    <section id="pricing" class="padding">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="heading">{{ \Lang::get('front.drink',[], \App::getLocale()) }}</h2>
                    <hr class="heading_space" style="margin-bottom: 40px;">
                </div>
            </div>

            <div class="row" style="margin-bottom:15px;">
                <div class="col-md-12">
                    {!! $articlemenu->additional_conten !!}
                </div>
            </div>

            <?php $no = 1; ?>
            @foreach($drinkcategories as $dcat)
                @if($no == 1 || ($no%2 == 1))<div class="row">@endif
                <div class="col-sm-6">
                    <div style="margin-bottom: 20px;">
                        <h3>{{ $dcat->category }}</h3>
                    </div>
                    <?php ${'drink_'.$dcat->id} = $dcat->menus()->where('type', 'drink')->where('lang', App::getLocale())->get(); ?>
                    <div class="price padding-bottom">
                        <div class="price_body">
                            <ul class="pricing_feature">
                                @foreach(${'drink_'.$dcat->id} as $drk)
                                    <li>{{ $drk->name }} <strong>IDR {{ number_format($drk->price) }}</strong></li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
                @if(($no%2 == 0) || $drinkcategories->last() == $dcat)</div>@endif
                <?php $no++; ?>
            @endforeach

            <div class="alert alert-info">
                <div class="row">
                    <div class="col-md-10">{!! $taxinfo->value !!}</div>
                    <div class="col-md-2"><a href="{{ url(preg_replace('#/+#','/', config('app.locale_prefix').'/'.\Lang::get('route.contact',[], App::getLocale()))) }}" class="btn-common btn-block" style="border-radius: 0px; text-align: center; border: none; padding: 12px; margin: 0px;">Order Now</a></div>
                </div>
            </div>
        </div>
    </section>
    @endif

    @foreach($allmenus as $amm)
        <div class="modal fade" id="{{ 'dmenu'.$amm->id }}" tabindex="-1" role="dialog" aria-labelledby="{{ 'dmenu'.$amm->id.'Label' }}" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-notify modal-info" role="dialog">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #a95892;color: #fff;">
                        <h3 class="modal-title" style="display: inline;">Menu Detail</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: 5px;">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="thumbnail" style="border: none; border-radius: 0px;">
                                    <img src="{{ asset('assets/front/images/'.$amm->image) }}" alt="{{ $amm->image }}" width="100%">
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="">
                                    <div class="row form-group">
                                        <label for="name" class="col-md-4">Name</label>
                                        <div class="col-md-8">
                                            {{ $amm->name }}
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label for="price" class="col-md-4">Price</label>
                                        <div class="col-md-8">
                                            IDR {{ number_format($amm->price) }}
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label for="description" class="col-md-4">Description</label>
                                        <div class="col-md-8">
                                            {!! $amm->description !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection