@extends('layouts.mobwebview')

@section('custom-pluginscript')
    <script src="{{ asset('assets/front/js/jquery.parallax-1.1.3.js') }}"></script>
    <script src="{{ asset('assets/front/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('assets/front/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ asset('assets/front/js/slider.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/front/js/jquery.mixitup.min.js') }}"></script>
@endsection

@section('page-head-seo')
    <title>{{ \Lang::get('front.news-archive-title',[], App::getLocale()) }} {{ date('F Y', strtotime($thbln)) }} - Bebek Bengil Restaurant</title>
@endsection

@section('conten')
    <!-- Blogs -->
    <section id="blog" class="padding-top">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-7">
                    @foreach($newsarchive as $news)
                        <div class="blog_item padding-bottom">
                            <h2>{{ $news->title }}</h2>
                            <ul class="comments">
                                <li><a href="{{ url(preg_replace('#/+#','/', 'mob/'.config('app.locale_prefix').'/'.\Lang::get('route.news',[], config('app.locale_prefix')).'/'.$news->slug)) }}">{{ date('M d, Y', strtotime($news->created_at)) }}</a></li>
                            </ul>
                            <div class="image_container">
                                <img src="{{ asset('assets/front/images/'.$news->image) }}" class="img-responsive" alt="{{ $news->title }}">
                            </div>
                            <p>{{ substr(strip_tags($news->description),0,250).'...' }}</p>
                            <a class="btn-common button3" href="{{ url(preg_replace('#/+#','/', 'mob/'.config('app.locale_prefix').'/'.\Lang::get('route.news',[], config('app.locale_prefix')).'/'.$news->slug)) }}">{{ \Lang::get('front.bt-readmore',[], App::getLocale()) }}</a>
                        </div>
                    @endforeach

                    <hr>
                    {{ $newsarchive->links() }}
                </div>
                <div class="col-md-4 col-sm-5">
                    @include('webview._sidebarnews')
                </div>
            </div>
        </div>
    </section>
@endsection