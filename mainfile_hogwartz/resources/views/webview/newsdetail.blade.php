@extends('layouts.mobwebview')

@section('customcss')
    <style type="text/css">
        #share-buttons img {
            width: 35px;
            padding: 5px;
            border: 0;
            box-shadow: 0;
            display: inline;
        }

        @media (max-width:480px){
            .clearfix > .pull-left {
                float: none !important;
                text-align: center;
            }

            .clearfix > .pull-right {
                float: none !important;
                text-align: center;
            }

            #blog .blog_item ul.comments li {
                margin-bottom: 10px;
            }
        }

    </style>
@endsection

@section('custom-pluginscript')
    <script src="{{ asset('assets/front/js/jquery.parallax-1.1.3.js') }}"></script>
    <script src="{{ asset('assets/front/js/jquery.themepunch.tools.min.js') }}"></script>
    <script src="{{ asset('assets/front/js/jquery.themepunch.revolution.min.js') }}"></script>
    <script src="{{ asset('assets/front/js/slider.js') }}" type="text/javascript"></script>
    <script src="{{ asset('assets/front/js/jquery.mixitup.min.js') }}"></script>
@endsection

@section('custom-script')
    <script id="dsq-count-scr" src="//bebek-bengil-restaurant.disqus.com/count.js" async></script>
@endsection

@section('page-head-seo')
    <meta name="description" content="{{ $news->meta_description }}">
    <meta name="keywords" content="{{ $news->meta_keyword }}">
    <title>{{ $news->meta_title }} - Bebek Bengil Restaurant</title>
@endsection

@section('conten')
    <!-- Blog Details -->
    <section id="blog" class="padding-top">
        <div class="container">
            <div class="row">
                <div class="col-md-8 col-sm-7">
                    <div class="blog_item padding-bottom order-page">
                        <h2>{{ $news->title }}</h2>
                        <ul class="comments">
                            <li>{{ date('M d, Y', strtotime($news->created_at)) }}</li>
                        </ul>
                        <div class="image_container">
                            <img src="{{ asset('assets/front/images/'.$news->image) }}" class="img-responsive" alt="{{ $news->title }}">
                        </div>
                        <p>{!! $news->description !!}</p>
                        <div class="clearfix">
                            <ul class="comments pull-left">
                                <li>
                                    Category:
                                    @foreach($cats as $cat)
                                        @if($cats->last() == $cat)
                                            {{ $cat->category }}
                                        @else
                                            {{ $cat->category.', ' }}
                                        @endif
                                    @endforeach

                                </li>
                                <li>
                                    Tag:
                                    @foreach($tags as $tag)
                                        @if($tags->last() == $tag)
                                            {{ $tag->tag}}
                                        @else
                                            {{ $tag->tag.', ' }}
                                        @endif
                                    @endforeach
                                </li>
                            </ul>
                            <ul class="social_icon pull-right">
                                <li class="black"><a href="https://www.facebook.com/sharer.php?u={{ url(preg_replace('#/+#','/', config('app.locale_prefix').'/'.\Lang::get('route.news',[], config('app.locale_prefix')).'/'.$news->slug)) }}" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                <li class="black"><a href="https://twitter.com/share?url={{ url(preg_replace('#/+#','/', config('app.locale_prefix').'/'.\Lang::get('route.news',[], config('app.locale_prefix')).'/'.$news->slug)) }}&amp;text={{ $news->title.' - Bebek Bengil Restaurant' }}" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                <li class="black"><a href="https://plus.google.com/share?url={{ url(preg_replace('#/+#','/', config('app.locale_prefix').'/'.\Lang::get('route.news',[], config('app.locale_prefix')).'/'.$news->slug)) }}" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                <li class="black"><a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                            </ul>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div id="disqus_thread"></div>
                                <script>
                                    (function() { // DON'T EDIT BELOW THIS LINE
                                        var d = document, s = d.createElement('script');
                                        s.src = 'https://bebek-bengil-restaurant.disqus.com/embed.js';
                                        s.setAttribute('data-timestamp', +new Date());
                                        (d.head || d.body).appendChild(s);
                                    })();
                                </script>
                                <noscript>Please enable JavaScript to view the <a href="https://disqus.com/?ref_noscript">comments powered by Disqus.</a></noscript>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-5">
                    @include('webview._sidebarnews')
                </div>
            </div>
        </div>
    </section>
@endsection