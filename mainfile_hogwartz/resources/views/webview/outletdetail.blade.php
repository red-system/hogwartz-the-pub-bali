@extends('layouts.mobwebview')

@section('customcss')
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/front/wowslider/style.css') }}">
    <link type="text/css" href="{{ asset('assets/front/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet"/>
    <style>
        ul.menu_widget li {
            font-size: 14px;
        }

        .work-filter li a {
            font-size: 12px;
        }

        #gallery .gallery_content h3 {
            font-size: 14px;
        }

        .heading_space {
            margin-bottom: 30px;
        }

        .work-filter {
            margin-bottom: 30px;
        }

    </style>
@endsection

@section('custom-pluginscript')
    <script type="text/javascript" src="{{ asset('assets/front/js/moment.min.js') }}"></script>
    <script type="text/javascript" src="{{ asset('assets/front/js/bootstrap-datetimepicker.min.js') }}"></script>
    <script src="http://maps.googleapis.com/maps/api/js?key=AIzaSyAOBKD6V47-g_3opmidcmFapb3kSNAR70U&libraries=places"></script>
    <script src="{{ asset('assets/front/js/gmap3.js') }}"></script>
    <script>
        $('#map')
            .gmap3({
                center:[{{ $outlet->lat }}, {{ $outlet->lng }}],
                zoom:17
            })
            .marker([
                {position:[{{ $outlet->lat }}, {{ $outlet->lng }}]},
            ])
            .on('click', function (marker) {
                marker.setIcon('http://maps.google.com/mapfiles/marker_red.png');
            });

        @if(!$errors->isEmpty())
            $(document).ready(function(){
                $('#reservationModal').modal('show');
            });
        @endif

    </script>

    <script src="{{ asset('assets/front/wowslider/wowslider.js') }}"></script>
    <script src="{{ asset('assets/front/wowslider/script.js') }}"></script>
@endsection

@section('custom-script')
    <script>
        $(document).ready(function () {
            $('#reservation_date').datetimepicker({
                minDate: new Date(),
                format: 'YYYY-MM-DD HH:mm'
            });
        });
    </script>
@endsection

@section('page-head-seo')
    <meta name="description" content="{{ $outlet->meta_description }}">
    <meta name="keywords" content="{{ $outlet->meta_keyword }}">
    <title>{{ $outlet->meta_title }} - Bebek Bengil Restaurant</title>
@endsection

@section('conten')
    <!-- Blog Details -->
    <section id="blog" class="padding-top">
        <div class="container">
            <div class="row">
                <div class="col-md-9 col-sm-8">
                    <div class="blog_item padding-bottom order-page">
                        <h2>{{ $outlet->title }}</h2>
                        <div class="row">
                            <div class="col-md-12">
                                @if(Session::has('notification'))
                                    <div class="alert alert-{{ Session::get('notification.level', 'info') }}" role="alert" style="margin-bottom: 15px;">
                                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                        {{ Session::get('notification.message') }}
                                    </div>
                                @endif
                            </div>
                        </div>
                        @if($sliders->count() > 0)
                        <div id="wowslider-container1" style="margin-top: 15px;">
                            <?php $noid = 0; ?>
                            <div class="ws_images"><ul>
                                @foreach($sliders as $slider)
                                    <li><img src="{{ asset('assets/front/images/'.$slider->image) }}" alt="{{ $slider->name }}" title="" id="{{ 'wows1_'.$noid }}"/></li>
                                    <?php $noid++; ?>
                                @endforeach
                                </ul></div>
                            <div class="ws_bullets"><div>
                                @for($i = 1; $i <= $sliders->count(); $i++)
                                    <a href="#"><span>{{ $i }}</span></a>
                                @endfor
                            </div></div>
                            <div class="ws_shadow"></div>
                        </div>
                        @endif
                        <p>{!! $outlet->description !!}</p>

                        <div id="food">
                            <div class="row" style="margin-top: 60px;">
                                <div class="col-md-12">
                                    <h2 class="heading" style="font-size: 24px;">Menu</h2>
                                    <hr class="heading_space">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12" id="gallery">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <div class="work-filter">
                                                <ul class="text-center">
                                                    <li><a href="javascript:;" data-filter="all" class="active filter">All</a></li>
                                                    @foreach($menucategories as $mcat)
                                                        <li><a href="javascript:;" data-filter="{{ '.'.str_slug($mcat->category) }}" class="filter" style="font-size: 15px;"> {{ $mcat->category }}</a></li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="zerogrid">
                                            <div class="wrap-container">
                                                <div class="wrap-content clearfix">
                                                    @foreach($outletmenu->where('type', 'food') as $om)
                                                    <div class="col-1-3 mix work-item @foreach($om->categories()->where('type', 'menu')->get() as $oct) {{ str_slug($oct->category).' ' }} @endforeach heading_space">
                                                        <div class="wrap-col">
                                                            <div class="item-container">

                                                                <div class="image">
                                                                    <img src="{{ asset('assets/front/images/'.$om->image) }}" alt="{{ $om->image }}"/>
                                                                    <a href="#" data-toggle="modal" data-target="{{ '#dmenu'.$om->id }}">
                                                                    <div class="overlay">
                                                                        <i class=" icon-eye6"></i>
                                                                    </div>
                                                                    </a>
                                                                </div>

                                                                <div class="gallery_content" style="height: 120px;">
                                                                    <h3 style="margin-top: 25px;"><a href="#" data-toggle="modal" data-target="{{ '#dmenu'.$om->id }}">{{ $om->name }}</a></h3>
                                                                    <p style="margin-top: 10px;">{{ $om->description }}</p>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    @endforeach
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        @if($drinkcategories->count() > 0)
                        <div id="pricing" class="padding">
                            <div class="row" style="margin-top: 60px;">
                                <div class="col-md-12">
                                    <h2 class="heading">{{ \Lang::get('front.drink',[], \App::getLocale()) }}</h2>
                                    <hr class="heading_space" style="margin-bottom: 40px; margin-top: 0px;">
                                </div>
                            </div>

                            <?php $no = 1; ?>
                            @foreach($drinkcategories as $dcat)
                                @if($no == 1 || ($no%2 == 1))<div class="row">@endif
                                    <div class="col-sm-6">
                                        <div style="margin-bottom: 20px;">
                                            <h3 style="margin-top: 0px;">{{ $dcat->category }}</h3>
                                        </div>
                                        <?php ${'drink_'.$dcat->id} = $dcat->menus()->where('type', 'drink')->where('lang', App::getLocale())->get(); ?>
                                        <div class="price padding-bottom">
                                            <div class="price_body">
                                                <ul class="pricing_feature">
                                                    @foreach(${'drink_'.$dcat->id} as $drk)
                                                        <li>{{ $drk->name }} <strong>IDR {{ number_format($drk->price) }}</strong></li>
                                                    @endforeach
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    @if(($no%2 == 0) || $drinkcategories->last() == $dcat)</div>@endif
                                <?php $no++; ?>
                            @endforeach
                        </div>
                        @endif

                        <div class="alert alert-info">
                            <div class="row">
                                <div class="col-md-10" style="margin-bottom: 10px;">{!! $taxinfo->value !!}</div>
                                <div class="col-md-2"><button type="button" class="btn-common btn-block" data-toggle="modal" data-target="#reservationModal" style="border-radius: 0px; text-align: center; border: none; padding: 12px; margin: 0px;">Order Now</button></div>
                            </div>

                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-4">
                    <button type="button" class="btn-common btn-block" data-toggle="modal" data-target="#reservationModal" style="border-radius: 0px; text-align: center; border: none;">Reservation</button>
                    <div id="map" style="margin-top: 20px;"></div>
                    <div style="margin-top: 20px; margin-bottom: 30px;">
                        {!! $outlet->address_info !!}
                    </div>
                </div>
            </div>

            @if($otheroutlets->count() > 0)
            <div class="row padding-bottom" id="services">
                <div class="col-md-12 col-sm-12">
                    <h2 class="heading">{{ \Lang::get('front.other-outlet',[], App::getLocale()) }}</h2>
                    <hr class="heading_space" style="margin-bottom: 20px;">
                    <div class="slider_wrap">
                        <div id="outlet-slider" class="owl-carousel">
                            @foreach($otheroutlets as $otheroutlet)
                            <div class="item">
                                <div class="item_inner">
                                    <div class="image">
                                        <a href="{{ url(preg_replace('#/+#','/', 'mob/'.config('app.locale_prefix').'/'.\Lang::get('route.outlet',[], App::getLocale()).'/'.$otheroutlet->slug)) }}"><img src="{{ asset('assets/front/images/'.$otheroutlet->image) }}" alt="{{ $otheroutlet->title }}"></a>
                                    </div>
                                    <h3><a href="{{ url(preg_replace('#/+#','/', 'mob/'.config('app.locale_prefix').'/'.\Lang::get('route.outlet',[], App::getLocale()).'/'.$otheroutlet->slug)) }}">{{ $otheroutlet->title }}</a></h3>
                                    <p>{{ substr(strip_tags($otheroutlet->description),0,125).'...' }}</p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
    </section>

    <div class="modal fade" id="reservationModal" tabindex="-1" role="dialog" aria-labelledby="reservationModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-notify modal-info" role="document">
            <div class="modal-content">
                <div class="modal-header" style="background-color: #a95892;color: #fff;">
                    <h3 class="modal-title" id="reservationModalLabel" style="display: inline;">Reservation Form</h3>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: 5px;">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    {!! Form::open(['url' => preg_replace('#/+#','/',  config('app.locale_prefix').'/'.\Lang::get('route.outlet',[], App::getLocale()).'/onlineorder'),'method' => 'post', 'class' => 'callus', 'style' => 'margin-top: 0px;'])!!}
                    <div class="row">
                        <div class="col-md-12">
                            @if(Session::has('notification'))
                                <div class="alert alert-{{ Session::get('notification.level', 'info') }}" role="alert">
                                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                                    {{ Session::get('notification.message') }}
                                </div>
                            @endif
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-group {!! $errors->has('name') ? 'has-error' : '' !!}">
                                    {!! Form::text('name', null, ['placeholder' => \Lang::get('front.rsf-name',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                    {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <div class="form-group {!! $errors->has('email') ? 'has-error' : '' !!}">
                                    {!! Form::email('email', null, ['placeholder' => \Lang::get('front.rsf-email',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors->has('phone') ? 'has-error' : '' !!}">
                                {!! Form::text('phone', null, ['placeholder' => \Lang::get('front.rsf-phone',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                {!! $errors->first('phone', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group {!! $errors->has('outlet') ? 'has-error' : '' !!}">
                                {!! Form::select('outlet',[$outlet->equal_id => $outlet->title], $outlet->equal_id,['class'=>'form-control', 'disabled' => 'disabled']) !!}
                                {!! $errors->first('outlet', '<p class="help-block">:message</p>') !!}
                            </div>
                        </div>
                        <input type="hidden" name="outlet_id" value="{{ $outlet->equal_id }}">
                        <div class="col-md-12">
                            <div class="form-group {!! $errors->has('reservation_datetime') ? 'has-error' : '' !!}">
                                {!! Form::text('reservation_datetime', null, ['placeholder' => \Lang::get('front.rsf-date',[], \App::getLocale()), 'class' => 'form-control', 'id' => 'reservation_date']) !!}
                                {!! $errors->first('reservation_datetime', '<p class="help-block">:message</p>') !!}
                            </div>

                            <div class="form-group {!! $errors->has('remark') ? 'has-error' : '' !!}">
                                {!! Form::textarea('remark', null, ['placeholder' => \Lang::get('front.rsf-remark',[], \App::getLocale()), 'class' => 'form-control']) !!}
                                {!! $errors->first('remark', '<p class="help-block">:message</p>') !!}
                            </div>

                            <div class="form-group">
                                {{-- {!! app('captcha')->render() !!} --}}
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <div class="btn-submit button3" style="padding: 5px;">
                        {!! Form::submit(\Lang::get('front.send-reservation',[], \App::getLocale()),['class' => 'btn btn-default', 'style' => 'background-color: #a95892;border-color: #a95892; color: #fff;']) !!}
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>

    @foreach($outletmenu->where('type', 'food') as $om)
        <div class="modal fade" id="{{ 'dmenu'.$om->id }}" tabindex="-1" role="dialog" aria-labelledby="{{ 'dmenu'.$om->id.'Label' }}" aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-notify modal-info" role="dialog">
                <div class="modal-content">
                    <div class="modal-header" style="background-color: #a95892;color: #fff;">
                        <h3 class="modal-title" style="display: inline;">Menu Detail</h3>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: 5px;">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-5">
                                <div class="thumbnail" style="border: none; border-radius: 0px;">
                                    <img src="{{ asset('assets/front/images/'.$om->image) }}" alt="{{ $om->image }}" width="100%">
                                </div>
                            </div>
                            <div class="col-md-7">
                                <div class="">
                                    <div class="row form-group">
                                        <label for="name" class="col-md-4">Name</label>
                                        <div class="col-md-8">
                                            {{ $om->name }}
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label for="price" class="col-md-4">Price</label>
                                        <div class="col-md-8">
                                            IDR {{ number_format($om->price) }}
                                        </div>
                                    </div>
                                    <div class="row form-group">
                                        <label for="description" class="col-md-4">Description</label>
                                        <div class="col-md-8">
                                            {!! $om->description !!}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection