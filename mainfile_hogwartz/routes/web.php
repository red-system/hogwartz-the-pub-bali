<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('logout', 'Auth\LoginController@logout')->name('logout');
Route::group(['middleware' => 'guest'], function () {
    Route::get('login', 'Auth\LoginController@showLoginForm')->name('login');
    Route::post('login', 'Auth\LoginController@login');
    Route::get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail')->name('password.email');
    Route::get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm')->name('reset');
    Route::post('password/reset', 'Auth\ResetPasswordController@reset');
});

Route::get('/', 'FHomeController@index')->name('front.index');
$all_langs = config('app.all_langs');
foreach ($all_langs as $prefix) {
    Route::group(['prefix' => $prefix, 'middleware' => 'configcookieslang'], function () use ($prefix) {
        Route::get('/', 'FIndexController@index');
        Route::get(\Lang::get('route.about', [], $prefix), 'FAboutController@index');
        Route::get(\Lang::get('route.confirmation', [], $prefix), 'FConfirmationController@index');
        Route::get(\Lang::get('route.reservation', [], $prefix), 'FReservationController@index');
        Route::get(\Lang::get('route.schedule', [], $prefix), 'FScheduleController@index');
        Route::get(\Lang::get('route.schedule', [], $prefix) . '/{slug}', 'FScheduleController@detail');
        Route::get(\Lang::get('route.outlet', [], $prefix), 'FOutletController@index');
        Route::post(\Lang::get('route.outlet', [], $prefix) . '/onlineorder', 'FOutletController@onlineorder');
        Route::get(\Lang::get('route.outlet', [], $prefix) . '/{slug}', 'FOutletController@detail');
        Route::get(\Lang::get('route.package', [], $prefix) . '/{slug}', 'FPackageController@index');
        Route::get(\Lang::get('route.menu', [], $prefix), 'FMenusController@index');
        Route::get(\Lang::get('route.news', [], $prefix), 'FNewsFeedController@index');
        Route::get(\Lang::get('route.news', [], $prefix) . '/{slug}', 'FNewsFeedController@detail');
        Route::get(\Lang::get('route.news', [], $prefix) . '/category/{slug}', 'FNewsFeedController@category');
        Route::get(\Lang::get('route.news', [], $prefix) . '/tag/{slug}', 'FNewsFeedController@tag');
        Route::get(\Lang::get('route.news', [], $prefix) . '/archive/{thbln}', 'FNewsFeedController@archive');
        Route::get(\Lang::get('route.sistercompany', [], $prefix), 'FSisterCompanyController@index');
        Route::get(\Lang::get('route.contact', [], $prefix), 'FContactController@index');
        Route::post(\Lang::get('route.contact', [], $prefix), 'FContactController@store');
        Route::post(\Lang::get('route.contact', [], $prefix) . '/getdataoutlet', 'FContactController@outlet');
        Route::get(\Lang::get('route.contact', [], $prefix) . '/getdataoutlet', 'FContactController@outletwithoutloc');
        Route::post('online-order', 'FOnlineOrderController@store');
        Route::post('online-order-form', 'FOnlineOrderController@storeReservation');
        Route::post('confirmation-send', 'FConfirmationController@store');
        Route::post('confirmation-send', 'FConfirmationController@store');
        Route::get(\Lang::get('route.gallery', [], $prefix), 'FGalleryController@index');
        Route::get(\Lang::get('route.gallery', [], $prefix) . '/{slug}', 'FGalleryController@detail');
    });
}
Route::get('homeslider', 'FIndexController@homeslider');
Route::get('sitemap.xml', 'FSiteMapController@siteMapRobot');
Route::get('test-email', "FOnlineOrderController@tryEmail");

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get('/', ['as' => 'admin.index', 'uses' => 'HomeController@index']);
    Route::resource('email', 'BEmailController', ['except' => 'show']);
    Route::resource('social-link', 'BSocialController', ['except' => 'show']);
    Route::resource('user', 'BAdminController', ['only' => [
        'index', 'create', 'store', 'destroy'
    ]]);
    Route::post('contact-messages/confirm', ['as' => 'contact-messages.confirm', 'uses' => 'BContactController@confirm']);
    Route::resource('contact-messages', 'BContactController', ['only' => [
        'index', 'destroy'
    ]]);
    Route::get('changepassword', 'BUserController@password');
    Route::post('changepassword', 'BUserController@updatepassword');
    Route::get('menu-utama', ['as' => 'menu-utama.index', 'uses' => 'BMenuUtamaController@index']);
    Route::get('menu-utama/{id}/edit', ['as' => 'menu-utama.edit', 'uses' => 'BMenuUtamaController@edit']);
    Route::patch('menu-utama/{id}', ['as' => 'menu-utama.update', 'uses' => 'BMenuUtamaController@update']);

    Route::get('footer/{id}/edit', ['as' => 'footer.edit', 'uses' => 'BFooterController@edit']);
    Route::patch('footer/{id}', ['as' => 'footer.update', 'uses' => 'BFooterController@update']);

    Route::get('online-order', ['as' => 'onlineorder.index', 'uses' => 'BOnlineOrderController@index']);
    Route::get('online-order/{id}/edit', ['as' => 'onlineorder.edit', 'uses' => 'BOnlineOrderController@edit']);
    Route::patch('online-order/{id}', ['as' => 'onlineorder.update', 'uses' => 'BOnlineOrderController@update']);
    Route::delete('online-order/{id}', ['as' => 'onlineorder.destroy', 'uses' => 'BOnlineOrderController@destroy']);

    Route::get('web-config', ['as' => 'web-config.index', 'uses' => 'BWebConfigController@index']);
    Route::patch('web-config', ['as' => 'web-config.update', 'uses' => 'BWebConfigController@update']);

    Route::get('filemanager', ['as' => 'filemanager.index', 'uses' => 'BWebConfigController@filemanager']);

    //homeslider
    Route::get('config/homeslider', ['as' => 'admin.config.homeslider.index', 'uses' => 'BHomeSliderController@index']);
    Route::get('config/homeslider/create', ['as' => 'admin.config.homeslider.create', 'uses' => 'BHomeSliderController@create']);
    Route::post('config/homeslider', ['as' => 'admin.config.homeslider.store', 'uses' => 'BHomeSliderController@store']);
    Route::get('config/homeslider/{id}/edit', ['as' => 'admin.config.homeslider.edit', 'uses' => 'BHomeSliderController@edit']);
    Route::patch('config/homeslider/{id}', ['as' => 'admin.config.homeslider.update', 'uses' => 'BHomeSliderController@update']);
    Route::post('config/homeslider/destroy', ['as' => 'admin.config.homeslider.destroy', 'uses' => 'BHomeSliderController@destroy']);

    //homekonten
    Route::get('config/home-konten', ['as' => 'admin.config.home-konten.index', 'uses' => 'BHomeKontenController@index']);
    //specialities
    Route::get('config/home-konten/specialities/create', ['as' => 'specialities.create', 'uses' => 'BSpecialitiesController@create']);
    Route::post('config/home-konten/specialities', ['as' => 'specialities.store', 'uses' => 'BSpecialitiesController@store']);
    Route::get('config/home-konten/specialities/{id}/edit', ['as' => 'specialities.edit', 'uses' => 'BSpecialitiesController@edit']);
    Route::patch('config/home-konten/specialities/{id}', ['as' => 'specialities.update', 'uses' => 'BSpecialitiesController@update']);
    Route::delete('config/home-konten/specialities/{equal_id}', ['as' => 'specialities.destroy', 'uses' => 'BSpecialitiesController@destroy']);

    Route::patch('discover/{id}', ['as' => 'discover.update', 'uses' => 'BHomeKontenController@updateDiscover']);

    Route::patch('config/dsimage', ['as' => 'dsimage.update', 'uses' => 'BHomeKontenController@updateDsImage']);

    //Services
    Route::get('config/home-konten/services/create', ['as' => 'services.create', 'uses' => 'BServiceController@create']);
    Route::post('config/home-konten/services', ['as' => 'services.store', 'uses' => 'BServiceController@store']);
    Route::get('config/home-konten/services/{id}/edit', ['as' => 'services.edit', 'uses' => 'BServiceController@edit']);
    Route::patch('config/home-konten/services/{id}', ['as' => 'services.update', 'uses' => 'BServiceController@update']);
    Route::delete('config/home-konten/services/{equal_id}', ['as' => 'services.destroy', 'uses' => 'BServiceController@destroy']);

    //Testimonial
    Route::get('config/home-konten/testimonial/create', ['as' => 'testimonial.create', 'uses' => 'BTestimonialController@create']);
    Route::post('config/home-konten/testimonial', ['as' => 'testimonial.store', 'uses' => 'BTestimonialController@store']);
    Route::get('config/home-konten/testimonial/{id}/edit', ['as' => 'testimonial.edit', 'uses' => 'BTestimonialController@edit']);
    Route::patch('config/home-konten/testimonial/{id}', ['as' => 'testimonial.update', 'uses' => 'BTestimonialController@update']);
    Route::delete('config/home-konten/testimonial/{equal_id}', ['as' => 'testimonial.destroy', 'uses' => 'BTestimonialController@destroy']);

    //package recomended
    Route::get('config/packages/createRecommend', ['as' => 'packages.createRecommend', 'uses' => 'BPackagesController@createRecommend']);
    Route::post('config/packages/updateRecommend', ['as' => 'packages.updateRecommend', 'uses' => 'BPackagesController@updateRecommend']);
    Route::delete('config/packages/destroyRecommend/{id}', ['as' => 'packages.destroyRecommend', 'uses' => 'BPackagesController@destroyRecommend']);

    //menus recomended
    Route::get('config/menus/createRecommend', ['as' => 'menus.createRecommend', 'uses' => 'BMenusController@createRecommend']);
    Route::post('config/menus/updateRecommend', ['as' => 'menus.updateRecommend', 'uses' => 'BMenusController@updateRecommend']);
    Route::delete('config/menus/destroyRecommend/{id}', ['as' => 'menus.destroyRecommend', 'uses' => 'BMenusController@destroyRecommend']);

    //video
    Route::patch('config/home-konten/video/{id}', ['as' => 'videos.update', 'uses' => 'BVideoController@update']);

    //about us konten
    Route::get('config/about', ['as' => 'admin.config.about.index', 'uses' => 'BAboutUsController@index']);
    Route::get('config/about/create', ['as' => 'about.create', 'uses' => 'BAboutUsController@create']);
    Route::post('config/about', ['as' => 'about.store', 'uses' => 'BAboutUsController@store']);
    Route::get('config/about/{id}/edit', ['as' => 'about.edit', 'uses' => 'BAboutUsController@edit']);
    Route::patch('config/about/{id}', ['as' => 'about.update', 'uses' => 'BAboutUsController@update']);
    Route::delete('config/about/destroy/{id}', ['as' => 'about.destroy', 'uses' => 'BAboutUsController@destroy']);

    //jadwal acara
    Route::get('config/schedule', ['as' => 'admin.config.schedule.index', 'uses' => 'BScheduleController@index']);
    Route::get('config/schedule/create', ['as' => 'schedule.create', 'uses' => 'BScheduleController@create']);
    Route::post('config/schedule', ['as' => 'schedule.store', 'uses' => 'BScheduleController@store']);
    Route::get('config/schedule/{id}/edit', ['as' => 'schedule.edit', 'uses' => 'BScheduleController@edit']);
    Route::patch('config/schedule/{id}', ['as' => 'schedule.update', 'uses' => 'BScheduleController@update']);
    Route::delete('config/schedule/destroy/{id}', ['as' => 'schedule.destroy', 'uses' => 'BScheduleController@destroy']);

    //galeri
    Route::get('config/gallery', ['as' => 'admin.config.gallery.index', 'uses' => 'BGalleryController@index']);
    Route::get('config/gallery/create', ['as' => 'gallery.create', 'uses' => 'BGalleryController@create']);
    Route::post('config/gallery', ['as' => 'gallery.store', 'uses' => 'BGalleryController@store']);
    Route::get('config/gallery/{id}/edit', ['as' => 'gallery.edit', 'uses' => 'BGalleryController@edit']);
    Route::patch('config/gallery/{id}', ['as' => 'gallery.update', 'uses' => 'BGalleryController@update']);
    Route::delete('config/gallery/destroy/{id}', ['as' => 'gallery.destroy', 'uses' => 'BGalleryController@destroy']);

    //outlet konten
    Route::get('config/outlet', ['as' => 'admin.config.outlet.index', 'uses' => 'BOutletController@index']);
    Route::get('config/outlet/create', ['as' => 'outlet.create', 'uses' => 'BOutletController@create']);
    Route::post('config/outlet', ['as' => 'outlet.store', 'uses' => 'BOutletController@store']);
    Route::get('config/outlet/{id}/edit', ['as' => 'outlet.edit', 'uses' => 'BOutletController@edit']);
    Route::patch('config/outlet/{id}', ['as' => 'outlet.update', 'uses' => 'BOutletController@update']);
    Route::delete('config/outlet/destroy/{id}', ['as' => 'outlet.destroy', 'uses' => 'BOutletController@destroy']);

    Route::get('config/slider-outlet/{equal_id}', ['as' => 'slider-outlet.index', 'uses' => 'BOutletSliderController@index']);
    Route::get('config/slider-outlet/{equal_id}/create', ['as' => 'slider-outlet.create', 'uses' => 'BOutletSliderController@create']);
    Route::post('config/slider-outlet/{equal_id}', ['as' => 'slider-outlet.store', 'uses' => 'BOutletSliderController@store']);
    Route::get('config/slider-outlet/{id}/edit', ['as' => 'slider-outlet.edit', 'uses' => 'BOutletSliderController@edit']);
    Route::patch('config/slider-outlet/{id}', ['as' => 'slider-outlet.update', 'uses' => 'BOutletSliderController@update']);
    Route::delete('config/slider-outlet/destroy/{id}', ['as' => 'slider-outlet.destroy', 'uses' => 'BOutletSliderController@destroy']);

    Route::get('config/outlet-menu/{equal_id}', ['as' => 'outlet-menu.index', 'uses' => 'BOutletMenuController@index']);
    Route::post('config/outlet-menu/{equal_id}', ['as' => 'outlet-menu.update', 'uses' => 'BOutletMenuController@update']);

    //packages konten
    Route::get('config/packages', ['as' => 'admin.config.packages.index', 'uses' => 'BPackageCategoryController@index']);
    Route::get('config/packages/create', ['as' => 'packages.create', 'uses' => 'BPackageCategoryController@create']);
    Route::post('config/packages', ['as' => 'packages.store', 'uses' => 'BPackageCategoryController@store']);
    Route::get('config/packages/{id}/edit', ['as' => 'packages.edit', 'uses' => 'BPackageCategoryController@edit']);
    Route::patch('config/packages/{id}', ['as' => 'packages.update', 'uses' => 'BPackageCategoryController@update']);
    Route::delete('config/packages/{id}', ['as' => 'packages.destroy', 'uses' => 'BPackageCategoryController@destroy']);

    Route::get('config/packageslist/{cat_id}', ['as' => 'packageslist.index', 'uses' => 'BPackagesController@index']);
    Route::get('config/packageslist/{cat_id}/create', ['as' => 'packageslist.create', 'uses' => 'BPackagesController@create']);
    Route::post('config/packageslist/{cat_id}', ['as' => 'packageslist.store', 'uses' => 'BPackagesController@store']);
    Route::get('config/packageslist/{id}/edit', ['as' => 'packageslist.edit', 'uses' => 'BPackagesController@edit']);
    Route::patch('config/packageslist/{id}', ['as' => 'packageslist.update', 'uses' => 'BPackagesController@update']);
    Route::delete('config/packageslist/{id}', ['as' => 'packageslist.destroy', 'uses' => 'BPackagesController@destroy']);

    //news
    Route::get('config/news', ['as' => 'admin.config.news.index', 'uses' => 'BNewsController@index']);
    Route::get('config/news/create', ['as' => 'news.create', 'uses' => 'BNewsController@create']);
    Route::post('config/news', ['as' => 'news.store', 'uses' => 'BNewsController@store']);
    Route::get('config/news/{id}/edit', ['as' => 'news.edit', 'uses' => 'BNewsController@edit']);
    Route::patch('config/news/{id}', ['as' => 'news.update', 'uses' => 'BNewsController@update']);
    Route::delete('config/news/destroy/{id}', ['as' => 'news.destroy', 'uses' => 'BNewsController@destroy']);

    Route::get('config/news-category/create', ['as' => 'news-category.create', 'uses' => 'BNewsCategoriesController@create']);
    Route::post('config/news-category', ['as' => 'news-category.store', 'uses' => 'BNewsCategoriesController@store']);
    Route::get('config/news-category/{id}/edit', ['as' => 'news-category.edit', 'uses' => 'BNewsCategoriesController@edit']);
    Route::patch('config/news-category/{id}', ['as' => 'news-category.update', 'uses' => 'BNewsCategoriesController@update']);
    Route::delete('config/news-category/destroy/{id}', ['as' => 'news-category.destroy', 'uses' => 'BNewsCategoriesController@destroy']);

    Route::get('config/news-tag/create', ['as' => 'news-tag.create', 'uses' => 'BNewsTagsController@create']);
    Route::post('config/news-tag', ['as' => 'news-tag.store', 'uses' => 'BNewsTagsController@store']);
    Route::get('config/news-tag/{id}/edit', ['as' => 'news-tag.edit', 'uses' => 'BNewsTagsController@edit']);
    Route::patch('config/news-tag/{id}', ['as' => 'news-tag.update', 'uses' => 'BNewsTagsController@update']);
    Route::delete('config/news-tag/destroy/{id}', ['as' => 'news-tag.destroy', 'uses' => 'BNewsTagsController@destroy']);

    //Sister Company
    Route::get('config/sistercompany', ['as' => 'admin.config.sistercompany.index', 'uses' => 'BSisterCompanyController@index']);
    Route::get('config/sistercompany/create', ['as' => 'sistercompany.create', 'uses' => 'BSisterCompanyController@create']);
    Route::post('config/sistercompany', ['as' => 'sistercompany.store', 'uses' => 'BSisterCompanyController@store']);
    Route::get('config/sistercompany/{id}/edit', ['as' => 'sistercompany.edit', 'uses' => 'BSisterCompanyController@edit']);
    Route::patch('config/sistercompany/{id}', ['as' => 'sistercompany.update', 'uses' => 'BSisterCompanyController@update']);
    Route::delete('config/sistercompany/destroy/{id}', ['as' => 'sistercompany.destroy', 'uses' => 'BSisterCompanyController@destroy']);

    // Opening & Closing
    Route::get('config/open', ['as' => 'admin.config.open.index', 'uses' => 'BOpenController@index']);
    Route::patch('config/open', ['as' => 'admin.config.open.update', 'uses' => 'BOpenController@update']);

    //menukonten

    Route::get('config/menu-konten', ['as' => 'admin.config.menu-konten.index', 'uses' => 'BMenusController@index']);


    //category
    Route::get('config/menu-konten/category/create', ['as' => 'category.create', 'uses' => 'BCategoryController@create']);
    Route::post('config/menu-konten/category', ['as' => 'category.store', 'uses' => 'BCategoryController@store']);
    Route::get('config/menu-konten/category/{id}/edit', ['as' => 'category.edit', 'uses' => 'BCategoryController@edit']);
    Route::patch('config/menu-konten/category/{id}', ['as' => 'category.update', 'uses' => 'BCategoryController@update']);
    Route::delete('config/menu-konten/category/{id}', ['as' => 'category.destroy', 'uses' => 'BCategoryController@destroy']);

    //menu
    Route::get('config/menu-konten/menus/create', ['as' => 'menus.create', 'uses' => 'BMenusController@create']);
    Route::post('config/menu-konten/menus', ['as' => 'menus.store', 'uses' => 'BMenusController@store']);
    Route::get('config/menu-konten/menus/{id}/edit', ['as' => 'menus.edit', 'uses' => 'BMenusController@edit']);
    Route::patch('config/menu-konten/menus/{id}', ['as' => 'menus.update', 'uses' => 'BMenusController@update']);
    Route::delete('config/menu-konten/menus/{id}', ['as' => 'menus.destroy', 'uses' => 'BMenusController@destroy']);

    Route::get('config/menu-konten/additional', ['as' => 'menus-additional.edit', 'uses' => 'BMenusController@editadditional']);
    Route::patch('config/menu-konten/additional', ['as' => 'menus-additional.update', 'uses' => 'BMenusController@updateadditional']);

    //Clear Cache facade value:
    Route::get('/clear-cache', function () {
        $exitCode = Artisan::call('cache:clear');
        return '<h1>Cache facade value cleared</h1>';
    });

    //Reoptimized class loader:
    Route::get('/optimize', function () {
        $exitCode = Artisan::call('optimize');
        return '<h1>Reoptimized class loader</h1>';
    });

    //Route cache:
    Route::get('/route-cache', function () {
        $exitCode = Artisan::call('route:cache');
        return '<h1>Routes cached</h1>';
    });

    //Route list:
    Route::get('/route-list', function () {
        $exitCode = Artisan::call('route:list');
        return '<h1>Routes list</h1>';
    });

    //Clear Route cache:
    Route::get('/route-clear', function () {
        $exitCode = Artisan::call('route:clear');
        return '<h1>Route cache cleared</h1>';
    });

    //Clear View cache:
    Route::get('/view-clear', function () {
        $exitCode = Artisan::call('view:clear');
        return '<h1>View cache cleared</h1>';
    });

    //Config cache:
    Route::get('/config-cache', function () {
        $exitCode = Artisan::call('config:cache');
        return '<h1>Config cached</h1>';
    });

    //Clear config cache:
    Route::get('/config-clear', function () {
        $exitCode = Artisan::call('config:clear');
        return '<h1>Clear config cached</h1>';
    });

    Route::get('/dump-autoload', function () {
        system('composer dump-autoload');
        echo '<h1>dump-autoload complete</h1>';
    });
});


